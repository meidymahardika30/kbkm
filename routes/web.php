<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'LandingPageController@index');

Route::get('/landingpage/index','LandingPageController@index');
Route::get('findkota','PendaftaranController@findkota') ;
Route::get('findkecamatan','PendaftaranController@findkec') ;

Route::get('/pendaftaran/index', 'PendaftaranController@index');
Route::post('pendaftaran/store', 'PendaftaranController@store');
Route::get('/pendaftaran/pdf/index', 'PdfController@index');

Route::get('/pendaftaran/cek/index', 'CekController@index');
Route::get('/pendaftaran/cek/store', 'CekController@store');
Route::get('/pendaftaran/cek/edit/{id}', 'CekController@edit');
Route::patch('/pendaftaran/cek/update/{id}', 'CekController@update');

//Berita Arsip Landingpage
Route::get('/arsip','ViewArsipContoller@index');
Route::get('/arsip/detail/{id}','ViewArsipContoller@show');

//admin
Route::get('/admin','ViewArsipContoller@login');

Route::group(['namespace' => 'admin', 'prefix' => 'admin' ],  function() {
    Route::get('/dashboard','DashboardController@index');
    Route::get('/peserta','PesertaController@index');
    Route::get('/kelompok','KelompokController@index');
    Route::get('/kelompok/ktp/pdf/{id}','KelompokController@ktp');
    Route::get('/kelompok/portofolio/pdf/{id}','KelompokController@portofolio');
    Route::get('/kelompok/detail/{id}','KelompokController@show');
    Route::get('/kelompok/delete/{id}','KelompokController@destroy');
    Route::get('/balai','BalaiController@index');
    Route::get('/kelompok/edit/{id}','KelompokController@edit');
    Route::patch('/kelompok/update/{id}','KelompokController@update');

    //Route Pendaftar
    Route::get('/pendaftar/edit/{id}','PesertaController@edit');
    Route::patch('/pendaftar/update/{id}','PesertaController@update');

    //Berita Controller
    Route::get('/berita','BeritaController@index');
    Route::get('/berita/create','BeritaController@create');
    Route::post('/berita/store','BeritaController@store');
    Route::get('/berita/edit/{id}','BeritaController@edit');
    Route::patch('/berita/update/{id}','BeritaController@update');
    Route::get('/berita/delete/{id}','BeritaController@destroy');

    //Arsip Controller
    Route::get('/arsip','ArsipController@index');
    Route::get('/arsip/create','ArsipController@create');
    Route::post('/arsip/store','ArsipController@store');
    Route::get('/arsip/edit/{id}','ArsipController@edit');
    Route::patch('/arsip/update/{id}','ArsipController@update');
    Route::get('/arsip/delete/{id}','ArsipController@destroy');

    //User Controller
    Route::get('/user','RegisterController@index');
    Route::get('/user/view','RegisterController@view');
    Route::post('/user/create','RegisterController@create');
    Route::get('/user/edit/{id}','RegisterController@edit');
    Route::patch('/user/update/{id}','RegisterController@update');
    Route::get('/user/delete/{id}','RegisterController@destroy');
    
    Route::group(['prefix' => 'report'], function() {
        Route::get('/peserta','ReportController@indexPeserta');
        Route::get('/kelompok','ReportController@indexKelompok');
        Route::get('/kelompok/ktp/pdf/{id}','ReportController@ktp');
        Route::get('/kelompok/portofolio/pdf/{id}','ReportController@portofolio');
        Route::get('/kelompok/find-wilayah','ReportController@findwilayah');
        Route::get('/kelompok/search-kelompok','ReportController@searchkelompok');
        Route::get('/kelompok/search-peserta','ReportController@searchpeserta');
        
        //Export Excel
        Route::get('/kelompok/export','ReportController@exportkelompok');
        Route::get('/kelompok/kelompok/export','ReportController@exportkelompok');
        Route::get('/exportpendaftar','ReportController@exportpendaftar');
        Route::get('/kelompok/exportpendaftar','ReportController@exportpendaftar');

        Route::get('/bpnb','BalaiController@exportbpnb');

        //Route PDF
        Route::get('/kelompok/detail/{id}','ReportController@detailpdfkelompok');
        Route::get('/kelompok/pdf/{id}','ReportController@pdf');
    });
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
