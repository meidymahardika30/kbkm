// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
var ctx = document.getElementById("myBarChart");
var age18 = document.getElementById("age18").value;
var age19 = document.getElementById("age19").value;
var age20 = document.getElementById("age20").value;
var age21 = document.getElementById("age21").value;
var age22 = document.getElementById("age22").value;
var age23 = document.getElementById("age23").value;
var age24 = document.getElementById("age24").value;
var age25 = document.getElementById("age25").value;
var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["18", "19", "20", "21", "22", "23","24","25"],
    datasets: [{
      label: "Revenue",
      backgroundColor: "rgba(2,117,216,1)",
      borderColor: "rgba(2,117,216,1)",
      data: [age18, age19, age20, age21, age22, age23, age24, age25],
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 6
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 50,
          maxTicksLimit: 5
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});
