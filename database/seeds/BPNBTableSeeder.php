<?php

use Illuminate\Database\Seeder;
use App\Nomenklatur;
use App\Wilayah;

class BPNBTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //input dummy data di table nomenklatur        
        Nomenklatur::create([
            'id_nomenklatur'=>'bpnb1',
            'nama_nomenklatur'=>'Balai Pelestarian Nilai Budaya Aceh (BPNB Aceh)',
        ]);

        Nomenklatur::create([
            'id_nomenklatur'=>'bpnb2',
            'nama_nomenklatur'=>'Balai Pelestarian Nilai Budaya Sumatera Barat (BPNB Sumatera Barat) ',
        ]);

        Nomenklatur::create([
            'id_nomenklatur'=>'bpnb3',
            'nama_nomenklatur'=>'Balai Pelestarian Nilai Budaya Kepulauan Riau (BPNB Kepulauan Riau) ',
        ]);

        Nomenklatur::create([
            'id_nomenklatur'=>'bpnb4',
            'nama_nomenklatur'=>'Balai Pelestarian Nilai Budaya Jawa Barat (BPNB Jawa Barat) ',
        ]);

        Nomenklatur::create([
            'id_nomenklatur'=>'bpnb5',
            'nama_nomenklatur'=>'Balai Pelestarian Nilai Budaya D.I.Yogyakarta (BPNB D.I.Yogyakarta) ',
        ]);

        Nomenklatur::create([
            'id_nomenklatur'=>'bpnb6',
            'nama_nomenklatur'=>'Balai Pelestarian Nilai Budaya Kalimantan Barat (BPNB Kalimantan Barat) ',
        ]);

        Nomenklatur::create([
            'id_nomenklatur'=>'bpnb7',
            'nama_nomenklatur'=>'Balai Pelestarian Nilai Budaya Bali (BPNB Bali) ',
        ]);

        Nomenklatur::create([
            'id_nomenklatur'=>'bpnb8',
            'nama_nomenklatur'=>'Balai Pelestarian Nilai Budaya Sulawesi Selatan (BPNB Sulawesi Selatan) ',
        ]);

        Nomenklatur::create([
            'id_nomenklatur'=>'bpnb9',
            'nama_nomenklatur'=>'Balai Pelestarian Nilai Budaya Sulawesi Utara (BPNB Sulawesi Utara) ',
        ]);

        Nomenklatur::create([
            'id_nomenklatur'=>'bpnb10',
            'nama_nomenklatur'=>'Balai Pelestarian Nilai Budaya Maluku (BPNB Maluku) ',
        ]);

        Nomenklatur::create([
            'id_nomenklatur'=>'bpnb11',
            'nama_nomenklatur'=>'Balai Pelestarian Nilai Budaya Papua (BPNB Papua) ',
        ]);
         //input dummy data di table wilayah        
         Wilayah::create([
            'id_nomenklatur'=>'bpnb1',
            'id_provinsi'=>11,
            'name'=>'ACEH',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb1',
            'id_provinsi'=>12,
            'name'=>'SUMATERA UTARA',
        ]);

        Wilayah::create([
            'id_nomenklatur'=>'bpnb2',
            'id_provinsi'=>13,
            'name'=>'SUMATERA BARAT',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb2',
            'id_provinsi'=>17,
            'name'=>'BENGKULU',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb2',
            'id_provinsi'=>16,
            'name'=>'SUMATERA SELATAN',
        ]);

        Wilayah::create([
            'id_nomenklatur'=>'bpnb3',
            'id_provinsi'=>21,
            'name'=>'KEPULAUAN RIAU',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb3',
            'id_provinsi'=>14,
            'name'=>'RIAU',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb3',
            'id_provinsi'=>15,
            'name'=>'JAMBI',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb3',
            'id_provinsi'=>19,
            'name'=>'KEPULAUAN BANGKA BELITUNG',
        ]);

        Wilayah::create([
            'id_nomenklatur'=>'bpnb4',
            'id_provinsi'=>32,
            'name'=>'JAWA BARAT',
        ]);  
        Wilayah::create([
            'id_nomenklatur'=>'bpnb4',
            'id_provinsi'=>31,
            'name'=>'DKI JAKARTA',
        ]);  
        Wilayah::create([
            'id_nomenklatur'=>'bpnb4',
            'id_provinsi'=>36,
            'name'=>'BANTEN',
        ]);  
        Wilayah::create([
            'id_nomenklatur'=>'bpnb4',
            'id_provinsi'=>18,
            'name'=>'LAMPUNG',
        ]);  

        Wilayah::create([
            'id_nomenklatur'=>'bpnb5',
            'id_provinsi'=>34,
            'name'=>'DI YOGYAKARTA',
        ]); 
        Wilayah::create([
            'id_nomenklatur'=>'bpnb5',
            'id_provinsi'=>35,
            'name'=>'JAWA TIMUR',
        ]);  
        Wilayah::create([
            'id_nomenklatur'=>'bpnb5',
            'id_provinsi'=>33,
            'name'=>'JAWA TENGAH',
        ]);

        Wilayah::create([
            'id_nomenklatur'=>'bpnb6',
            'id_provinsi'=>61,
            'name'=>'KALIMANTAN BARAT',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb6',
            'id_provinsi'=>64,
            'name'=>'KALIMANTAN TIMUR',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb6',
            'id_provinsi'=>62,
            'name'=>'KALIMANTAN TENGAH',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb6',
            'id_provinsi'=>63,
            'name'=>'KALIMANTAN SELATAN',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb6',
            'id_provinsi'=>65,
            'name'=>'KALIMANTAN UTARA',
        ]);

        Wilayah::create([
            'id_nomenklatur'=>'bpnb7',
            'id_provinsi'=>51,
            'name'=>'BALI',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb7',
            'id_provinsi'=>52,
            'name'=>'NUSA TENGGARA BARAT',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb7',
            'id_provinsi'=>53,
            'name'=>'NUSA TENGGARA TIMUR',
        ]);

        Wilayah::create([
            'id_nomenklatur'=>'bpnb8',
            'id_provinsi'=>73,
            'name'=>'SULAWESI SELATAN',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb8',
            'id_provinsi'=>74,
            'name'=>'SULAWESI TENGGARA',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb8',
            'id_provinsi'=>76,
            'name'=>'SULAWESI BARAT',
        ]);

        Wilayah::create([
            'id_nomenklatur'=>'bpnb9',
            'id_provinsi'=>71,
            'name'=>'SULAWESI UTARA',
        ]);
         Wilayah::create([
            'id_nomenklatur'=>'bpnb9',
            'id_provinsi'=>72,
            'name'=>'SULAWESI TENGAH',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb9',
            'id_provinsi'=>72,
            'name'=>'SULAWESI TENGAH',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb9',
            'id_provinsi'=>75,
            'name'=>'GORONTALO',
        ]);

        Wilayah::create([
            'id_nomenklatur'=>'bpnb10',
            'id_provinsi'=>81,
            'name'=>'MALUKU',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb10',
            'id_provinsi'=>82,
            'name'=>'MALUKU UTARA',
        ]);

        Wilayah::create([
            'id_nomenklatur'=>'bpnb11',
            'id_provinsi'=>94,
            'name'=>' PAPUA ',
        ]);
        Wilayah::create([
            'id_nomenklatur'=>'bpnb11',
            'id_provinsi'=>91,
            'name'=>'PAPUA BARAT',
        ]);
    }
}
