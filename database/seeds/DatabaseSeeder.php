
<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Crypt;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BPNBTableSeeder::class);
        
        //input dummy data di table user 
        // username: admin.kbkm@gmail.com 
        // pass: kbkm2020
        // User::create([
        //     'name'=>'Admin KBKM',
        //     'email'=>'admin.kbkm@gmail.com',
        //     'password'=>Hash::make('kbkm2020'),
        // ]);
    }
}
