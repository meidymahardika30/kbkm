<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendaftarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_kelompok');
            $table->string('nama');
            $table->char('jk');
            $table->string('no_hp');
            $table->string('email');
            $table->string('pendidikan');
            $table->string('nik');
            $table->date('tanggal_lahir');
            $table->integer('id_provinsi');
            $table->integer('id_kota');
            $table->integer('id_kecamatan');
            $table->text('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftar');
    }
}
