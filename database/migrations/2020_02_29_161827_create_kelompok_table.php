<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKelompokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelompok', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_kelompok');
            $table->string('ketua');
            $table->string('ktp');
            // $table->string('portofolio');
            // $table->text('subjek_portofolio');
            $table->string('mata_lomba');
            $table->text('pertanyaan1');
            $table->text('pertanyaan2');
            $table->text('pertanyaan3');
            $table->text('pertanyaan4');
            $table->text('pertanyaan5');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelompok');
    }
}
