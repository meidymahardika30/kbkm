@extends('layouts.header')

@section('content')
  <br>
  <div class="container area-padding">
    @foreach($kelompok as $row)
    <form method="POST" action="{{ url('/pendaftaran/cek/update/'.$row->id) }}" enctype="multipart/form-data">
      @csrf {{ method_field('PATCH') }}
      <br>

      @include('layouts.alert')

      @if ($errors->has('nama.*') || $errors->has('jk.*') || $errors->has('no_hp.*') || $errors->has('email.*') || $errors->has('pendidikan.*') || $errors->has('nik.*') || $errors->has('tanggal_lahir.*') || $errors->has('provinsi.*') || $errors->has('kota.*') || $errors->has('kecamatan.*') || $errors->has('alamat.*'))
        <div class="alert alert-danger" role="alert">
          <strong>Ada data yang belum diisi atau salah, mohon cek kembali.</strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="alert alert-danger" role="alert">
          <strong>Harap isi kembali bagian wilayah.</strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif

      <!-- @if ($errors->has('portofolio'))
        <div class="alert alert-danger" role="alert">
          <strong>{{ $errors->first('portofolio') }}</strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif -->

      <br>
      <h3>Profile Kelompok Edit</h3>

      <div class="form-group {{ $errors->has('nama_kelompok') ? 'has-error' : '' }}">
        <label for="nama_kelompok">Nama Kelompok</label>
        <input type="text" class="form-control" name="nama_kelompok" value="{{ $row->nama_kelompok }}">
        @if ($errors->has('nama_kelompok'))
          <span class="help-block">
            {{ $errors->first('nama_kelompok') }}
          </span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('mata_lomba') ? 'has-error' : '' }}">
        <label for="jenis_lomba">Bentuk inisiatif seperti apa yang akan kelompok kamu lakukan untuk menghasilkan output dan outcome, pilih bidang lomba berikut.</label>
        <div class="form-check form-check-inline">
          <input readonly class="form-check-input detail"  type="radio" name="mata_lomba" value="aplikasi" {{ ($row->mata_lomba) == "aplikasi" ? "checked" : "" }} id="aplikasi">
          <label class="form-check-label" for="aplikasi">
            Aplikasi
          </label>

          <input readonly class="form-check-input detail"  type="radio" name="mata_lomba" value="prakarya" {{ ($row->mata_lomba) == "prakarya" ? "checked" : "" }} id="prakarya" style="margin-left: 20px;">
          <label class="form-check-label" for="prakarya">
            Prakarya
          </label>

        </div>
        @if ($errors->has('mata_lomba'))
          <span class="text-danger">{{ $errors->first('mata_lomba') }}</span>
        @endif
      </div>

      <br>
      
      <label>Catatan:</label>
      <ol>
        <li>Untuk mata lomba Aplikasi, satu kelompok harus terdiri dari 5 orang dan untuk mata lomba Prakarya, satu kelompok harus terdiri dari 3 orang.</li>
        <li>Satu kelompok harus memiliki perpaduan unsur laki-laki dan perempuan. Minimal satu orang perempuan.</li>
        <li>Satu kelompok diharapkan berdomisili di satu Kota / Kabupaten yang sama untuk mempermudah koordinasi.</li>
        <li>Wajib melampirkan salinan ktp.</li>
        <li>Nomor HP yang dicantumkan harus memiliki fitur Whatsapp.</li>
      </ol>

      <br>

      <table class="table table-bordered">
        <tr id="form-head" align="center">
          <td>No</td>
          <td>Data</td>
        </tr>
        @foreach($pendaftar as $index => $item)
        <tr id="form-satu">
          <td style="text-align: center; vertical-align: middle;">{{ $index+1 }}</td>
          <td>
            <div class="row">
              <div class="col-sm-3 form-group {{ $errors->has('nama.0') ? 'has-error' : '' }}">
                  <label for="nama" class="control-label">Nama Ketua Kelompok</label>
                  <input type="text" class="form-control" name=nama[] value="{{ $item->nama }}">
                  @if ($errors->has('nama'.$index))
                    <span class="help-block">
                      {{ $errors->first('nama'.$index) }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('jk.0') ? 'has-error' : '' }}">
                  <label for="jk" class="control-label">Jenis Kelamin</label>
                  <select class="form-control" name=jk[]>
                    <option value="{{ $item->jk }}">{{ $item->jk == 'l' ? 'Laki-Laki' : 'Perempuan' }}</option>
                    <option value="l">Laki-Laki</option>
                    <option value="p">Perempuan</option>
                  </select>
                  @if ($errors->has('jk'.$index))
                    <span class="help-block">
                      {{ $errors->first('jk'.$index) }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('no_hp.0') ? 'has-error' : '' }}">
                  <label class="control-label">No.HP</label>
                  <input type="text" class="form-control" name=no_hp[] value="{{ $item->no_hp }}">
                  @if ($errors->has('no_hp'.$index))
                    <span class="help-block">
                      {{ $errors->first('no_hp'.$index) }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('email.0') ? 'has-error' : '' }}">
                  <label class="control-label">Email</label>
                  <input readonly type="email" class="form-control" name=email[] value="{{ $item->email }}">
                  @if ($errors->has('email'.$index))
                    <span class="help-block">
                      {{ $errors->first('email'.$index) }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-4 form-group {{ $errors->has('pendidikan'.$index) ? 'has-error' : '' }}">
                  <label class="control-label">Pendidikan</label>
                  <select class="form-control" name=pendidikan[] value="{{ old('pendidikan'.$index) }}">
                    <option value="{{ $item->pendidikan }}">{{ $item->pendidikan }}</option>
                    <option value="SD">SD</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA">SMA</option>
                    <option value="SMK">SMK</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="D3">D3</option>
                    <option value="D4">D4</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                  @if ($errors->has('pendidikan'.$index))
                    <span class="help-block">
                      {{ $errors->first('pendidikan'.$index) }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('nik'.$index) ? 'has-error' : '' }}">
                  <label class="control-label">NIK</label>
                  <input type="number" class="form-control" name=nik[] value="{{ $item->nik }}" readonly>
                  @if ($errors->has('nik'.$index))
                    <span class="help-block">
                      {{ $errors->first('nik'.$index) }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('tanggal_lahir'.$index) ? 'has-error' : '' }}">
                  <label>Tanggal Lahir</label>
                  <input type="date" class="form-control" name=tanggal_lahir[] max={{ $max }} min={{ $min }} value="{{ $item->tanggal_lahir }}">
                  @if ($errors->has('tanggal_lahir'.$index))
                    <span class="help-block">
                      {{ $errors->first('tanggal_lahir'.$index) }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <table border="0" width="100%" align="center">
                    <tr>
                      <td width="33%" style="text-align:center"><label>Provinsi</label></td>
                      <td width="33%" style="text-align:center"><label>Kabupaten / Kota</label></td>
                      <td width="33%" style="text-align:center"><label>Kecamatan</label></td>
                      </tr>
                      <tr>
                      <td style="width:30%; " class="form-group" colspan="3">
                        <select style="width:30%; height:30px" name=provinsi[] class="custom-select provinsi">
                            <option value="{{ $item->id_provinsi }}">{!! GetProvinsi($item->id_provinsi) !!}</option>
                            @foreach ($provinsi as $prov)
                              <option value="{{$prov->id}}">{{ $prov->name }}</option>
                            @endforeach
                        </select>
                        <select style="margin-left:2%; width:30%; height:30px;" name=kota[] class="custom-select kota">
                            <option value="{{ $item->id_kota }}">{!! GetKota($item->id_kota) !!}</option>
                        </select>
                        <select style="margin-left:3%; width:32%; height:30px;" name=kecamatan[] class="custom-select kecamatan">
                            <option value="{{ $item->id_kecamatan }}">{!! GetKecamatan($item->id_kecamatan) !!}</option>
                        </select>
                      </td>
                    </tr>
                  </table>
                  @if ($errors->has('provinsi'.$index) || $errors->has('kota'.$index) || $errors->has('Kecamatan'.$index))
                    <span class="text-danger">
                      Wilayah wajib diisi.
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group {{ $errors->has('alamat'.$index) ? 'has-error' : '' }}">
                <label for="exampleFormControlInput1">Alamat</label>
                <textarea class="form-control" name=alamat[] rows="3">{{ $item->alamat }}</textarea>
                @if ($errors->has('alamat'.$index))
                  <span class="help-block">
                    {{ $errors->first('alamat'.$index) }}
                  </span>
                @endif
            </div>
          </td>
        </tr>
        @endforeach
      </table>

      <div class="form-group">
        <label for="ktp">Upload KTP</label>
        <input type="file" class="form-control" name="ktp" id="ktp">
        <span style="color: red;">*pilih ulang ktp</span>
        <br>
        <span style="color: red;">*data ktp dijadikan dalam satu file (<i>.pdf</i>) untuk 1 kelompok. <a href="{{ url('/pendaftaran/pdf/index') }}" target="_blank"><b>klik untuk melihat contoh</b></a></span>
        <p style="color: red;">*max file size 5mb</p>
        @if ($errors->has('ktp'))
          <span class="text-danger">{{ $errors->first('ktp') }}</span>
        @endif
      </div>

      <!-- <div class="form-group">
        <label for="portofolio">Upload Portofolio</label>
        <input type="file" class="form-control" name="portofolio" id="portofolio">
        <span style="color: red;">*pilih ulang portofolio</span>
        <br>
        <span style="color: red;">*file portofolio harus (<i>.pdf</i>)</span>
        <p style="color: red;">*max file size 10mb</p>
        @if ($errors->has('portofolio'))
          <span class="text-danger">{{ $errors->first('portofolio') }}</span>
        @endif
      </div> -->

      <!-- <div class="form-group">
          <label for="subjek_portofolio">Subjek Portofolio</label>
          <textarea class="form-control" name="subjek_portofolio" id="subjek_portofolio" rows="3"></textarea>
      </div> -->

      <br>

      <h3>Gambaran Inisatif</h3>
      
      <br>

      <label>Catatan:</label>
      <ol>
        <li>Uraian tentang permasalahan pemajuan kebudayaan di Indonesia dapat merujuk ke Pokok Pikiran Kebudayaan Daerah Kabupaten/Kota dan Provinsi. Dokumen-dokumen ini dapat diakses di <a href="http://kongres.kebudayaan.id/pra-kongres-kebudayaan-2018/" target="_blank">http://kongres.kebudayaan.id/pra-kongres-kebudayaan-2018/</a></li>
        <li><b>Pemajuan Kebudayaan</b> adalah upaya meningkatkan ketahanan budaya dan kontribusi budaya Indonesia di tengah peradaban dunia melalui Pelindungan, Pengembangan, Pemanfaatan, dan Pembinaan Kebudayaan.</li>
        <li><b>Pelindungan</b> adalah upaya menjaga keberlanjutan Kebudayaan yang dilakukan dengan cara inventarisasi, pengamanan, pemeliharaan, penyelamatan, dan publikasi.</li>
        <li><b>Pengembangan</b> adalah upaya menghidupkan ekosistem Kebudayaan serta meningkatkan, memperkaya, dan menyebarluaskan Kebudayaan.</li>
        <li><b>Pemanfaatan </b> adalah upaya pendayagunaan Objek Pemajuan Kebudayaan untuk menguatkan ideologi, politik, ekonomi, sosial, budaya, pertahanan, dan keamanan dalam mewujudkan tujuan nasional.</li>
        <li><b>Pembinaan</b> adalah upaya pemberdayaan Sumber Daya Manusia Kebudayaan, lembaga Kebudayaan, dan pranata Kebudayaan dalam meningkatkan dan memperluas peran aktif dan inisiatif masyarakat.</li>
        <li><b>Aplikasi</b> adalah piranti lunak (software) yang dapat menjawab tantangan pemajuan kebudayaan, misalnya aplikasi pelaporan persekusi, game online, aplikasi match-making tempat pertunjukan, dan lain-lain.</li>
        <li><b>Prakarya</b> adalah kriya fisik atau piranti keras (hardware) yang dapat menjawab tantangan pemajuan kebudayaan, misalnya mesin pemindai manuskrip lontar, board game bertema sejarah, robot pembaca puisi, dan lain-lain.</li>
        <li><b>Pendekatan STEAM</b> adalah pendekatan pemajuan kebudayaan yang memadukan aspek ilmu pengetahuan, teknologi, rekayasa (engineering), seni dan matematika.</li>
      </ol>
      
      <br>

      <table class="table table-bordered">
        <tr>
          <td width=50% style="font-size: 16px;">Apa sih pemajuan kebudayaan menurutmu?</td>
          <td>
            <div class="form-group">
              <textarea class="form-control" name="pertanyaan1" rows="3">{{ $row->pertanyaan1 }}</textarea>
            </div>
          </td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Apa masalah pemajuan kebudayaan di sekitarmu yang mau kamu pecahkan?</td>
          <td>
            <div class="form-group">
              <textarea class="form-control" name="pertanyaan2" rows="3">{{ $row->pertanyaan2 }}</textarea>
            </div>
          </td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Deskripsikan ide pemecahan masalah kamu dalam 100 kata</td>
          <td>
            <div class="form-group">
              <textarea class="form-control" name="pertanyaan3" rows="3">{{ $row->pertanyaan3 }}</textarea>
            </div>
          </td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Langkah kerja apa yang akan kelompok kamu lakukan untuk memecahkan masalah tersebut?</td>
          <td>
            <div class="form-group">
              <textarea class="form-control" name="pertanyaan4" rows="3">{{ $row->pertanyaan4 }}</textarea>
            </div>
          </td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Apa hasil akhir dan dampak dari pemecahan masalah tersebut? </td>
          <td>
            <div class="form-group">
              <textarea class="form-control" name="pertanyaan5" rows="3">{{ $row->pertanyaan5 }}</textarea>
            </div>
          </td>
        </tr>
      </table>

      <br>

      <div class="form-group">
        <!-- Button trigger modal -->
        <button type="submit" class="form-control btn btn-primary">Update</button>
      </div>

      <!-- Modal -->
      <!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <b>Apakah anda sudah yakin? cek kembali form jika ada kesalahan</b>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </div>
        </div>
      </div> -->
    </form>
    @endforeach
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    
    <script type="text/javascript">
      // $('#exampleModal').on('hidden.bs.modal', function (e) {
      //   var button = e.relatedTarget;
      //   if($(button).hasClass('no-modal')) {
      //     e.stopPropagation();
      //   } 
      // })
      $(':radio:not(:checked)').attr('disabled', true);

      $('#exampleModal').on('show.bs.modal', function (event) {
        var top = $("body").scrollTop(); $("body").css('position','fixed').css('overflow','hidden').css('top',-top).css('width','100%');
        
      }).on("hide.bs.modal", function () {
        var top = $("body").position().top; $("body").css('position','relative').css('overflow','visible');
      });

      $(document).ready(function(){
        $(document).on('change','.provinsi',function(){
          var id_pro = $(this).val();
          var div = $(this).parent();
          var op = "";

          console.log(div);

          $.ajax({
            type : 'get',
            url : "{{ URL::to('findkota') }}",
            data : {'id':id_pro},
            dataType : 'json',
            success:function(data){
            op+='<option value="0">Pilih Kota</option>';
            for(var i = 0; i<data.length; i++){
              op+='<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }

            div.find('.kota').html("");
            div.find('.kota').append(op);
            },
            error:function(){

            }
          });
        });
        $(document).on('change','.kota',function(){
          var id_kec = $(this).val();
          var div = $(this).parent();
          var op = "";

          $.ajax({
            type : 'get',
            url : "{{ URL::to('findkecamatan') }}",
            data : {'id':id_kec},
            dataType : 'json',
            success:function(data){
              op+='<option value="0">Pilih Kecamatan</option>';
            for(var i = 0; i<data.length; i++){
              op+='<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }

            div.find('.kecamatan').html("");
            div.find('.kecamatan').append(op);
            },
            error:function(){

            }
          });
        });
      });

      // $(document).ready(function(){
      //   if ($("input[name='mata_lomba']:checked").val() == "aplikasi" ) { //Jika radio button "berbeda" dipilih maka tampilkan form-inputan
          
      //   } else {
      //     $("#form-empat").css("display","none");
      //     $("#form-lima").css("display","none");
      //   }

      //   // $("#form-aplikasi").css("display","none");
      //   // $("#form-prakarya").css("display","none");

      //   $(".detail").click(function(){ //Memberikan even ketika class detail di klik (class detail ialah class radio button)
      //     if ($("input[name='mata_lomba']:checked").val() == "aplikasi" ) { //Jika radio button "berbeda" dipilih maka tampilkan form-inputan
      //       $("#form-head").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
      //       $("#form-satu").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
      //       $("#form-dua").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
      //       $("#form-tiga").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
      //       $("#form-empat").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
      //       $("#form-lima").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
      //     } else {
      //       $("#form-head").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
      //       $("#form-satu").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
      //       $("#form-dua").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
      //       $("#form-tiga").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
      //       $("#form-empat").slideUp("fast"); //Efek Slide Down (Menampilkan Form Input)
      //       $("#form-lima").slideUp("fast"); //Efek Slide Down (Menampilkan Form Input)
      //     }
      //   });
      // });
    </script>

    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
  </div>
@endsection