<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>KBKM</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
  <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset('assets/lib/nivo-slider/css/nivo-slider.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/lib/owlcarousel/owl.carousel.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/lib/owlcarousel/owl.transitions.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/lib/animate/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/lib/venobox/venobox.css') }}" rel="stylesheet">

  <!-- Nivo Slider Theme -->
  <link href="{{ asset('assets/css/nivo-slider-theme.css') }}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

  <!-- Responsive Stylesheet File -->
  <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">

  <!-- =======================================================
    Theme Name: eBusiness
    Theme URL: https://bootstrapmade.com/ebusiness-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body data-spy="scroll" data-target="#navbar-example">

    <div class="container area-padding">
        @foreach($kelompok as $row)	
            @csrf
            <h2 align="center">PENDAFTARAN KBKM</h2>

            @include('layouts.alert')

            <br>
            <h3>Profil Kelompok</h3>

            <table class="table table-bordered">
            <tr>
                <td colspan="12">Nama Kelompok: <b>{{ $row->nama_kelompok }}</b></td>
            </tr>
            <tr>
                <td colspan="12">Nama Ketua: <b>{{ $row->ketua }}</b></td>
            </tr>
            <tr align="center">
                <td align="center">No</td>
                <td>Nama Peserta</td>
                <td>Jenis Kelamin</td>
                <td>No. HP</td>
                <td>Email</td>
                <td>Pendidikan</td>
                <td>NIK</td>
                <td>Tanggal Lahir</td>
                <td colspan="4">Alamat</td>
            </tr>
            <?php $i = 1; ?>
            @foreach($pendaftar as $item)
              <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{ $item->nama }}</td>
                  <td>{{ $item->jk == 'l' ? 'Laki-Laki' : 'Perempuan' }}</td>
                  <td>{{ $item->no_hp }}</td>
                  <td>{{ $item->email }}</td>
                  <td>{{ $item->pendidikan }}</td>
                  <td>{{ $item->nik }}</td>
                  <td>{{ $item->tanggal_lahir }}</td>
                  <td>{{ $item->alamat }}, {!! GetKecamatan($item->id_kecamatan) !!}, {!! GetKota($item->id_kota) !!}, {!! GetProvinsi($item->id_provinsi) !!}</td>
              </tr>
            @endforeach
            </table>
      
            <label>Catatan:</label>
            <ol>
                <li>Untuk mata lomba Aplikasi, satu kelompok harus terdiri dari 5 orang dan untuk mata lomba Prakarya, satu kelompok harus terdiri dari 3 orang.</li>
                <li>Satu kelompok harus memiliki perpaduan unsur laki-laki dan perempuan. Minimal satu orang perempuan.</li>
                <li>Satu kelompok diharapkan berdomisili di satu Kota / Kabupaten yang sama untuk mempermudah koordinasi.</li>
                <li>Wajib melampirkan salinan ktp.</li>
                <li>Nomor HP yang dicantumkan harus memiliki fitur Whatsapp.</li>
            </ol>

            <br>
            <h3>Gambaran Inisatif</h3>

      <table class="table table-bordered">
        <tr>
          <td width=50% style="font-size: 16px;">Apa sih pemajuan kebudayaan menurutmu?</td>
          <td>{{ $row->pertanyaan1 }}</td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Apa masalah pemajuan kebudayaan di sekitarmu yang mau kamu pecahkan?</td>
          <td>{{ $row->pertanyaan2 }}</td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Deskripsikan ide pemecahan masalah kamu dalam 100 kata</td>
          <td>{{ $row->pertanyaan3 }}</td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Langkah kerja apa yang akan kelompok kamu lakukan untuk memecahkan masalah tersebut?</td>
          <td>{{ $row->pertanyaan4 }}</td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Apa hasil akhir (output) dari pemecahan masalah tersebut?</td>
          <td>{{ $row->pertanyaan5 }}</td>
        </tr>

        <tr>
          <td width=50% style="font-size : 16px;">Bentuk inisiatif seperti apa yang akan kelompok kamu lakukan untuk mencapai hasil tersebut?</td>
          <td>{{ $row->mata_lomba }}</td>
        </tr>
      </table>

            <br>

            <label>Catatan:</label>
            <ol>
                <li>Uraian tentang permasalahan pemajuan kebudayaan di Indonesia dapat merujuk ke Pokok Pikiran Kebudayaan Daerah Kabupaten/Kota dan Provinsi. Dokumen-dokumen ini dapat diakses di <a href="http://kongres.kebudayaan.id/pra-kongres-kebudayaan-2018/" target="_blank">http://kongres.kebudayaan.id/pra-kongres-kebudayaan-2018/</a></li>
                <li><b>Pemajuan Kebudayaan</b> adalah upaya meningkatkan ketahanan budaya dan kontribusi budaya Indonesia di tengah peradaban dunia melalui Pelindungan, Pengembangan, Pemanfaatan, dan Pembinaan Kebudayaan.</li>
                <li><b>Pelindungan</b> adalah upaya menjaga keberlanjutan Kebudayaan yang dilakukan dengan cara inventarisasi, pengamanan, pemeliharaan, penyelamatan, dan publikasi.</li>
                <li><b>Pengembangan</b> adalah upaya menghidupkan ekosistem Kebudayaan serta meningkatkan, memperkaya, dan menyebarluaskan Kebudayaan.</li>
                <li><b>Pemanfaatan </b> adalah upaya pendayagunaan Objek Pemajuan Kebudayaan untuk menguatkan ideologi, politik, ekonomi, sosial, budaya, pertahanan, dan keamanan dalam mewujudkan tujuan nasional.</li>
                <li><b>Pembinaan</b> adalah upaya pemberdayaan Sumber Daya Manusia Kebudayaan, lembaga Kebudayaan, dan pranata Kebudayaan dalam meningkatkan dan memperluas peran aktif dan inisiatif masyarakat.</li>
                <li><b>Aplikasi</b> adalah piranti lunak (software) yang dapat menjawab tantangan pemajuan kebudayaan, misalnya aplikasi pelaporan persekusi, game online, aplikasi match-making tempat pertunjukan, dan lain-lain.</li>
                <li><b>Prakarya</b> adalah kriya fisik atau piranti keras (hardware) yang dapat menjawab tantangan pemajuan kebudayaan, misalnya mesin pemindai manuskrip lontar, board game bertema sejarah, robot pembaca puisi, dan lain-lain.</li>
                <li><b>Pendekatan STEAM</b> adalah pendekatan pemajuan kebudayaan yang memadukan aspek ilmu pengetahuan, teknologi, rekayasa (engineering), seni dan matematika.</li>
            </ol>

            <p style="color: red;">* Jika data anda sudah benar dan anda sudah yakin dengan isian anda, silahkan klik tombol simpan</p>
            
            <br>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                      <form action="{{ url('/pendaftaran/cek/edit/'.$sessionId) }}">
                          <button type="submit" class="col-md-6 form-control btn btn-warning" >Edit</button>
                      </form>
                    </div>
                    <div class="col-md-6">
                      <form action="{{ url('/pendaftaran/cek/store') }}">
                          <button type="submit" class="col-md-6 form-control btn btn-primary" >Simpan</button>
                      </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

  <!-- JavaScript Libraries -->
  <script src="{{ asset('assets/lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/lib/owlcarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/lib/venobox/venobox.min.js') }}"></script>
  <script src="{{ asset('assets/lib/knob/jquery.knob.js') }}"></script>
  <script src="{{ asset('assets/lib/wow/wow.min.js') }}"></script>
  <script src="{{ asset('assets/lib/parallax/parallax.js') }}"></script>
  <script src="{{ asset('assets/lib/easing/easing.min.js') }}"></script>
  <script src="{{ asset('assets/lib/nivo-slider/js/jquery.nivo.slider.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/lib/appear/jquery.appear.js') }}"></script>
  <script src="{{ asset('assets/lib/isotope/isotope.pkgd.min.js') }}"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <script src="{{ asset('assets/js/main.js') }}"></script>
  </body>
</html>