@extends('layouts.header')

@section('content')
  <br>
  <div class="container area-padding">		
    <form method="POST" action="{{ url('/pendaftaran/store') }}" enctype="multipart/form-data">
      {{ csrf_field() }} {{ method_field('POST') }}
      <br>

      @include('layouts.alert')

      @if ($errors->has('nama.*') || $errors->has('jk.*') || $errors->has('no_hp.*') || $errors->has('email.*') || $errors->has('pendidikan.*') || $errors->has('nik.*') || $errors->has('tanggal_lahir.*') || $errors->has('provinsi.*') || $errors->has('kota.*') || $errors->has('kecamatan.*') || $errors->has('alamat.*'))
        <div class="alert alert-danger" role="alert">
          <strong>Ada data yang belum diisi atau salah, mohon cek kembali.</strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif

      <!-- @if ($errors->has('portofolio'))
        <div class="alert alert-danger" role="alert">
          <strong>{{ $errors->first('portofolio') }}</strong>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      @endif -->

      <br>
      <h3>Profile Kelompok</h3>

      <div class="form-group {{ $errors->has('nama_kelompok') ? 'has-error' : '' }}">
        <label for="nama_kelompok" class="control-label">Nama Kelompok</label>
        <input type="text" class="form-control" name="nama_kelompok" value="{{ old('nama_kelompok') }}">
        @if ($errors->has('nama_kelompok'))
          <span class="help-block">
            {{ $errors->first('nama_kelompok') }}
          </span>
        @endif
      </div>

      <div class="form-group {{ $errors->has('mata_lomba') ? 'has-error' : '' }}">
        <label for="jenis_lomba" class="control-label">Bentuk inisiatif seperti apa yang akan kelompok kamu lakukan untuk menghasilkan output dan outcome, pilih bidang lomba berikut.</label>
        <div class="form-check form-check-inline">
          <input class="form-check-input detail"  type="radio" name="mata_lomba" value="aplikasi" id="aplikasi">
          <label for="aplikasi" class="form-check-label" >
            Aplikasi
          </label>

          <input class="form-check-input detail"  type="radio" name="mata_lomba" value="prakarya" id="prakarya" style="margin-left: 20px;">
          <label for="prakarya" class="form-check-label">
            Prakarya
          </label>
        </div>
        @if ($errors->has('mata_lomba'))
          <span class="help-block">
            {{ $errors->first('mata_lomba') }}
          </span>
        @endif
      </div>

      <br>
      
      <label>Catatan:</label>
      <ol>
        <li>Untuk mata lomba Aplikasi, satu kelompok harus terdiri dari 5 orang dan untuk mata lomba Prakarya, satu kelompok harus terdiri dari 3 orang.</li>
        <li>Satu kelompok harus memiliki perpaduan unsur laki-laki dan perempuan. Minimal satu orang perempuan.</li>
        <li>Satu kelompok diharapkan berdomisili di satu Kota / Kabupaten yang sama untuk mempermudah koordinasi.</li>
        <li>Wajib melampirkan salinan ktp.</li>
        <li>Nomor HP yang dicantumkan harus memiliki fitur Whatsapp.</li>
      </ol>

      <br>

      <table class="table table-bordered">
        <tr id="form-head" align="center">
          <td>No</td>
          <td>Data</td>
        </tr>
        <tr id="form-satu">
          <td style="text-align: center; vertical-align: middle;">1</td>
          <td>
            <div class="row">
              <div class="col-sm-3 form-group {{ $errors->has('nama.0') ? 'has-error' : '' }}">
                  <label for="nama" class="control-label">Nama Ketua Kelompok</label>
                  <input type="text" class="form-control" name=nama[] value="{{ old('nama.0') }}">
                  @if ($errors->has('nama.0'))
                    <span class="help-block">
                      {{ $errors->first('nama.0') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('jk.0') ? 'has-error' : '' }}">
                  <label for="jk" class="control-label">Jenis Kelamin</label>
                  <select class="form-control" name=jk[] value="{{ old('jk.0') }}">
                  <option value="{{old('jk.0')}}" @if(old('jk.0') == 'l') selected="selected"@endif> @if(old('jk.0') == '') Pilih Jenis Kelamin @elseif(old('jk.0') == 'l') Laki-laki @else Perempuan @endif</option>
                    <option value="l">Laki-Laki</option>
                    <option value="p">Perempuan</option>
                  </select>
                  @if ($errors->has('jk.0'))
                    <span class="help-block">
                      {{ $errors->first('jk.0') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('no_hp.0') ? 'has-error' : '' }}">
                  <label class="control-label">No.HP</label>
                  <input type="text" class="form-control" name=no_hp[] value="{{ old('no_hp.0') }}">
                  @if ($errors->has('no_hp.0'))
                    <span class="help-block">
                      {{ $errors->first('no_hp.0') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('email.0') ? 'has-error' : '' }}">
                  <label class="control-label">Email</label>
                  <input type="email" class="form-control" name=email[] value="{{ old('email.0') }}">
                  @if ($errors->has('email.0'))
                    <span class="help-block">
                      {{ $errors->first('email.0') }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-4 form-group {{ $errors->has('pendidikan.0') ? 'has-error' : '' }}">
                  <label class="control-label">Pendidikan</label>
                  <select class="form-control" name=pendidikan[] value="{{ old('pendidikan.0') }}">
                  <option value="{{old('pendidikan.0')}}"
                    @if(old('pendidikan.0') == 'SD') selected="selected"
                    @elseif(old('pendidikan.0') == 'SMP') selected="selected"
                    @elseif(old('pendidikan.0') == 'SMA') selected="selected"
                    @elseif(old('pendidikan.0') == 'SMK') selected="selected"
                    @elseif(old('pendidikan.0') == 'D1') selected="selected"
                    @elseif(old('pendidikan.0') == 'D2') selected="selected"
                    @elseif(old('pendidikan.0') == 'D3') selected="selected"
                    @elseif(old('pendidikan.0') == 'D4') selected="selected"
                    @elseif(old('pendidikan.0') == 'S1') selected="selected"
                    @elseif(old('pendidikan.0') == 'S2') selected="selected"
                    @else(old('pendidikan.0') == 'S3') selected="selected"
                    @endif
                    >@if(old('pendidikan.0') == '') Pilih Pendidikan
                     @elseif(old('pendidikan.0') == 'SD') SD 
                     @elseif(old('pendidikan.0') == 'SMP') SMP
                     @elseif(old('pendidikan.0') == 'SMA') SMA
                     @elseif(old('pendidikan.0') == 'SMK') SMK 
                     @elseif(old('pendidikan.0') == 'D1') D1 
                     @elseif(old('pendidikan.0') == 'D2') D2 
                     @elseif(old('pendidikan.0') == 'D3') D3
                     @elseif(old('pendidikan.0') == 'D4') D4 
                     @elseif(old('pendidikan.0') == 'S1') S1 
                     @elseif(old('pendidikan.0') == 'S2') S2 
                     @else S3 
                     @endif</option>
                    <option value="SD">SD</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA">SMA</option>
                    <option value="SMK">SMK</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="D3">D3</option>
                    <option value="D4">D4</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                  @if ($errors->has('pendidikan.0'))
                    <span class="help-block">
                      {{ $errors->first('pendidikan.0') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('nik.0') ? 'has-error' : '' }}">
                  <label class="control-label">NIK</label>
                  <input type="number" class="form-control" name=nik[] value="{{ old('nik.0') }}">
                  @if ($errors->has('nik.0'))
                    <span class="help-block">
                      {{ $errors->first('nik.0') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('tanggal_lahir.0') ? 'has-error' : '' }}">
                  <label>Tanggal Lahir</label>
                  <input type="date" class="form-control" name=tanggal_lahir[] max={{ $max }} min={{ $min }} value="{{ old('tanggal_lahir.0') }}">
                  @if ($errors->has('tanggal_lahir.0'))
                    <span class="help-block">
                      {{ $errors->first('tanggal_lahir.0') }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <table border="0" width="100%" align="center">
                    <tr>
                      <td width="33%" style="text-align:center"><label>Provinsi</label></td>
                      <td width="33%" style="text-align:center"><label>Kabupaten / Kota</label></td>
                      <td width="33%" style="text-align:center"><label>Kecamatan</label></td>
                      </tr>
                      <tr>
                      <td style="width:30%; " class="form-group" colspan="3">
                      <select style="width:30%; height:30px" name=provinsi[] class="custom-select provinsi">
                            <option value="" >-- Pilih Provinsi --</option>
                            @foreach ($provinsi as $item)
                              <option value="{{$item->id}}" @if(old('provinsi.0') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <select style="margin-left:2%; width:30%; height:30px;" name=kota[] class="custom-select kota">
                              <option value="" >-- Pilih Kota --</option>
                            @foreach ($kota as $item)
                              <option value="{{$item->id}}" @if(old('kota.0') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <select style="margin-left:3%; width:32%; height:30px;" name=kecamatan[] class="custom-select kecamatan">
                          <option value="" >-- Pilih Kecamatan --</option>
                            @foreach ($kecamatan as $item)
                              <option value="{{$item->id}}" @if(old('kecamatan.0') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                      </td>
                    </tr>
                  </table>
                  @if ($errors->has('provinsi.0') || $errors->has('kota.0') || $errors->has('Kecamatan.0'))
                    <span class="text-danger">
                      Wilayah wajib diisi.
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group {{ $errors->has('alamat.0') ? 'has-error' : '' }}">
                <label for="exampleFormControlInput1">Alamat</label>
                <textarea class="form-control" name=alamat[] rows="3">{{ old('alamat.0') }}</textarea>
                @if ($errors->has('alamat.0'))
                  <span class="help-block">
                    {{ $errors->first('alamat.0') }}
                  </span>
                @endif
            </div>
          </td>
        </tr>

        <tr id="form-dua">
          <td style="text-align: center; vertical-align: middle;">2</td>
          <td>
            <div class="row">
              <div class="col-sm-3 form-group {{ $errors->has('nama.1') ? 'has-error' : '' }}">
                  <label for="nama" class="control-label">Nama Peserta</label>
                  <input type="text" class="form-control" name=nama[] value="{{ old('nama.1') }}">
                  @if ($errors->has('nama.1'))
                    <span class="help-block">
                      {{ $errors->first('nama.1') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('jk.1') ? 'has-error' : '' }}">
                  <label for="jk" class="control-label">Jenis Kelamin</label>
                  <select class="form-control" name=jk[] value="{{ old('jk.1') }}">
                  <option value="{{old('jk.1')}}" @if(old('jk.1') == 'l') selected="selected"@endif> @if(old('jk.1') == '') Pilih Jenis Kelamin @elseif(old('jk.1') == 'l') Laki-laki @else Perempuan @endif</option>
                    <option value="l">Laki-Laki</option>
                    <option value="p">Perempuan</option>
                  </select>
                  @if ($errors->has('jk.1'))
                    <span class="help-block">
                      {{ $errors->first('jk.1') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('no_hp.1') ? 'has-error' : '' }}">
                  <label class="control-label">No.HP</label>
                  <input type="text" class="form-control" name=no_hp[] value="{{ old('no_hp.1') }}">
                  @if ($errors->has('no_hp.1'))
                    <span class="help-block">
                      {{ $errors->first('no_hp.1') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('email.1') ? 'has-error' : '' }}">
                  <label class="control-label">Email</label>
                  <input type="email" class="form-control" name=email[] value="{{ old('email.1') }}">
                  @if ($errors->has('email.1'))
                    <span class="help-block">
                      {{ $errors->first('email.1') }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-4 form-group {{ $errors->has('pendidikan.1') ? 'has-error' : '' }}">
                  <label class="control-label">Pendidikan</label>
                  <select class="form-control" name=pendidikan[] value="{{ old('pendidikan.1') }}">
                  <option value="{{old('pendidikan.1')}}"
                    @if(old('pendidikan.1') == 'SD') selected="selected"
                    @elseif(old('pendidikan.1') == 'SMP') selected="selected"
                    @elseif(old('pendidikan.1') == 'SMA') selected="selected"
                    @elseif(old('pendidikan.1') == 'SMK') selected="selected"
                    @elseif(old('pendidikan.1') == 'D1') selected="selected"
                    @elseif(old('pendidikan.1') == 'D2') selected="selected"
                    @elseif(old('pendidikan.1') == 'D3') selected="selected"
                    @elseif(old('pendidikan.1') == 'D4') selected="selected"
                    @elseif(old('pendidikan.1') == 'S1') selected="selected"
                    @elseif(old('pendidikan.1') == 'S2') selected="selected"
                    @else(old('pendidikan.1') == 'S3') selected="selected"
                    @endif
                    >@if(old('pendidikan.1') == '') Pilih Pendidikan
                     @elseif(old('pendidikan.1') == 'SD') SD 
                     @elseif(old('pendidikan.1') == 'SMP') SMP
                     @elseif(old('pendidikan.1') == 'SMA') SMA
                     @elseif(old('pendidikan.1') == 'SMK') SMK 
                     @elseif(old('pendidikan.1') == 'D1') D1 
                     @elseif(old('pendidikan.1') == 'D2') D2 
                     @elseif(old('pendidikan.1') == 'D3') D3
                     @elseif(old('pendidikan.1') == 'D4') D4 
                     @elseif(old('pendidikan.1') == 'S1') S1 
                     @elseif(old('pendidikan.1') == 'S2') S2 
                     @else S3 
                     @endif</option>
                    <option value="SD">SD</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA">SMA</option>
                    <option value="SMK">SMK</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="D3">D3</option>
                    <option value="D4">D4</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                  @if ($errors->has('pendidikan.1'))
                    <span class="help-block">
                      {{ $errors->first('pendidikan.1') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('nik.1') ? 'has-error' : '' }}">
                  <label class="control-label">NIK</label>
                  <input type="number" class="form-control" name=nik[] value="{{ old('nik.1') }}">
                  @if ($errors->has('nik.1'))
                    <span class="help-block">
                      {{ $errors->first('nik.1') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('tanggal_lahir.1') ? 'has-error' : '' }}">
                  <label>Tanggal Lahir</label>
                  <input type="date" class="form-control" name=tanggal_lahir[] max={{ $max }} min={{ $min }} value="{{ old('tanggal_lahir.1') }}">
                  @if ($errors->has('tanggal_lahir.1'))
                    <span class="help-block">
                      {{ $errors->first('tanggal_lahir.1') }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <table border="0" width="100%" align="center">
                    <tr>
                      <td width="33%" style="text-align:center"><label>Provinsi</label></td>
                      <td width="33%" style="text-align:center"><label>Kabupaten / Kota</label></td>
                      <td width="33%" style="text-align:center"><label>Kecamatan</label></td>
                      </tr>
                      <tr>
                      <td style="width:30%; " class="form-group" colspan="3">
                      <select style="width:30%; height:30px" name=provinsi[] class="custom-select provinsi">
                            <option value="" >-- Pilih Provinsi --</option>
                            @foreach ($provinsi as $item)
                              <option value="{{$item->id}}" @if(old('provinsi.1') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <select style="margin-left:2%; width:30%; height:30px;" name=kota[] class="custom-select kota">
                              <option value="" >-- Pilih Kota --</option>
                            @foreach ($kota as $item)
                              <option value="{{$item->id}}" @if(old('kota.1') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <select style="margin-left:3%; width:32%; height:30px;" name=kecamatan[] class="custom-select kecamatan">
                          <option value="" >-- Pilih Kecamatan --</option>
                            @foreach ($kecamatan as $item)
                              <option value="{{$item->id}}" @if(old('kecamatan.1') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                      </td>
                    </tr>
                  </table>
                  @if ($errors->has('provinsi.1') || $errors->has('kota.1') || $errors->has('Kecamatan.1'))
                    <span class="text-danger">
                      Wilayah wajib diisi.
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group {{ $errors->has('alamat.1') ? 'has-error' : '' }}">
                <label for="exampleFormControlInput1">Alamat</label>
                <textarea class="form-control" name=alamat[] rows="3">{{ old('alamat.1') }}</textarea>
                @if ($errors->has('alamat.1'))
                  <span class="help-block">
                    {{ $errors->first('alamat.1') }}
                  </span>
                @endif
            </div>
          </td>
        </tr>

        <tr id="form-tiga">
          <td style="text-align: center; vertical-align: middle;">3</td>
          <td>
            <div class="row">
              <div class="col-sm-3 form-group {{ $errors->has('nama.2') ? 'has-error' : '' }}">
                  <label for="nama" class="control-label">Nama Peserta</label>
                  <input type="text" class="form-control" name=nama[] value="{{ old('nama.2') }}">
                  @if ($errors->has('nama.2'))
                    <span class="help-block">
                      {{ $errors->first('nama.2') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('jk.2') ? 'has-error' : '' }}">
                  <label for="jk" class="control-label">Jenis Kelamin</label>
                  <select class="form-control" name=jk[] value="{{ old('jk.2') }}">
                  <option value="{{old('jk.2')}}" @if(old('jk.2') == 'l') selected="selected"@endif> @if(old('jk.2') == '') Pilih Jenis Kelamin @elseif(old('jk.2') == 'l') Laki-laki @else Perempuan @endif</option>
                    <option value="l">Laki-Laki</option>
                    <option value="p">Perempuan</option>
                  </select>
                  @if ($errors->has('jk.2'))
                    <span class="help-block">
                      {{ $errors->first('jk.2') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('no_hp.2') ? 'has-error' : '' }}">
                  <label class="control-label">No.HP</label>
                  <input type="text" class="form-control" name=no_hp[] value="{{ old('no_hp.2') }}">
                  @if ($errors->has('no_hp.2'))
                    <span class="help-block">
                      {{ $errors->first('no_hp.2') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('email.2') ? 'has-error' : '' }}">
                  <label class="control-label">Email</label>
                  <input type="email" class="form-control" name=email[] value="{{ old('email.2') }}">
                  @if ($errors->has('email.2'))
                    <span class="help-block">
                      {{ $errors->first('email.2') }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-4 form-group {{ $errors->has('pendidikan.2') ? 'has-error' : '' }}">
                  <label class="control-label">Pendidikan</label>
                  <select class="form-control" name=pendidikan[] value="{{ old('pendidikan.2') }}">
                  <option value="{{old('pendidikan.2')}}"
                    @if(old('pendidikan.2') == 'SD') selected="selected"
                    @elseif(old('pendidikan.2') == 'SMP') selected="selected"
                    @elseif(old('pendidikan.2') == 'SMA') selected="selected"
                    @elseif(old('pendidikan.2') == 'SMK') selected="selected"
                    @elseif(old('pendidikan.2') == 'D1') selected="selected"
                    @elseif(old('pendidikan.2') == 'D2') selected="selected"
                    @elseif(old('pendidikan.2') == 'D3') selected="selected"
                    @elseif(old('pendidikan.2') == 'D4') selected="selected"
                    @elseif(old('pendidikan.2') == 'S1') selected="selected"
                    @elseif(old('pendidikan.2') == 'S2') selected="selected"
                    @else(old('pendidikan.2') == 'S3') selected="selected"
                    @endif
                    >@if(old('pendidikan.2') == '') Pilih Pendidikan
                     @elseif(old('pendidikan.2') == 'SD') SD 
                     @elseif(old('pendidikan.2') == 'SMP') SMP
                     @elseif(old('pendidikan.2') == 'SMA') SMA
                     @elseif(old('pendidikan.2') == 'SMK') SMK 
                     @elseif(old('pendidikan.2') == 'D1') D1 
                     @elseif(old('pendidikan.2') == 'D2') D2 
                     @elseif(old('pendidikan.2') == 'D3') D3
                     @elseif(old('pendidikan.2') == 'D4') D4 
                     @elseif(old('pendidikan.2') == 'S1') S1 
                     @elseif(old('pendidikan.2') == 'S2') S2 
                     @else S3 
                     @endif</option>
                    <option value="SD">SD</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA">SMA</option>
                    <option value="SMK">SMK</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="D3">D3</option>
                    <option value="D4">D4</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                  @if ($errors->has('pendidikan.2'))
                    <span class="help-block">
                      {{ $errors->first('pendidikan.2') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('nik.2') ? 'has-error' : '' }}">
                  <label class="control-label">NIK</label>
                  <input type="number" class="form-control" name=nik[] value="{{ old('nik.2') }}">
                  @if ($errors->has('nik.2'))
                    <span class="help-block">
                      {{ $errors->first('nik.2') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('tanggal_lahir.2') ? 'has-error' : '' }}">
                  <label>Tanggal Lahir</label>
                  <input type="date" class="form-control" name=tanggal_lahir[] max={{ $max }} min={{ $min }} value="{{ old('tanggal_lahir.2') }}">
                  @if ($errors->has('tanggal_lahir.2'))
                    <span class="help-block">
                      {{ $errors->first('tanggal_lahir.2') }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <table border="0" width="100%" align="center">
                    <tr>
                      <td width="33%" style="text-align:center"><label>Provinsi</label></td>
                      <td width="33%" style="text-align:center"><label>Kabupaten / Kota</label></td>
                      <td width="33%" style="text-align:center"><label>Kecamatan</label></td>
                      </tr>
                      <tr>
                      <td style="width:30%; " class="form-group" colspan="3">
                      <select style="width:30%; height:30px" name=provinsi[] class="custom-select provinsi">
                            <option value="" >-- Pilih Provinsi --</option>
                            @foreach ($provinsi as $item)
                              <option value="{{$item->id}}" @if(old('provinsi.2') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <select style="margin-left:2%; width:30%; height:30px;" name=kota[] class="custom-select kota">
                              <option value="" >-- Pilih Kota --</option>
                            @foreach ($kota as $item)
                              <option value="{{$item->id}}" @if(old('kota.2') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <select style="margin-left:3%; width:32%; height:30px;" name=kecamatan[] class="custom-select kecamatan">
                          <option value="" >-- Pilih Kecamatan --</option>
                            @foreach ($kecamatan as $item)
                              <option value="{{$item->id}}" @if(old('kecamatan.2') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                      </td>
                    </tr>
                  </table>
                  @if ($errors->has('provinsi.2') || $errors->has('kota.2') || $errors->has('Kecamatan.2'))
                    <span class="text-danger">
                      Wilayah wajib diisi.
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group {{ $errors->has('alamat.2') ? 'has-error' : '' }}">
                <label for="exampleFormControlInput1">Alamat</label>
                <textarea class="form-control" name=alamat[] rows="3">{{ old('alamat.2') }}</textarea>
                @if ($errors->has('alamat.2'))
                  <span class="help-block">
                    {{ $errors->first('alamat.2') }}
                  </span>
                @endif
            </div>
          </td>
        </tr>

        <tr id="form-empat">
          <td style="text-align: center; vertical-align: middle;">4</td>
          <td>
            <div class="row">
              <div class="col-sm-3 form-group {{ $errors->has('nama.3') ? 'has-error' : '' }}">
                  <label for="nama" class="control-label">Nama Peserta</label>
                  <input type="text" class="form-control" name=nama[] value="{{ old('nama.3') }}">
                  @if ($errors->has('nama.3'))
                    <span class="help-block">
                      {{ $errors->first('nama.3') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('jk.3') ? 'has-error' : '' }}">
                  <label for="jk" class="control-label">Jenis Kelamin</label>
                  <select class="form-control" name=jk[] value="{{ old('jk.3') }}">
                  <option value="{{old('jk.3')}}" @if(old('jk.3') == 'l') selected="selected"@endif> @if(old('jk.3') == '') Pilih Jenis Kelamin @elseif(old('jk.3') == 'l') Laki-laki @else Perempuan @endif</option>
                    <option value="l">Laki-Laki</option>
                    <option value="p">Perempuan</option>
                  </select>
                  @if ($errors->has('jk.3'))
                    <span class="help-block">
                      {{ $errors->first('jk.3') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('no_hp.3') ? 'has-error' : '' }}">
                  <label class="control-label">No.HP</label>
                  <input type="text" class="form-control" name=no_hp[] value="{{ old('no_hp.3') }}">
                  @if ($errors->has('no_hp.3'))
                    <span class="help-block">
                      {{ $errors->first('no_hp.3') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('email.3') ? 'has-error' : '' }}">
                  <label class="control-label">Email</label>
                  <input type="email" class="form-control" name=email[] value="{{ old('email.3') }}">
                  @if ($errors->has('email.3'))
                    <span class="help-block">
                      {{ $errors->first('email.3') }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-4 form-group {{ $errors->has('pendidikan.3') ? 'has-error' : '' }}">
                  <label class="control-label">Pendidikan</label>
                  <select class="form-control" name=pendidikan[] value="{{ old('pendidikan.3') }}">
                  <option value="{{old('pendidikan.3')}}"
                    @if(old('pendidikan.3') == 'SD') selected="selected"
                    @elseif(old('pendidikan.3') == 'SMP') selected="selected"
                    @elseif(old('pendidikan.3') == 'SMA') selected="selected"
                    @elseif(old('pendidikan.3') == 'SMK') selected="selected"
                    @elseif(old('pendidikan.3') == 'D1') selected="selected"
                    @elseif(old('pendidikan.3') == 'D2') selected="selected"
                    @elseif(old('pendidikan.3') == 'D3') selected="selected"
                    @elseif(old('pendidikan.3') == 'D4') selected="selected"
                    @elseif(old('pendidikan.3') == 'S1') selected="selected"
                    @elseif(old('pendidikan.3') == 'S2') selected="selected"
                    @else(old('pendidikan.3') == 'S3') selected="selected"
                    @endif
                    >@if(old('pendidikan.3') == '') Pilih Pendidikan
                     @elseif(old('pendidikan.3') == 'SD') SD 
                     @elseif(old('pendidikan.3') == 'SMP') SMP
                     @elseif(old('pendidikan.3') == 'SMA') SMA
                     @elseif(old('pendidikan.3') == 'SMK') SMK 
                     @elseif(old('pendidikan.3') == 'D1') D1 
                     @elseif(old('pendidikan.3') == 'D2') D2 
                     @elseif(old('pendidikan.3') == 'D3') D3
                     @elseif(old('pendidikan.3') == 'D4') D4 
                     @elseif(old('pendidikan.3') == 'S1') S1 
                     @elseif(old('pendidikan.3') == 'S2') S2 
                     @else S3 
                     @endif</option>
                    <option value="SD">SD</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA">SMA</option>
                    <option value="SMK">SMK</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="D3">D3</option>
                    <option value="D4">D4</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                  @if ($errors->has('pendidikan.3'))
                    <span class="help-block">
                      {{ $errors->first('pendidikan.3') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('nik.3') ? 'has-error' : '' }}">
                  <label class="control-label">NIK</label>
                  <input type="number" class="form-control" name=nik[] value="{{ old('nik.3') }}">
                  @if ($errors->has('nik.3'))
                    <span class="help-block">
                      {{ $errors->first('nik.3') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('tanggal_lahir.3') ? 'has-error' : '' }}">
                  <label>Tanggal Lahir</label>
                  <input type="date" class="form-control" name=tanggal_lahir[] max={{ $max }} min={{ $min }} value="{{ old('tanggal_lahir.3') }}">
                  @if ($errors->has('tanggal_lahir.3'))
                    <span class="help-block">
                      {{ $errors->first('tanggal_lahir.3') }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <table border="0" width="100%" align="center">
                    <tr>
                      <td width="33%" style="text-align:center"><label>Provinsi</label></td>
                      <td width="33%" style="text-align:center"><label>Kabupaten / Kota</label></td>
                      <td width="33%" style="text-align:center"><label>Kecamatan</label></td>
                      </tr>
                      <tr>
                      <td style="width:30%; " class="form-group" colspan="3">
                      <select style="width:30%; height:30px" name=provinsi[] class="custom-select provinsi">
                            <option value="" >-- Pilih Provinsi --</option>
                            @foreach ($provinsi as $item)
                              <option value="{{$item->id}}" @if(old('provinsi.3') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <select style="margin-left:2%; width:30%; height:30px;" name=kota[] class="custom-select kota">
                              <option value="" >-- Pilih Kota --</option>
                            @foreach ($kota as $item)
                              <option value="{{$item->id}}" @if(old('kota.3') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <select style="margin-left:3%; width:32%; height:30px;" name=kecamatan[] class="custom-select kecamatan">
                          <option value="" >-- Pilih Kecamatan --</option>
                            @foreach ($kecamatan as $item)
                              <option value="{{$item->id}}" @if(old('kecamatan.3') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                      </td>
                    </tr>
                  </table>
                  @if ($errors->has('provinsi.3') || $errors->has('kota.3') || $errors->has('Kecamatan.3'))
                    <span class="text-danger">
                      Wilayah wajib diisi.
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group {{ $errors->has('alamat.3') ? 'has-error' : '' }}">
                <label for="exampleFormControlInput1">Alamat</label>
                <textarea class="form-control" name=alamat[] rows="3">{{ old('alamat.3') }}</textarea>
                @if ($errors->has('alamat.3'))
                  <span class="help-block">
                    {{ $errors->first('alamat.3') }}
                  </span>
                @endif
            </div>
          </td>
        </tr>

        <tr id="form-lima">
          <td style="text-align: center; vertical-align: middle;">5</td>
          <td>
            <div class="row">
              <div class="col-sm-3 form-group {{ $errors->has('nama.4') ? 'has-error' : '' }}">
                  <label for="nama" class="control-label">Nama Peserta</label>
                  <input type="text" class="form-control" name=nama[] value="{{ old('nama.4') }}">
                  @if ($errors->has('nama.4'))
                    <span class="help-block">
                      {{ $errors->first('nama.4') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('jk.4') ? 'has-error' : '' }}">
                  <label for="jk" class="control-label">Jenis Kelamin</label>
                  <select class="form-control" name=jk[] value="{{ old('jk.4') }}">
                  <option value="{{old('jk.4')}}" @if(old('jk.4') == 'l') selected="selected"@endif> @if(old('jk.4') == '') Pilih Jenis Kelamin @elseif(old('jk.4') == 'l') Laki-laki @else Perempuan @endif</option>
                    <option value="l">Laki-Laki</option>
                    <option value="p">Perempuan</option>
                  </select>
                  @if ($errors->has('jk.4'))
                    <span class="help-block">
                      {{ $errors->first('jk.4') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('no_hp.4') ? 'has-error' : '' }}">
                  <label class="control-label">No.HP</label>
                  <input type="text" class="form-control" name=no_hp[] value="{{ old('no_hp.4') }}">
                  @if ($errors->has('no_hp.4'))
                    <span class="help-block">
                      {{ $errors->first('no_hp.4') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-3 form-group {{ $errors->has('email.4') ? 'has-error' : '' }}">
                  <label class="control-label">Email</label>
                  <input type="email" class="form-control" name=email[] value="{{ old('email.4') }}">
                  @if ($errors->has('email.4'))
                    <span class="help-block">
                      {{ $errors->first('email.4') }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-4 form-group {{ $errors->has('pendidikan.4') ? 'has-error' : '' }}">
                  <label class="control-label">Pendidikan</label>
                  <select class="form-control" name=pendidikan[] value="{{ old('pendidikan.4') }}">
                  <option value="{{old('pendidikan.4')}}"
                    @if(old('pendidikan.4') == 'SD') selected="selected"
                    @elseif(old('pendidikan.4') == 'SMP') selected="selected"
                    @elseif(old('pendidikan.4') == 'SMA') selected="selected"
                    @elseif(old('pendidikan.4') == 'SMK') selected="selected"
                    @elseif(old('pendidikan.4') == 'D1') selected="selected"
                    @elseif(old('pendidikan.4') == 'D2') selected="selected"
                    @elseif(old('pendidikan.4') == 'D3') selected="selected"
                    @elseif(old('pendidikan.4') == 'D4') selected="selected"
                    @elseif(old('pendidikan.4') == 'S1') selected="selected"
                    @elseif(old('pendidikan.4') == 'S2') selected="selected"
                    @else(old('pendidikan.4') == 'S3') selected="selected"
                    @endif
                    >@if(old('pendidikan.4') == '') Pilih Pendidikan
                     @elseif(old('pendidikan.4') == 'SD') SD 
                     @elseif(old('pendidikan.4') == 'SMP') SMP
                     @elseif(old('pendidikan.4') == 'SMA') SMA
                     @elseif(old('pendidikan.4') == 'SMK') SMK 
                     @elseif(old('pendidikan.4') == 'D1') D1 
                     @elseif(old('pendidikan.4') == 'D2') D2 
                     @elseif(old('pendidikan.4') == 'D3') D3
                     @elseif(old('pendidikan.4') == 'D4') D4 
                     @elseif(old('pendidikan.4') == 'S1') S1 
                     @elseif(old('pendidikan.4') == 'S2') S2 
                     @else S3 
                     @endif</option>
                    <option value="SD">SD</option>
                    <option value="SMP">SMP</option>
                    <option value="SMA">SMA</option>
                    <option value="SMK">SMK</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="D3">D3</option>
                    <option value="D4">D4</option>
                    <option value="S1">S1</option>
                    <option value="S2">S2</option>
                    <option value="S3">S3</option>
                  </select>
                  @if ($errors->has('pendidikan.4'))
                    <span class="help-block">
                      {{ $errors->first('pendidikan.4') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('nik.4') ? 'has-error' : '' }}">
                  <label class="control-label">NIK</label>
                  <input type="number" class="form-control" name=nik[] value="{{ old('nik.4') }}">
                  @if ($errors->has('nik.4'))
                    <span class="help-block">
                      {{ $errors->first('nik.4') }}
                    </span>
                  @endif
              </div>

              <div class="col-sm-4 form-group {{ $errors->has('tanggal_lahir.4') ? 'has-error' : '' }}">
                  <label>Tanggal Lahir</label>
                  <input type="date" class="form-control" name=tanggal_lahir[] max={{ $max }} min={{ $min }} value="{{ old('tanggal_lahir.4') }}">
                  @if ($errors->has('tanggal_lahir.4'))
                    <span class="help-block">
                      {{ $errors->first('tanggal_lahir.4') }}
                    </span>
                  @endif
              </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <table border="0" width="100%" align="center">
                    <tr>
                      <td width="33%" style="text-align:center"><label>Provinsi</label></td>
                      <td width="33%" style="text-align:center"><label>Kabupaten / Kota</label></td>
                      <td width="33%" style="text-align:center"><label>Kecamatan</label></td>
                      </tr>
                      <tr>
                      <td style="width:30%; " class="form-group" colspan="3">
                      <select style="width:30%; height:30px" name=provinsi[] class="custom-select provinsi">
                            <option value="" >-- Pilih Provinsi --</option>
                            @foreach ($provinsi as $item)
                              <option value="{{$item->id}}" @if(old('provinsi.4') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <select style="margin-left:2%; width:30%; height:30px;" name=kota[] class="custom-select kota">
                              <option value="" >-- Pilih Kota --</option>
                            @foreach ($kota as $item)
                              <option value="{{$item->id}}" @if(old('kota.4') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <select style="margin-left:3%; width:32%; height:30px;" name=kecamatan[] class="custom-select kecamatan">
                          <option value="" >-- Pilih Kecamatan --</option>
                            @foreach ($kecamatan as $item)
                              <option value="{{$item->id}}" @if(old('kecamatan.4') == $item->id) selected="selected"@endif>{{ $item->name }}</option>
                            @endforeach
                        </select>
                      </td>
                    </tr>
                  </table>
                  @if ($errors->has('provinsi.4') || $errors->has('kota.4') || $errors->has('Kecamatan.4'))
                    <span class="text-danger">
                      Wilayah wajib diisi.
                    </span>
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group {{ $errors->has('alamat.4') ? 'has-error' : '' }}">
                <label for="exampleFormControlInput1">Alamat</label>
                <textarea class="form-control" name=alamat[] rows="3">{{ old('alamat.4') }}</textarea>
                @if ($errors->has('alamat.4'))
                  <span class="help-block">
                    {{ $errors->first('alamat.4') }}
                  </span>
                @endif
            </div>
          </td>
        </tr>
      </table>

      <div class="form-group {{ $errors->has('ktp') ? 'has-error' : '' }}">
        <label for="ktp">Upload KTP</label>
        <input type="file" class="form-control" name="ktp" id="ktp" value="{{ old('nama_kelompok') }}">
        @if ($errors->has('ktp'))
          <span class="help-block">{{ $errors->first('ktp') }}</span>
        @endif
        <div style="line-height: 1.2">
          <span style="color: red; font-size: 14px;">*data ktp dijadikan dalam satu file (<i>.pdf</i>) untuk 1 kelompok.<a href="{{ url('/pendaftaran/pdf/index') }}" target="_blank"><b>klik untuk melihat contoh</b></a></span>
          <p style="color: red; font-size: 14px;">*max file size 5mb</p>
        </div>
      </div>

      <!-- <div class="form-group">
        <label for="portofolio">Upload Portofolio</label>
        <input type="file" class="form-control" name="portofolio" id="portofolio">
        <div style="line-height: 1.2">
          <span style="color: red; font-size: 14px;">*file portofolio harus (<i>.pdf</i>)</span>
          <p style="color: red; font-size: 14px;">*max file size 10mb</p>
        </div>
        @if ($errors->has('portofolio'))
          <span class="text-danger">{{ $errors->first('portofolio') }}</span>
        @endif
      </div>

      <div class="form-group">
          <label for="subjek_portofolio">Subjek Portofolio</label>
          <textarea class="form-control" name="subjek_portofolio" id="subjek_portofolio" rows="3"></textarea>
      </div> -->

      <br>

      <h3>Gambaran Inisatif</h3>
      
      <br>

      <label>Catatan:</label>
      <ol>
        <li>Uraian tentang permasalahan pemajuan kebudayaan di Indonesia dapat merujuk ke Pokok Pikiran Kebudayaan Daerah Kabupaten/Kota dan Provinsi. Dokumen-dokumen ini dapat diakses di <a href="http://kongres.kebudayaan.id/pra-kongres-kebudayaan-2018/" target="_blank">http://kongres.kebudayaan.id/pra-kongres-kebudayaan-2018/</a></li>
        <li><b>Pemajuan Kebudayaan</b> adalah upaya meningkatkan ketahanan budaya dan kontribusi budaya Indonesia di tengah peradaban dunia melalui Pelindungan, Pengembangan, Pemanfaatan, dan Pembinaan Kebudayaan.</li>
        <li><b>Pelindungan</b> adalah upaya menjaga keberlanjutan Kebudayaan yang dilakukan dengan cara inventarisasi, pengamanan, pemeliharaan, penyelamatan, dan publikasi.</li>
        <li><b>Pengembangan</b> adalah upaya menghidupkan ekosistem Kebudayaan serta meningkatkan, memperkaya, dan menyebarluaskan Kebudayaan.</li>
        <li><b>Pemanfaatan </b> adalah upaya pendayagunaan Objek Pemajuan Kebudayaan untuk menguatkan ideologi, politik, ekonomi, sosial, budaya, pertahanan, dan keamanan dalam mewujudkan tujuan nasional.</li>
        <li><b>Pembinaan</b> adalah upaya pemberdayaan Sumber Daya Manusia Kebudayaan, lembaga Kebudayaan, dan pranata Kebudayaan dalam meningkatkan dan memperluas peran aktif dan inisiatif masyarakat.</li>
        <li><b>Aplikasi</b> adalah piranti lunak (software) yang dapat menjawab tantangan pemajuan kebudayaan, misalnya aplikasi pelaporan persekusi, game online, aplikasi match-making tempat pertunjukan, dan lain-lain.</li>
        <li><b>Prakarya</b> adalah kriya fisik atau piranti keras (hardware) yang dapat menjawab tantangan pemajuan kebudayaan, misalnya mesin pemindai manuskrip lontar, board game bertema sejarah, robot pembaca puisi, dan lain-lain.</li>
        <li><b>Pendekatan STEAM</b> adalah pendekatan pemajuan kebudayaan yang memadukan aspek ilmu pengetahuan, teknologi, rekayasa (engineering), seni dan matematika.</li>
      </ol>
      
      <br>

      <table class="table table-bordered">
        <tr>
          <td width=50% style="font-size: 16px;">Apa sih pemajuan kebudayaan menurutmu?</td>
          <td>
            <div class="form-group {{ $errors->has('pertanyaan1') ? 'has-error' : '' }}">
              <textarea class="form-control" name="pertanyaan1" rows="3">{{ old('pertanyaan1') }}</textarea>
              @if ($errors->has('pertanyaan1'))
                <span class="help-block">
                  {{ $errors->first('pertanyaan1') }}
                </span>
              @endif
            </div>
          </td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Apa masalah pemajuan kebudayaan di sekitarmu yang mau kamu pecahkan?</td>
          <td>
            <div class="form-group {{ $errors->has('pertanyaan2') ? 'has-error' : '' }}">
              <textarea class="form-control" name="pertanyaan2" rows="3">{{ old('pertanyaan2') }}</textarea>
              @if ($errors->has('pertanyaan2'))
                <span class="help-block">
                  {{ $errors->first('pertanyaan2') }}
                </span>
              @endif
            </div>
          </td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Deskripsikan ide pemecahan masalah kamu dalam 100 kata</td>
          <td>
            <div class="form-group {{ $errors->has('pertanyaan3') ? 'has-error' : '' }}">
              <textarea class="form-control" name="pertanyaan3" rows="3">{{ old('pertanyaan3') }}</textarea>
              @if ($errors->has('pertanyaan3'))
                <span class="help-block">
                  {{ $errors->first('pertanyaan3') }}
                </span>
              @endif
            </div>
          </td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Langkah kerja apa yang akan kelompok kamu lakukan untuk memecahkan masalah tersebut?</td>
          <td>
            <div class="form-group {{ $errors->has('pertanyaan4') ? 'has-error' : '' }}">
              <textarea class="form-control" name="pertanyaan4" rows="3">{{ old('pertanyaan4') }}</textarea>
              @if ($errors->has('pertanyaan4'))
                <span class="help-block">
                  {{ $errors->first('pertanyaan4') }}
                </span>
              @endif
            </div>
          </td>
        </tr>

        <tr>
          <td width=50% style="font-size: 16px;">Apa hasil akhir dan dampak dari pemecahan masalah tersebut? </td>
          <td>
            <div class="form-group {{ $errors->has('pertanyaan5') ? 'has-error' : '' }}">
              <textarea class="form-control" name="pertanyaan5" rows="3">{{ old('pertanyaan5') }}</textarea>
              @if ($errors->has('pertanyaan5'))
                <span class="help-block">
                  {{ $errors->first('pertanyaan5') }}
                </span>
              @endif
            </div>
          </td>
        </tr>
      </table>

      <br>

      <div class="form-group">
        <!-- Button trigger modal -->
        <button type="submit" class="form-control btn btn-primary">Submit</button>
      </div>
    </form>
  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    
    <script type="text/javascript">
      $('#exampleModal').on('show.bs.modal', function (event) {
        var top = $("body").scrollTop(); $("body").css('position','fixed').css('overflow','hidden').css('top',-top).css('width','100%');
        
      }).on("hide.bs.modal", function () {
        var top = $("body").position().top; $("body").css('position','relative').css('overflow','visible');
      });

      $(document).ready(function(){
        $(document).on('change','.provinsi',function(){
          var id_pro = $(this).val();
          var div = $(this).parent();
          var op = "";

          console.log(div);

          $.ajax({
            type : 'get',
            url : "{{ URL::to('findkota') }}",
            data : {'id':id_pro},
            dataType : 'json',
            success:function(data){
            op+='<option value="0">Pilih Kota</option>';
            for(var i = 0; i<data.length; i++){
              op+='<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }

            div.find('.kota').html("");
            div.find('.kota').append(op);
            },
            error:function(){

            }
          });
        });
        $(document).on('change','.kota',function(){
          var id_kec = $(this).val();
          var div = $(this).parent();
          var op = "";

          $.ajax({
            type : 'get',
            url : "{{ URL::to('findkecamatan') }}",
            data : {'id':id_kec},
            dataType : 'json',
            success:function(data){
              op+='<option value="0">Pilih Kecamatan</option>';
            for(var i = 0; i<data.length; i++){
              op+='<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }

            div.find('.kecamatan').html("");
            div.find('.kecamatan').append(op);
            },
            error:function(){

            }
          });
        });
      });
      
      $(document).ready(function(){
        $("#form-head").css("display","none");
        $("#form-satu").css("display","none");
        $("#form-dua").css("display","none");
        $("#form-tiga").css("display","none");
        $("#form-empat").css("display","none");
        $("#form-lima").css("display","none");
          //Menghilangkan form-input ketika pertama kali dijalankan
        $(".detail").click(function(){ //Memberikan even ketika class detail di klik (class detail ialah class radio button)
          if ($("input[name='mata_lomba']:checked").val() == "aplikasi" ) { //Jika radio button "berbeda" dipilih maka tampilkan form-inputan
            $("#form-head").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
            $("#form-satu").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
            $("#form-dua").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
            $("#form-tiga").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
            $("#form-empat").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
            $("#form-lima").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
          } else {
            $("#form-head").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
            $("#form-satu").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
            $("#form-dua").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
            $("#form-tiga").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
            $("#form-empat").slideUp("fast"); //Efek Slide Down (Menampilkan Form Input)
            $("#form-lima").slideUp("fast"); //Efek Slide Down (Menampilkan Form Input)
          }
        });
      });
    </script>

    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
  </div>
@endsection