<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>KBKM</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  <!-- Favicons -->
  <link href="{{ asset('assets/img/Logo KBKM-01.png') }}" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('assets/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset('assets/lib/nivo-slider/css/nivo-slider.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/lib/owlcarousel/owl.carousel.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/lib/owlcarousel/owl.transitions.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/lib/animate/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/lib/venobox/venobox.css') }}" rel="stylesheet">

  <!-- Nivo Slider Theme -->
  <link href="{{ asset('assets/css/nivo-slider-theme.css') }}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

  <!-- Responsive Stylesheet File -->
  <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
</head>

<body data-spy="scroll" data-target="#navbar-example">

  <!-- <div id="preloader"></div> -->

  <header>
    <!-- header-area start -->
    <div id="sticker" class="header-area" style="min-height:15%; background-color: #f5f4f4;">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="" style=" float:left; height:2px;"> 
              <img src="{{ asset('assets/img/logo biru-01.png') }}" alt="" width="8%" height="" style= "margin-left: -0%; margin-top: 0%; margin-bottom: 0%;"  >        
            </div>
            <!-- Navigation -->
            <nav class="navbar navbar-right">
           
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header" style="width: 100%;">
                 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Brand -->
                
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse main-menu bs-example-navbar-collapse-1" id="navbar-example">


                <ul class="nav navbar-nav navbar-right" style="float:right; position:relative;">
                  <li class="active">
                    <a class="page-scroll" href="/">Beranda</a>
                  </li>
                </ul>
              </div>
              <!-- navbar-collapse -->
            </nav>
            <!-- END: Navigation -->
          </div>
        </div>
      </div>
    </div>
    <!-- header-area end -->
  </header>
  <!-- header end -->

  @yield('content')

<!-- Start Footer bottom Area -->
<footer>
  <div class="footer-area">
    <div class="container">
      <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-12">
          <div class="footer-content">
            <div class="footer-head">
              <div class="footer-logo">
              <img src="{{ asset('assets/img/Logo KBKM-01.png') }}" alt="" width="100%">
            </div>
          </div>
          </div>
        </div>
        <!-- end single footer -->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="footer-content">
            <div class="footer-head" style="font-size: 14px;">
              <h4>Informasi:</h4>
              <p>
                Direktorat Pembinaan Tenaga dan Lembaga Kebudayaan, Direktorat Jenderal Kebudayaan Republik Indonesia
                <br>
                Komplek Kemdikbud, Gedung E Lt.9
                <br>
                Jln. Jenderal Sudirman Senayan Jakarta 10270
                <br>
                <span>Tel:</span> (021) 5725540
                <span>Fax:</span> (021) 5725539
              </p>  
            </div>
          </div>
        </div>
        <!-- end single footer -->

           <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="footer-content">
              <div class="footer-head">
              <div class="footer-logo">
                <div class="footer-contacts" style="line-height: 1; font-size: 14px;">
                  <p><span>Narahubung KBKM 2020</span></p>
                  <p><span>Tita:</span> +62 812-9788-5105</p>
                  <p><span>Rizky:</span> +62 857-8036-0055</p>
                </div>
              </div>
            </div>
          <div class="footer-icons" style="margin-bottom: 40px;">
            <ul>
              <li>
                <a href="https://web.facebook.com/budayasaya/?_rdc=1&_rdr"><i class="fa fa-facebook"></i></a>
              </li>
              <li>
                <a href="https://twitter.com/budayasaya?lang=id"><i class="fa fa-twitter"></i></a>
              </li>
              <li>
                <a href="https://www.youtube.com/user/kebudayaanindonesia"><i class="fa fa-youtube"></i></a>
              </li>
              <li>
                <a href="https://www.instagram.com/budayasaya/?hl=id"><i class="fa fa-instagram"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                Hak Cipta &copy; <strong>Kementerian Pendidikan dan Kebudayaan 2020</strong>
              </p>
            </div>
          </div>
        </div>
      </div>
  </div>
</footer>

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="{{ asset('assets/lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/lib/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/lib/owlcarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/lib/venobox/venobox.min.js') }}"></script>
  <script src="{{ asset('assets/lib/knob/jquery.knob.js') }}"></script>
  <script src="{{ asset('assets/lib/wow/wow.min.js') }}"></script>
  <script src="{{ asset('assets/lib/parallax/parallax.js') }}"></script>
  <script src="{{ asset('assets/lib/easing/easing.min.js') }}"></script>
  <script src="{{ asset('assets/lib/nivo-slider/js/jquery.nivo.slider.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/lib/appear/jquery.appear.js') }}"></script>
  <script src="{{ asset('assets/lib/isotope/isotope.pkgd.min.js') }}"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <script src="{{ asset('assets/js/main.js') }}"></script>
  </body>
</html>