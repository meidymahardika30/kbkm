@extends('admin.layouts.layout')

@section('content')  

@if(session('message_store'))
  <script>alert('{{session('message_store')}}')</script>
@endif

<main>
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-md-4">
                <a href="{{ URL('admin/user/view') }}" class="btn btn-success"><i class="fa fa-plus"></i> User</a> <br>
            </div>
        </div>    
        <br>
        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-table mr-1"></i>Admin KBKM</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead align="center">
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th colspan="2">Action</th>
                            </tr>
                        </thead>
                        <?php
                        $no = 1;
                        ?>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td align="center">{{ $no++ }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{!! $item->email !!}</td>
                                <td align="center">
                                    <!-- <a href="{{ URL('admin/user/edit/'. $item->id) }}" class="btn btn-default"><i class="fa fa-edit"></i> edit</a>  -->
                                    
                                    <a href="{{ URL('admin/user/delete/'. $item->id) }}" onclick="return myFunction();" class="btn btn-default"><i class="fa fa-trash"></i> hapus</a>
                                    </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>     
</main>
<script>
      function myFunction() {
          if(!confirm("Apakah anda yakin akan menghapus ini ?"))
          event.preventDefault();
      }
</script>

@endsection

