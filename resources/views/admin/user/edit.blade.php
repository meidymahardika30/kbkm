@extends('admin.layouts.layout')

@section('content')  
<main>
    <div class="container-fluid">
        <br>
        <div class="card mb-2">
            <div class="card-header"><i class="fas fa-table mr-1"></i>Form Tambah Berita Arsip 2019</div>
            <div class="card-body">
            @foreach($data as $item)
            <form method="POST" action="{{ URL('admin/user/update/'. $item->id) }}" enctype="multipart/form-data">
                        @csrf {{ method_field('PATCH') }}

                        <div class="form-group ">
                            <label for="name" class="col-md-4 ">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $item->name }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="email" class="col-md-4 ">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $item->email }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group ">
                            <label for="password" class="col-md-4">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" value="{{ $pass}}">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                                <input type="checkbox" onclick="myFunction()"> Show Password
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                    @endforeach
                </div>
            </div>
        </div>
    </div>     
</main>
<script>
function myFunction(){
    var x = document.getElementById('password');
    if(x.type === "password") {
        x.type = "text";
    }else {
        x.type = "password";
    }
}
</script>
@endsection

