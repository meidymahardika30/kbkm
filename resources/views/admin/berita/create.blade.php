@extends('admin.layouts.layout')

@section('content')  
<main>
    <div class="container-fluid">
        <br>
        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-table mr-1"></i>Form Tambah Berita</div>
                <div class="card-body">
                <form method="POST" action="{{ URL('/admin/berita/store') }}" enctype="multipart/form-data">
                    @csrf
                    <label class="col-sm-3 control-label">Judul Berita</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="title" name="title" required>
                    </div>
                    <br>
                    <label class="col-sm-3 control-label">Isi Berita</label>
                    <div class="col-sm-12">
                        <textarea name="konten" id="konten" class="form-control" rows="10" cols="50" required></textarea>
                    </div>
                    <br>
                    <label for="inputName" class="col-sm-3 control-label">Gambar</label>
                    <div class="col-sm-12">
                        <input accept="image/*"  id="uploadImage" type="file" name="foto" onChange="PreviewImage();" class="form-control" required/>
                    </div>
                    
                    <br>

                    <div class="col-sm-12">
                        <button type="submit" class="form-control btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>     
</main>
@endsection

