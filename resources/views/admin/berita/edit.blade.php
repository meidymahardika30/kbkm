@extends('admin.layouts.layout')

@section('content')  
<main>
    <div class="container-fluid">
        <br>
        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-pen mr-1"></i> Form Edit Berita</div>
                <div class="card-body">
                @foreach($data as $item)
                    <form method="POST" action="{{ URL('admin/berita/update/'. $item->id) }}" enctype="multipart/form-data">
                        @csrf {{ method_field('PATCH') }}
                        <div class="row">
                            <label class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control has-error" id="title" name="title" value="{{ $item->title }}" required="">
                            </div>

                            <label class="col-sm-3 control-label">Isi Konten</label>
                            <div class="col-sm-12">
                                <textarea name="konten" id="konten" class="form-control" required="">{{ $item->konten }}</textarea>
                            </div>

                            <label class="col-sm-3 control-label">Image <span style="color: red;">(* pilih gambar ulang)</span></label>
                            <div class="col-sm-12">
                                <input accept="image/*"  id="uploadImage" type="file" name="foto" onChange="PreviewImage();" required="" class="form-control" />
                            </div>
                            <br>
                            <br>
                            <div class="col-sm-6">
                                <a href="{{ URL('admin/berita/') }}" type="button" class="form-control btn btn-warning">Kembali</a>
                            </div>
                            <div class="col-sm-6">
                                <button type="submit" class="form-control btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                @endforeach
            </div>
        </div>
    </div>     
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{asset('assets/js/kelompok.js')}}"></script>
@endsection

