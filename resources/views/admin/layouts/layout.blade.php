<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Admin - KBKM</title>
        <link href="{{ asset('assets/img/Logo KBKM-01.png') }}" rel="icon">
        <link href="{{ asset('assets/css/styles.css') }}" rel="stylesheet" />
        <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="index.html">ADMIN - KBKM</a><button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button
            ><!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
                <!-- <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div> -->
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </nav>

        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Home</div>

                            <a class="nav-link" href="{{URL('/admin/dashboard')}}"
                                ><div class="sb-nav-link-icon"><i class="fas fa-bars mr-2"></i></div>
                                Dashboard</a
                            >

                            <a class="nav-link" href="{{ URL('/admin/peserta') }}"
                                ><div class="sb-nav-link-icon"><i class="fas fa-user mr-2"></i></div>
                                Data Peserta</a
                            >

                            <a class="nav-link" href="{{ URL('/admin/kelompok') }}"
                                ><div class="sb-nav-link-icon"><i class="fas fa-users mr-1"></i></div>
                                Data Kelompok</a
                            >

                            <a class="nav-link" href="{{ URL('/admin/balai') }}"
                                ><div class="sb-nav-link-icon"><i class="fas fa-book mr-2"></i></div>
                                Data BPNB</a
                            >

                            <a class="nav-link" href="{{ URL('/admin/berita') }}"
                                ><div class="sb-nav-link-icon"><i class="fas fa-newspaper mr-1"></i></div>
                                Berita</a
                            >

                            <a class="nav-link" href="{{ URL('/admin/arsip') }}"
                                ><div class="sb-nav-link-icon"><i class="fas fa-newspaper mr-1"></i></div>
                                Arsip 2019</a
                            >

                            <a class="nav-link" href="{{ URL('/admin/user') }}"
                                ><div class="sb-nav-link-icon"><i class="fas fa-newspaper mr-1"></i></div>
                                User</a
                            >

                            <div class="sb-sidenav-menu-heading">Report</div>

                            <a class="nav-link" href="{{ URL('/admin/report/peserta?nomenklatur=&wilayah=&dari=&sampai=') }}"
                                ><div class="sb-nav-link-icon"><i class="fas fa-file"></i></div>
                                Report Peserta</a
                            >

                            <a class="nav-link" href="{{ URL('/admin/report/kelompok?mata_lomba=&mulai=&akhir=') }}"
                                ><div class="sb-nav-link-icon"><i class="fas fa-file"></i></div>
                                Report Kelompok</a
                            >
                            
                            <!-- <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts"
                                ><div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Report
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                            ></a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="{{ URL('landingpage/admin/pendaftar') }}">Data Seluruh Pendaftar</a></nav>
                            </div>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="{{ URL('landingpage/admin/kelompok') }}">Data Seluruh Kelompok</a></nav>
                            </div> -->
                            <!-- <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayoutss" aria-expanded="false" aria-controls="collapseLayouts"
                                ><div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div> 
                                Berita
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                            ></a>
                            <div class="collapse" id="collapseLayoutss" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="{{ URL('landingpage/admin/berita') }}">Konten Berita</a></nav>
                            </div> -->
                        </div>
                    </div>
                </nav>
            </div>

            <div id="layoutSidenav_content">
                @yield("content")
            </div>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{!! asset('assets/js/scripts.js') !!}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('assets/demo/chart-bar-demo.js') }}"></script>
        <script src="{{ asset('assets/demo/chart-pie-demo.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
        <script src="{{ asset('assets/demo/datatables-demo.js') }}"></script>
    
        <!-- CK Editor -->
        <script src="{{asset('assets/js/kelompok.js')}}"></script>
        <script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
        <script>
            var konten = document.getElementById("konten");
            CKEDITOR.replace(konten,{
                language:'en-gb'
            });
            CKEDITOR.config.allowedContent = true;

            var ctx = document.getElementById('myChart').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['18', '19', '20', '21','22', '23','24','25'],
                    datasets: [{
                        label: '# of Votes',
                        data: [12, 19, 3, 5, 2, 3, 5, 7],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(255, 99, 132, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
            </script> 
    </body>
</html>
