@extends('admin.layouts.layout')

@section('content')
<main>
    <div class="container-fluid">
        <br>
        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-table mr-1"></i>Data Peserta</div>
            <div class="card-body">
            
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead align="center">
                            <tr>
                                <th>No</th>
                                <th>Nama Kelompok</th>
                                <th>Nama Peserta</th>
                                <th>Jenis Kelamin</th>
                                <th>No.HP</th>
                                <th>Email</th>
                                <th>Pendidikan</th>
                                <th>NIK</th>
                                <th>Tanggal Lahir</th>
                                <th>Alamat</th>
                            </tr>
                        </thead>
                        <?php
                        $no = 1;
                        ?>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td align="center">{{ $no++ }}</td>
                                <td>{!! GetNamaKelompok($item->id_kelompok) !!}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->jk == "l" ? "Laki-Laki" : "Perempuan" }}</td>
                                <td>{{ $item->no_hp }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->pendidikan }}</td>
                                <td>{{ $item->nik }}</td>
                                <td>{{ $item->tanggal_lahir }}</td>
                                <td>{{$item->alamat}}, {!! GetKecamatan($item->id_kecamatan) !!}, {!! GetKota($item->id_kota) !!}, {!! GetProvinsi($item->id_provinsi) !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
@endsection