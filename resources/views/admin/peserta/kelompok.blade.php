@extends('admin.layouts.layout')

@section('content')          
<main>
  <div class="container-fluid">
      <h1 class="mt-4">Daftar Seluruh Kelompok</h1>
      <ol class="breadcrumb mb-4">
      @yield("content")
          <li class="breadcrumb-item active">Rekapan semua data kelompok </li>
      </ol>
      <div class="card mb-4">
          <div class="card-header"><i class="fas fa-table mr-1"></i>DataTable Kelompok</div>
          <div class="card-body">
          <label for="">Filter By Mata Lomba</label>
          <form action="">
          <select name="mata_lomba" id="mata_lomba" class="form-control" style="width:50%;">
            <option value="aplikasi">Aplikasi</option>
            <option value="prakarya">Prakarya</option>
          </select><br>
          <button class="btn btn-success" style="margin-left:45%;"> Cari</button>
          </form>
          <br>
              <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                          <tr>
                          <th>No</th>
                          <th>Nama Kelompok</th>
                          <th>Mata Lomba</th>
                          <th>KTP</th>
                          <th>Portofolio</th>
                          <th>Subjek Portofolio</th>
                          <th></th>
                          </tr>
                      </thead>
                      <tfoot>
                          <tr>
                          <th>No</th>
                          <th>Nama Kelompok</th>
                          <th>Mata Lomba</th>
                          <th>KTP</th>
                          <th>Portofolio</th>
                          <th>Subjek Portofolio</th>
                          <th></th>
                          </tr>
                      </tfoot>
                      <?php
                      $no = 1;
                      ?>
                      <tbody>
                      @foreach($data_kel as $k)
                          <tr>
                              <td>{{ $no++ }}</td>
                              <td>{{ $k->nama_kelompok }}</td>
                              <td>{{ $k->mata_lomba }}</td>
                              <td>{{ $k->ktp }}</td>
                              <td>{{ $k->portofolio }}</td>
                              <td>{{ $k->subjek_portofolio }}</td>
                              <td>
                              <a href="{{ URL('landingpage/kelompok/detail/'. $k->id)}}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                              </td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
    <!-- Modal -->
    <input id="url" type="hidden" value="{{ \Request::url() }}">

        <!-- MODAL SECTION -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Masukan Data</h4>
              </div>
              <div class="modal-body">
                <form id="frmProducts" name="frmProducts" class="form-horizontal" novalidate="">
                  <div class="form-group error">
                    <label for="inputName" class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control has-error" id="name" name="name" placeholder="Masukan name" value="">
                    </div>
                  </div>
                  <div class="form-group">
                   <label for="inputDetail" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="email" name="email" placeholder="Masukan email" value="">
                    </div>
                  </div>
                  <div class="form-group">
                   <label for="inputDetail" class="col-sm-3 control-label">Role</label>
                    <div class="col-sm-9">
                      <select id="role" name="role" class="form-control">
                      	<option>-- Pilih --</option>
                      	<option value="User NFI Umum">User NFI Umum</option>
                      	<option value="User NFI Maklon">User NFI Maklon</option>
                      	<option value="User Maklon">User Maklon</option>
                      	<option value="User QC NFI">User QC NFI</option>
                        <option value="NFI Maklon Manager">NFI Maklon Manager</option>
                      	<option value="Administrator">Administrator</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                   <label for="inputDetail" class="col-sm-3 control-label" id="lblpas">Password</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" id="password" name="password" placeholder="Masukan password" value="">
                    </div>
                  </div>
                  <div class="form-group">
                   <label for="inputDetail" class="col-sm-3 control-label" id="lblpass">perusahaan</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="perusahaan" name="perusahaan" placeholder="Masukan perusahaan" value="">
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn-save" value="add">Save Changes</button>
                <input type="hidden" id="product_id" name="product_id" value="0">
              </div>
            </div>
          </div>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2019</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
                <script src="{{asset('js/kelompok.js')}}"></script>
            @endsection  