@extends('admin.layouts.layout')

@section('content')  

@if(session('message_store'))
  <script>alert('{{session('message_store')}}')</script>
@endif

<main>
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-md-4">
                <a href="{{ URL('admin/arsip/create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Berita Arsip 2019</a> <br>
            </div>
        </div>    
        <br>
        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-table mr-1"></i>Arsip 2019</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead align="center">
                            <tr>
                                <th>No</th>
                                <th>Judul</th>
                                <th>Isi Konten</th>
                                <th>Foto</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <?php
                        $no = 1;
                        ?>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td align="center">{{ $no++ }}</td>
                                <td>{{ $item->title }}</td>
                                <td>{!! $item->konten !!}</td>
                                <td>{{ $item->foto }}</td>
                                <td>{{ $item->tgl }}</td>
                                <td align="center">
                                    <a href="{{ URL('admin/arsip/edit/'. $item->id) }}" class="btn btn-default"><i class="fa fa-edit"></i> edit</a> 
                                    
                                    <a href="{{ URL('admin/arsip/delete/'. $item->id) }}" onclick="return myFunction();" class="btn btn-default"><i class="fa fa-trash"></i> hapus</a>
                                    </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>     
</main>
<script>
      function myFunction() {
          if(!confirm("Apakah anda yakin akan menghapus ini ?"))
          event.preventDefault();
      }
</script>

@endsection

