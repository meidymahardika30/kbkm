@extends('layouts.header')

@section('content')
<style>
.more{
    display :none;
}
</style>
    <div class="container" style="margin-top:10%;">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Arsip 2019</h2>
          </div>
        </div>
      </div>
    </div>
 <!-- Start Arsip area -->
 @foreach($data as $index => $item)
  <div id="about" class="about-area area-padding">
    <div class="container">
      <div class="row">
        <!-- single-well start-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-left">
            <div class="single-well">
              <a href="#">
              <img src="{{ asset('storage/admin/arsip/'.$item->title.'/'.$item->foto) }}" alt="{{ $item->title }}">
              </a>
            </div>
          </div>
        </div>
        <!-- single-well end-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-middle">
            <div class="single-well">
              <a href="#">
                <h4 class="sec-head">{{$item->title}}</h4>
              </a>
              <span class="date-type">
                <i class="fa fa-calendar"></i>{{ $item->tgl }}
              </span>
              <p>
              ...
              </p>
              <a href="{{URL('/arsip/detail/'.$item->id)}}" class="btn btn-info">Read More</a>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  @endforeach
  <!-- End Arsip area -->
<!-- Start Arsip area -->
<script>
    $(document).ready(function(){
        $("#myBtn").click(function(){
            $(this).prev().toggle();
        });
    });
</script>






   
@endsection