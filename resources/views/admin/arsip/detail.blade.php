@extends('layouts.header')

@section('content')
<style>
.more{
    display :none;
}
</style>
    <div class="container" style="margin-top:10%;">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Arsip 2019</h2>
          </div>
        </div>
      </div>
    </div>
 <!-- Start Arsip area -->
 @foreach($data as $index => $item)
  <div id="about" class="about-area area-padding">
    <div class="container">
      <div class="row">
        <!-- single-well start-->
        <div class="col-md-12">
          
              <a href="#">
              <img style="margin-left:25%;" src="{{ asset('storage/admin/arsip/'.$item->title.'/'.$item->foto) }}" alt="{{ $item->title }}" height="50%" width="50%">
              </a>
              <a href="#">
                <h4 style="margin-left:43%;" class="sec-head">{{$item->title}}</h4>
              </a>
              <br>
              <span class="date-type" style="margin-left:45%;">
                <i class="fa fa-calendar"></i>{{ $item->tgl }}
              </span>
            
        </div>
        <!-- single-well end-->
        <div class="col-md-12">
          

              <p  style="text-align:justify; margin-left:50%;">
              {!! $item->konten !!}
              </p>
        </div>
        <!-- End col-->
        <a style="margin-left:48%;" href="{{URL('/arsip')}}" class="btn btn-info">Back</a>
      </div>
    </div>
  </div>
  @endforeach
  <!-- End Arsip area -->
<!-- Start Arsip area -->
<script>
    $(document).ready(function(){
        $("#myBtn").click(function(){
            $(this).prev().toggle();
        });
    });
</script>






   
@endsection