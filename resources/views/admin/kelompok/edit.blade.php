@extends('admin.layouts.layout')

@section('content')       

@if(session('message_update'))
  <script>alert('{{session('message_store')}}')</script>
@endif

<main>
  <div class="container-fluid">
    <br>
    <div class="card mb-4">
      <div class="card-header"><i class="fas fa-table mr-1"></i>Data Kelompok</div>
      <div class="card-body">
      <div class="container area-padding">
        @foreach($kelompok as $row)	
        <form method="POST" action="{{ URL('admin/kelompok/update/'. $row->id) }}" enctype="multipart/form-data">
                        @csrf {{ method_field('PATCH') }}

            @include('layouts.alert')

            <h4>Profil Kelompok</h4>
            <br>
            <div class="table-responsive">
            <table class="table table-bordered">
              <tr>
                  <td colspan="12">Nama Kelompok: <input type="text" class="form-control" name="nama_kelompok" value="{{ $row->nama_kelompok }}"></td>
              </tr>
              <tr>
                  <td colspan="12">Nama Ketua: <b>{{ $row->ketua }}</b></td>
              </tr>
              <tr>
                  <td colspan="12">Mata lomba yang dipilih: <b>{{ $row->mata_lomba }}</b></td>
              </tr>
              <tr align="center">
                  <td align="center">No</td>
                  <td>Nama Peserta</td>
                  <td>Jenis Kelamin</td>
                  <td>No. HP</td>
                  <td>Email</td>
                  <td>Pendidikan</td>
                  <td>NIK</td>
                  <td>Tanggal Lahir</td>
                  <td colspan="4">Alamat</td>
              </tr>
            <?php $i = 1; ?>
            @foreach($pendaftar as $item)
              <tr>
                  <td>{{ $i++ }}</td>
                  <td>{{ $item->nama }}</td>
                  <td>{{ $item->jk == 'l' ? 'Laki-Laki' : 'Perempuan' }}</td>
                  <td>{{ $item->no_hp }}</td>
                  <td>{{ $item->email }}</td>
                  <td>{{ $item->pendidikan }}</td>
                  <td>{{ $item->nik }}</td>
                  <td>{{ $item->tanggal_lahir }}</td>
                  <td>{{ $item->alamat }}, {!! GetKecamatan($item->id_kecamatan) !!}, {!! GetKota($item->id_kota) !!}, {!! GetProvinsi($item->id_provinsi) !!}</td>
              </tr>
            @endforeach
            </table>
            </div>
            <td><a href="{{ URL('admin/pendaftar/edit/'. $row->id) }}" class="btn btn-info">Edit</a></td>
            <br>
            <h3>Gambaran Inisatif</h3>
            <br>

            <table class="table table-bordered">
              <tr>
                <td width=50% style="font-size: 16px;">Apa sih pemajuan kebudayaan menurutmu?</td>
                <td><b> <input type="text" class="form-control" name="pertanyaan1" value="{{ $row->pertanyaan1 }}"></b></td>
              </tr>

              <tr>
                <td width=50% style="font-size: 16px;">Apa masalah pemajuan kebudayaan di sekitarmu yang mau kamu pecahkan?</td>
                <td><input type="text" class="form-control" name="pertanyaan2" value="{{ $row->pertanyaan2 }}"></td>
              </tr>

              <tr>
                <td width=50% style="font-size: 16px;">Deskripsikan ide pemecahan masalah kamu dalam 100 kata</td>
                <td><b><input type="text" class="form-control" name="pertanyaan3" value="{{ $row->pertanyaan3 }}"></b></td>
              </tr>

              <tr>
                <td width=50% style="font-size: 16px;">Langkah kerja apa yang akan kelompok kamu lakukan untuk memecahkan masalah tersebut?</td>
                <td><b><input type="text" class="form-control" name="pertanyaan4" value="{{ $row->pertanyaan4 }}"></b></td>
              </tr>

              <tr>
                <td width=50% style="font-size: 16px;">Apa hasil akhir dan dampak dari pemecahan masalah tersebut?</td>
                <td><b><input type="text" class="form-control" name="pertanyaan5" value="{{ $row->pertanyaan5 }}"></b></td>
              </tr>
            </table>
        <br>
        @endforeach
        <div class="row">
          <div class="col-md-6">
            <a href="{{ URL('admin/kelompok')}}" class="col-md-12 form-control btn btn-primary">Back</a>
          </div>
          <div class="col-md-6">
          <button type="submit" class="col-md-12 form-control btn btn-secondary">Update</button>
          </div>
          </form>
    </div>
    </div>
  </div>
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="{{asset('js/kelompok.js')}}"></script>
@endsection  