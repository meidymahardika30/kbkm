@extends('admin.layouts.layout')

@section('content')         
<main>
  <div class="container-fluid">
    <br>
    <div class="card mb-4">
        <div class="card-header"><i class="fas fa-table mr-1"></i>Data Kelompok</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead align="center">
                        <tr>
                          <th>No</th>
                          <th>Nama Kelompok</th>
                          <th>Nama Ketua</th>
                          <th>Mata Lomba</th>
                          <th>KTP</th>
                          <th>Aksi</th>
                        </tr>
                    </thead>
                    <?php
                    $no = 1;
                    ?>
                    <tbody>
                    @foreach($data_kel as $item)
                        <tr>
                            <td align="center">{{ $no++ }}</td>
                            <td>{{ $item->nama_kelompok }}</td>
                            <td>{{ $item->ketua }}</td>
                            <td>{{ $item->mata_lomba }}</td>
                            <td align="center">
                              <a href="{{ URL('admin/kelompok/ktp/pdf/'. $item->id) }}" target="_blank" class="btn btn-info"><i class="fa fa-download"></i> download</a>
                            </td>
                            <td align="center">
                              <a href="{{ URL('admin/kelompok/detail/'. $item->id) }}" class="btn btn-primary">detail</a>  
                            </td>
                            <td>
                            <a href="{{ URL('admin/kelompok/delete/'. $item->id) }}" class="btn btn-danger"  onclick="return myFunction();"><i class="fa fa-trash"></i></a>
                            </td>
                            <td>
                            <a href="{{ URL('admin/kelompok/edit/'. $item->id) }}" class="btn btn-info">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
          <script>
              function myFunction() {
                  if(!confirm("Apakah anda yakin akan menghapus ini ?"))
                  event.preventDefault();
              }
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="{{asset('js/kelompok.js')}}"></script>
    @endsection  