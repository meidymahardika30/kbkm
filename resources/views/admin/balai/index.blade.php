@extends('admin.layouts.layout')

@section('content')  
                <main>
                    <div class="container-fluid">
                        <br>
                        <div class="card mb-4">
                            <div class="card-header"><i class="fas fa-table mr-1"></i>Data Balai Pelestarian Nilai Budaya (BPNB)</div>
                            <div class="card-body">   
                            <a href="{{ url('admin/report/bpnb')}}" class="btn btn-success"><i class="fa fa-file-excel"></i></a> 
                            <br>
                            <br>
                            <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered table striped" id="user_table" width="100%" cellspacing="0" >
                                    <tr>
                                        <td rowspan="2">No</td>
                                        <td rowspan="2">Wilayah</td>
                                        <td colspan="3" align="center">Kategori</td>
                                    </tr>
                                    <tr>
                                        <td>Aplikasi</td>
                                        <td>Prakarya</td>
                                        <td>Total</td>
                                    </tr>

                                        <?php
                                        $no = 1;
                                        ?>
                                        @foreach($data as $d)
                                        <tr>
                                        <td>{{$no++}}</td>
                                        <td style = "padding:20px;">
                                            <b>{{$d->nama_nomenklatur}}</b>
                                        </td>
                                      
                                    
                                      @if($d['id_nomenklatur'] == 'bpnb1')
                                        <td>
                                            <b>{{$countapk1}}</b>
                                        </td>
                                        <td>
                                            <b>{{$countpra1}}</b>
                                        </td>
                                        <td>
                                            <b>{{$count1}}</b>
                                        </td>
                                      @elseif($d['id_nomenklatur'] == 'bpnb2')
                                      <td>
                                            <b>{{$countapk2}}</b>
                                        </td>
                                        <td>
                                            <b>{{$countpra2}}</b>
                                        </td>
                                        <td>
                                            <b>{{$count2}}</b>
                                        </td>
                                      @elseif($d['id_nomenklatur'] == 'bpnb3')
                                      <td>
                                            <b>{{$countapk3}}</b>
                                        </td>
                                        <td>
                                            <b>{{$countpra3}}</b>
                                        </td>
                                        <td>
                                            <b>{{$count3}}</b>
                                        </td>
                                      @elseif($d['id_nomenklatur'] == 'bpnb4')
                                      <td>
                                            <b>{{$countapk4}}</b>
                                        </td>
                                        <td>
                                            <b>{{$countpra4}}</b>
                                        </td>
                                        <td>
                                            <b>{{$count4}}</b>
                                        </td>
                                      @elseif($d['id_nomenklatur'] == 'bpnb5')
                                      <td>
                                            <b>{{$countapk5}}</b>
                                        </td>
                                        <td>
                                            <b>{{$countpra5}}</b>
                                        </td>
                                        <td>
                                            <b>{{$count5}}</b>
                                        </td>
                                      @elseif($d['id_nomenklatur'] == 'bpnb6')
                                      <td>
                                            <b>{{$countapk6}}</b>
                                        </td>
                                        <td>
                                            <b>{{$countpra6}}</b>
                                        </td>
                                        <td>
                                            <b>{{$count6}}</b>
                                        </td>
                                      @elseif($d['id_nomenklatur'] == 'bpnb7')
                                      <td>
                                            <b>{{$countapk7}}</b>
                                        </td>
                                        <td>
                                            <b>{{$countpra7}}</b>
                                        </td>
                                        <td>
                                            <b>{{$count7}}</b>
                                        </td>
                                      @elseif($d['id_nomenklatur'] == 'bpnb8')
                                      <td>
                                            <b>{{$countapk8}}</b>
                                        </td>
                                        <td>
                                            <b>{{$countpra8}}</b>
                                        </td>
                                        <td>
                                            <b>{{$count8}}</b>
                                        </td>
                                      @elseif($d['id_nomenklatur'] == 'bpnb9')
                                      <td>
                                            <b>{{$countapk9}}</b>
                                        </td>
                                        <td>
                                            <b>{{$countpra9}}</b>
                                        </td>
                                        <td>
                                            <b>{{$count9}}</b>
                                        </td>
                                      @elseif($d['id_nomenklatur'] == 'bpnb10')
                                      <td>
                                            <b>{{$countapk10}}</b>
                                        </td>
                                        <td>
                                            <b>{{$countpra10}}</b>
                                        </td>
                                        <td>
                                            <b>{{$count10}}</b>
                                        </td>
                                      @else
                                      <td>
                                            <b>{{$countapk11}}</b>
                                        </td>
                                        <td>
                                            <b>{{$countpra11}}</b>
                                        </td>
                                        <td>
                                            <b>{{$count11}}</b>
                                        </td>
                                    @endif
                                    </tr>
                                        @endforeach
                                     <tr>
                                        <td></td>
                                        <td>Total</td>
                                        <td>{{$countjumlahapk}}</td>
                                        <td>{{$countjumlahpra}}</td>
                                        <td>{{$countseluruh}}</td>
                                    </tr>
                                    </table>
                                </div>
                                </div>
                            </div>
                            
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Your Website 2019</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="{{asset('assets/js/kelompok.js')}}"></script>
@endsection

