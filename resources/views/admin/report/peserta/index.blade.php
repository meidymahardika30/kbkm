@extends('admin.layouts.layout')

@section('content')
<main>
    <div class="container-fluid">
        <br>
        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-filter mr-1"></i>Filter</div>
            <div class="card-body">
            <form action="{{ URL('admin/report/kelompok/search-peserta') }}">
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="exampleFormControlInput1">Balai Pelestarian Nilai Budaya</label>
                        <select name="nomenklatur" id="nomenklatur" class="form-control">
                            <option value="">Pilih BPNB</option>
                            @foreach($nomenklatur as $item)
                                <option value="{{ $item->id_nomenklatur }}">{{ $item->nama_nomenklatur }}</option>
                            @endforeach
                        </select>
                        </div>
                        <div class="form-group col-md-3">
                        <label for="exampleFormControlInput1">Provinsi</label>
                        <select name="wilayah" id="wilayah" class="form-control">
                        <option value="">Pilih Provinsi</option>
                        @foreach($provinsi as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="exampleFormControlInput1">Dari Tanggal</label>
                        <input type="date" class="form-control" id="exampleFormControlInput1" name="dari">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="exampleFormControlInput1">Sampai Tanggal</label>
                        <input type="date" class="form-control" id="exampleFormControlInput1" name="sampai">
                    </div>
                </div>
                <?php
                    $nomenklatur = $_GET['nomenklatur'];
                    $wilayah = $_GET['wilayah'];
                    $dari = $_GET['dari'];
                    $sampai = $_GET['sampai'];
                    if($nomenklatur != "" && $wilayah !="" && $dari != "" && $sampai != ""){
                        $ml = $nomenklatur;
                        $wl = $wilayah;
                        $d = $dari;
                        $s = $sampai;
                    }elseif($wilayah =="" && $dari != "" && $sampai != ""){
                        $ml = "";
                        $wl = "";
                        $d = $dari;
                        $s = $sampai;
                    }elseif($wilayah !=""){
                        $ml = "";
                        $wl = $wilayah;
                        $d = "";
                        $s = "";
                    }elseif($nomenklatur !=""){
                        $ml = $nomenklatur;
                        $wl = "";
                        $d = "";
                        $s = "";
                    }else{
                        $ml = "";
                        $wl = "";
                        $d = "";
                        $s = "";
                    }
                ?>
                <button class="btn btn-primary">submit</button>
            </form>
            </div>
        </div>
        
        <a href="exportpendaftar?nomenklatur={{$ml}}&wilayah={{$wl}}&dari={{$d}}&sampai={{$s}}" class="btn btn-success"><i class="fa fa-file-excel"></i> export to excel</a>
        <br>
        <br>

        <div class="card mb-4">
            <div class="card-header"><i class="fas fa-table mr-1"></i>Data Peserta</div>
            <div class="card-body">
            
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Mata Lomba</th>
                                <th>Nama Peserta</th>
                                <th>Nama Kelompok</th>
                                <th>Jenis Kelamin</th>
                                <th>No.HP</th>
                                <th>Email</th>
                                <th>Pendidikan</th>
                                <th>NIK</th>
                                <th>Tanggal Lahir</th>
                                <th>Alamat</th>
                            </tr>
                        </thead>
                        <?php
                        $no = 1;
                        ?>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{!! GetMataLomba($item->id_kelompok) !!}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{!! GetNamaKelompok($item->id_kelompok) !!}</td>
                                <td>{{ $item->jk }}</td>
                                <td>{{ $item->no_hp }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->pendidikan }}</td>
                                <td>{{ $item->nik }}</td>
                                <td>{{ $item->tanggal_lahir }}</td>
                                <td>{!! GetProvinsi($item->id_provinsi) !!} , {!! GetKota($item->id_kota) !!}, {!! GetKecamatan($item->id_kecamatan) !!}, {{$item->alamat}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(document).on('change','#nomenklatur',function(){
    var id_pro = $(this).val();
    var div = $(this).parent();
    var op = "";

    console.log(id_pro);

    $.ajax({
        type : 'get',
        url : "{{ URL::to('admin/report/kelompok/find-wilayah') }}",
        data : {'id':id_pro},
        dataType : 'json',
        success:function(data){
        op+='<option value="">-- Pilih Provinsi --</option>';
        for(var i = 0; i<data.length; i++){
        op+='<option value="' + data[i].id_provinsi + '">' + data[i].name + '</option>';
        }

        div.find('#wilayah').html("");
        div.find('#wilayah').append(op);
        },
        error:function(){

        }
    });
    });
});   
</script>         
@endsection