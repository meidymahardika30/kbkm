<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token()}}">

    <!-- Bootstrap CSS -->

    <title>{{$pdfview->nama_kelompok}}</title>
    <style>
         #customers {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
        }

        #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 4px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
        }

        .pdf-container{
            position: relative;
        }

        .header-wrapper{
           
        }

        .logo {
            width:50px;
        }

        .address{
            margin 0;
        }

        .footer-wrapper{
            position:absolute;
            bottom:0;
            text-align:center;
        }

        .page-break{
            page-break-after: always;
        }
    </style>
  </head>
  <body>
        <div class="pdf-container">
            <div class="row">
            <img src="assets/img/logo-tutwuri.png" width="60" height="60" style="margin:0px 0px 15px 20px; float:left">
                <b class="address" style="font-size:16px; margin-left:14%;" >Direktorat Pembinaan Tenaga dan Lembaga Kebudayaan, </b><br>
                <b class="address" style="font-size:16px; margin-left:16%;">Direktorat Jendral Kebudayaan Republik Indonesia</b>
                <div class="address" style="font-size:12px; margin-left: 24%;">Komplek Kemdikbud Gedung E Lt.9 Jln. Jenderal Sudirman Senayan Jakarta 10270</div><br>
        
            </div>
        </div>
        <div class="pdf-container">
                    <hr >
                    <br>
                <b style="font-size:18px;">Profil Kelompok</b>
                    <table class="table" id="customers" style="margin-top:2%;">
                    <tr>
                        <td style="font-size: 14px;">Nama Kelompok: <b>{{ $pdfview->nama_kelompok }}</b></td>
                    </tr>
                    <tr>
                        <td style="font-size: 14px;">Nama Ketua: <b>{{ $pdfview->ketua }}</b></td>
                    </tr>
                    <tr>
                        <td style="font-size: 14px;">Mata lomba yang dipilih: <b>{{ $pdfview->mata_lomba }}</b></td>
                    </tr>
                    </table>
                    <table border="1" id="customers" style="margin-top:2%;">
                    <tr align="center">
                        <td style="font-size: 12px;">#</td>
                        <td style="font-size: 12px;">Nama Peserta</td>
                        <td style="font-size: 12px;">Jk</td>
                        <td style="font-size: 12px;">No. HP</td>
                        <td style="font-size: 12px;">Email</td>
                        <td style="font-size: 12px;">Pend</td>
                        <td style="font-size: 12px;">NIK</td>
                        <td style="font-size: 12px;">Tanggal Lahir</td>
                        <td style="font-size: 12px;">Alamat</td>
                    </tr>
                    <?php $i = 1; ?>
                    @foreach($pendaftar as $pendaftars)
                    <tr>
                        <td style="font-size: 12px;">{{ $i++ }}</td>
                        <td style="font-size: 12px;">{{ $pendaftars->nama }}</td>
                        <td style="font-size: 12px;">{{ $pendaftars->jk}}</td>
                        <td style="font-size: 12px;">{{ $pendaftars->no_hp }}</td>
                        <td style="font-size: 12px;">{{ $pendaftars->email }}</td>
                        <td style="font-size: 12px;">{{ $pendaftars->pendidikan }}</td>
                        <td style="font-size: 12px;">{{ $pendaftars->nik }}</td>
                        <td style="font-size: 12px;">{{ $pendaftars->tanggal_lahir }}</td>
                        <td style="font-size: 12px;">{{ $pendaftars->alamat }}, {!! GetKecamatan($pendaftars->id_kecamatan) !!}, {!! GetKota($pendaftars->id_kota) !!}, {!! GetProvinsi($pendaftars->id_provinsi) !!}</td>
                    </tr>
                    @endforeach
                    </table>
                <div class="page-break"></div>
                <div class="col-md-12">
                <h4>Gambaran Inisatif</h4>
                <table class="table " id="customers" >
                <tr>
                    <td width=50% style="font-size: 16px;">Apa sih pemajuan kebudayaan menurutmu?</td>
                </tr>
                    <tr>
                    <td style="font-size: 14px;"><b>Jawab : {{ $pdfview->pertanyaan1 }}</b></td>
                    </tr>
                <tr>
                    <td width=50% style="font-size: 16px;">Apa masalah pemajuan kebudayaan di sekitarmu yang mau kamu pecahkan?</td>
                </tr>
                <tr>
                <td style="font-size: 14px;"><b>Jawab : {{ $pdfview->pertanyaan2 }}</b></td>
                </tr>
                <tr>
                    <td width=50% style="font-size: 16px;">Deskripsikan ide pemecahan masalah kamu dalam 100 kata</td>
                </tr>
                    <tr>
                    <td style="font-size: 14px;"><b>Jawab : {{ $pdfview->pertanyaan3 }}</b></td>
                    </tr>
                <tr>
                    <td width=50% style="font-size: 16px;">Langkah kerja apa yang akan kelompok kamu lakukan untuk memecahkan masalah tersebut?</td>
                </tr>
                    <tr>
                    <td style="font-size: 14px;"><b>Jawab : {{ $pdfview->pertanyaan4 }}</b></td>
                    </tr>
                <tr>
                <td width=50% style="font-size: 16px;">Apa hasil akhir dan dampak dari pemecahan masalah tersebut?</td>
                </tr>
                <tr>
                    <td style="font-size: 14px;"><b>Jawab : {{ $pdfview->pertanyaan5 }}</b></td>
                </tr>
                </table>
            </div>
            </div>
            <div class="footer-wrapper">
            <p class="address" style="font-size:12px;">Sekretariat Direktorat Jenderal, Direktorat Jenderal Kebudayaan Republik Indonesia</p>
            </div>
        </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>