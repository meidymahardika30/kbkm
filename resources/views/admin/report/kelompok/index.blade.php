@extends('admin.layouts.layout')

@section('content')         
<main>
  <div class="container-fluid">
    <br>
    <div class="card mb-4">
        <div class="card-header"><i class="fas fa-filter mr-1"></i>Filter</div>

        <div class="card-body">
            <form action="{{ URL('admin/report/kelompok/search-kelompok') }}">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label>Mata Lomba</label>
                        <select name="mata_lomba" id="mata_lomba" class="form-control">
                            <option value="">Pilih Mata Lomba</option>
                            <option value="aplikasi">Aplikasi</option>
                            <option value="prakarya">Prakarya</option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="exampleFormControlInput1">Dari Tanggal</label>
                        <input type="date" class="form-control" id="exampleFormControlInput1" name="mulai">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="exampleFormControlInput1">Sampai Tanggal</label>
                        <input type="date" class="form-control" id="exampleFormControlInput1" name="akhir">
                    </div>
                </div>
                <?php
                            $mata_lomba = $_GET['mata_lomba'];
                            $mulai = $_GET['mulai'];
                            $akhir = $_GET['akhir'];
                            if($mata_lomba != "" && $mulai != "" && $akhir != ""){
                                $ml = $mata_lomba;
                                $m = $mulai;
                                $a = $akhir;
                            }elseif($mulai != "" && $akhir != ""){
                                $ml = "";
                                $m = $mulai;
                                $a = $akhir;
                            }elseif($mata_lomba != ""){
                                $ml = $mata_lomba;
                                $m = "";
                                $a = "";
                            }else{
                                $ml = "";
                                $m = "";
                                $a = "";
                            }
                        ?>
                <button class="btn btn-primary">submit</button>
            </form>
        </div>
    </div>
  
    <a href="kelompok/export?mata_lomba={{$ml}}&mulai={{$m}}&akhir={{$a}}" class="btn btn-success"><i class="fa fa-file-excel"></i> export to excel</a>
    <br>
    <br>
    <div class="card mb-4">
        <div class="card-header"><i class="fas fa-table mr-1"></i>Data Kelompok</div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead align="center">
                        <tr>
                        <th>No</th>
                        <th>Nama Kelompok</th>
                        <th>Nama Ketua</th>
                        <th>Mata Lomba</th>
                        <th>KTP</th>
                        <th>Detail</th>
                        </tr>
                    </thead>
                    <?php
                    $no = 1;
                    ?>
                    <tbody>
                    @foreach($data_kel as $item)
                        <tr>
                            <td align="center">{{ $no++ }}</td>
                            <td>{{ $item->nama_kelompok }}</td>
                            <td>{{ $item->ketua }}</td>
                            <td>{{ $item->mata_lomba }}</td>
                            <td align="center">
                              <a href="{{ URL('admin/kelompok/ktp/pdf/'. $item->id) }}" target="_blank" class="btn btn-info"><i class="fa fa-download"></i> download</a>
                            </td>
                            <td align="center">
                              <a href="{{ URL('admin/report/kelompok/detail/'. $item->id) }}" class="btn btn-primary">detail</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>    
    </div>
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="{{asset('js/kelompok.js')}}"></script>
@endsection  