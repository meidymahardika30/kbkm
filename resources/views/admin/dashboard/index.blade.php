@extends('admin.layouts.layout')

@section('content')  
<input type="hidden" id="apk" value="{{ $jmlapk }}">
<input type="hidden" id="pra" value="{{ $jmlpra }}">
<input type="hidden" id="age18" value="{{ $age18 }}">
<input type="hidden" id="age19" value="{{ $age19 }}">
<input type="hidden" id="age20" value="{{ $age20 }}">
<input type="hidden" id="age21" value="{{ $age21 }}">
<input type="hidden" id="age22" value="{{ $age22 }}">
<input type="hidden" id="age23" value="{{ $age23 }}">
<input type="hidden" id="age24" value="{{ $age24 }}">
<input type="hidden" id="age25" value="{{ $age25 }}">

<main>
    <div class="container-fluid">
        <br>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card bg-primary text-white mb-4">
                    <div class="card-body" style="text-align:left;"><h5>{{ $jmlapk }} Kelompok</h5> Aplikasi </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-white stretched-link" data-toggle = "modal" data-target = "#myAplikasi">View Details</a>
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card bg-warning text-white mb-4">
                    <div class="card-body" style="text-align:left"><h5>{{ $jmlpra }} Kelompok</h5> Prakarya</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-white stretched-link" data-toggle = "modal" data-target = "#myPrakarya">View Details</a>
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card bg-success text-white mb-4">
                    <div class="card-body" style="text-align:lef;"><h5>{{ $dataall }} Peserta</h5> Seluruh Peserta</div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small text-white stretched-link" data-toggle = "modal" data-target = "#myAll">View Details </a>
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6">
                <div class="card mb-4">
                    <div class="card-header"><i class="fas fa-chart-area mr-1"></i>Data kelompok berdasarkan mata lomba</div>
                    <canvas id="myPieChart" style="height: 300px; width: 100%;"></canvas>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card mb-4">
                    <div class="card-header"><i class="fas fa-chart-bar mr-1"></i>Data pendaftar berdasarkan usia</div>
                        <canvas id="myBarChart" style="height: 300px; width: 100%;"></canvas>
                </div>
            </div>
        </div>
       
    </div>
</main>
<!-- Modal -->
<div class="modal fade" id="myAplikasi" role="dialog">
    <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title">Detail Pendaftar Aplikasi</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <table border="1" width="100%" style="text-align:center;">
            <tr>
                <td>Laki - Laki</td>
                <td>Perempuan </td>
            </tr>
            
            <tr>
                <td>{{ $apkboy }}</td>
                <td>{{ $apkgirl }}</td>
            </tr>
            
        </table>
        Total : <b>{{$apk}}</b>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myPrakarya" role="dialog">
    <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title">Detail Pendaftar Prakarya</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <table border="1" width="100%" style="text-align:center;">
            <tr>
                <td>Laki-Laki</td>
                <td>Perempuan </td>
            </tr>
            <tr>
                <td>{{$praboy}}</td>
                <td>{{$pragirl}}</td>
            </tr>
        </table>
        Total : <b>{{$pra}}</b>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myAll" role="dialog">
    <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title">Detail Pendaftar Keseluruhan</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <table border="1" width="100%" style="text-align:center; background-color = red;">
            <tr>
                <td>Laki - Laki</td>
                <td>Perempuan </td>
            </tr>
            <tr>
                <td>{{ $databoy }}</td>
                <td>{{ $datagirl }}</td>
            </tr>
        </table>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
    </div>
</div>
</div>
        
@endsection  

