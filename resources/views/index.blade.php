@extends('layouts.template')
<head>   
  <style>
  #more1 {
      display: none;
    }
  </style>

  <style>
    #more2 {
      display: none;
    }
  </style>

  <style>
    #more3 {
      display: none;
    }
  </style>
</head>
@section('content')




   <!-- Start Slider Area -->
  <div id="beranda" class="slider-area">
    <div class="bend niceties preview-2">
      <div id="ensign-nivoslider" class="slides">
        <img src="{{ asset('assets/img/slider/slider 1.jpeg') }}" alt="" title="#slider-direction-3" />
        <img src="{{ asset('assets/img/slider/slider 2.jpeg') }}" alt="" title="#slider-direction-1" />
        <img src="{{ asset('assets/img/slider/slider 3.jpeg') }}" alt="" title="#slider-direction-2" />
      </div>

      <!-- slider 1 -->
      <div id="slider-direction-1" class="slider-direction slider-one">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="slider-content">
                <div class="layer-1-3 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                  <a class="ready-btn page-scroll" href="/pendaftaran/index" style="color: #1a1a1a border: 1px #1a1a1a">Daftar Sekarang</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- slider 2 -->
    <div id="slider-direction-2" class="slider-direction slider-two">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="slider-content text-center">
              <div class="layer-1-3 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                <a class="ready-btn page-scroll" href="/pendaftaran/index">Daftar Sekarang</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  
  <!-- slider 3 -->
  <div id="slider-direction-3" class="slider-direction slider-two">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="slider-content">
            <div class="layer-1-3 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
              <a class="ready-btn page-scroll" href="/pendaftaran/index">Daftar Sekarang</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- End Slider Area -->
  
  <!-- Start Tentang KBKM 1-->
  <div id="tentang_kbkm" class="about-area area-padding" 
    style="background-image: url('{{ asset('assets/img/background/bg1.jfif') }}');background-repeat: no-repeat;background-size: 100% 105%"  >
    <div class="container"> 
      <div class="row">
      </div>
      <div class="row">
        <!-- single-well start-->
      
        <!-- single-well end-->
        <div class="col-md-12 col-sm-12 col-xs-12" style="background-color:rgba(225,225,225,0.7); #808080; border-radius: 35px; box-shadow: 1px 1px 0.7px grey;">
          <div class="well-middle">
            <div class="single-well">
              <a href="#">
              </a>
              <div class="section-headline text-center">
                <br>
                <br>
                <h2><font face="verdana">Tentang KBKM 2020</font></h2>
               </div>
               <div class="text">
                <h4><p style="text-align= justify; margin-left:5%; "><b>Di mana sih biasanya kaum muda tarung ide?</b></p></h4>
               </div>
              <p style="text-align= justify; margin-left:6%; margin-right:6%;">
              Tentu tanpa batasan ruang untuk Sahabat Muda dapat saling adu ide.
              Tahun 2019, Direktorat Jenderal Kebudayaan Kementerian Pendidikan
               dan Kebudayaan telah menginisiasi Kemah Budaya Kaum Muda. 
              </p> <br>
              <div class="text" style="margin-left:5%;">
                <h4><b>Apa sih Kemah Budaya Kaum Muda itu? </b></h4>
              </div>
              <p style="text-align:justify; margin-left:6%; margin-right:6%;">
              Nah, Kemah Budaya Kaum Muda ini merupakan tempat untuk 
              mempertemukan ide-ide Sahabat Muda dari berbagai daerah.
               Kini, di tahun 2020, Kemah Budaya Kaum Muda hadir kembali!
              </p>
              <br>
              <!-- <div class="text-center">
                <h3><b>Dasar Hukum</b></h3>
              </div> -->
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Tujuan KBKM adalah meningkatkan peran kaum muda dalam pemajuan 
              kebudayaan. Nah, kami percaya kalianlah kaum muda Avant Garde
               yang dapat menciptakan pemajuan kebudayaan!
              </p>
              <br>
              <!-- <p style="text-align: justify;">
                #KemahBudaya pertama kali diselenggarakan tahun 2019 lalu dan akan menjadi agenda tahunannya Ditjen Kebudayaan. Di tahun pertamanya, #KemahBudaya yang berskala Nasional diadakan di Bumi Perkemahan Prambanan, Yogyakarta. Seru banget!
              </p> -->
              <!-- <p style="text-align: center;">
               Gak sabar kan untuk mengikuti #KemahBudaya tahun 2020 ini? Tunggu pengumuman <i>@kemah.budaya</i> selanjutnya untuk tahu bagaimana caranya mendaftarkan diri kamu, Sahabat Muda!
              </p> -->
              <br>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  <!-- End Tentang KBKM 1 -->

   <!-- Start Tentang KBKM 2  -->
  <div id="tentang_kbkm" class="about-area area-padding" 
    style="background-image: url('{{ asset('assets/img/background/bg2.jfif') }}');background-repeat: no-repeat;background-size: 100% 102%  "  >
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" >
        </div>
      </div>
      <div class="row">
        <!-- single-well start-->
      
        <!-- single-well end-->
        <div class="col-md-12 col-sm-12 col-xs-12" style="background-color:rgba(225,225,225,0.7); #ffcc99;  border-radius: 15px; box-shadow: 1px 1px 0.7px grey;">
          <div class="well-middle">
            <div class="single-well">
              <br>
              <br>
              <div class="">
                <h4><p style="text-align= justify; margin-left:5%;"><b>Mengapa perlu ikut KBKM 2020?</b></p></h4>
              </div>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              KBKM adalah tempat yang tepat bagi kalian merancang karya gagasan.
              Sahabat Muda dapat menciptakan berbagai karya dalam upaya 
              pemajuan kebudayaan berbasis interaksi kreatif dengan menggunakan
              pendekatan terpadu di bidang <i>STEAM (Science, Technology, 
              Engineering, Arts, dan Mathematichs)</i>. Dengan semangat 4.0,
              Sahabat Muda dapat menciptakan banyak hal. Sudah saatnya
              gagasan Sahabat Muda gak hanya tersimpan di notes dan menguap
              menjadi angin lalu.
              </p>
              <br>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Banyak kelas-kelas inspiratif yang dapat sahabat muda ikuti. KBKM menjadi kesempatan Sahabat Muda kencang bersuara di depan para tutor inovasi yang memiliki rekam jejak yang keren di bidang kebudayaan. Kesempatan ngobrol langsung dan nongkrong bareng dengan para inovator bukankah akan jadi pengalaman menarik bagi kalian? Investor juga bakal melirik gagasan kalian agar semakin berkembang melesat ke masyarakat.
              </p>
              <br>
              <!-- <p style="text-align: justify;">
                Kamu juga akan diperkenalkan dengan calon investor yang nantinya dapat mendampingi kamu untuk mengembangkan aplikasi ataupun karya yang telah kamu buat agar bisa lebih dirasakan manfaatnya oleh masyarakat luas.
              </p>
              <br>
              <p style="text-align: justify;">
                Bukan hanya itu, sepanjang #KemahBudaya berlangsung pun Sahabat Muda akan berkesempatan mendapatkan pendampingan dari pakar-pakar inovasi yang sudah berpengalaman. Juga bisa bertemu dengan Sahabat Muda lainnya dari seluruh Indonesia untuk bisa saling bertukar ilmu dan pikiran. Jadi, jangan sampai ketinggalan.
              </p> -->
              <br>
              <!-- <p style="text-align: justify;">
                <ol>
                  <li>Menyediakan ruang bagi keragaman ekspresi budaya dan mendorong interaksi untuk memperkuat kebudayaan yang inklusif</li>
                  <li>Melindungi dan mengembangkan nilai, ekspresi dan praktik kebudayaan tradisional untuk memperkaya kebudayaan nasional</li>
                  <li>Mengembangkan dan memanfaatkan kekayaan budaya untuk memperkuat kedudukan Indonesia di dunia internasional</li>
                  <li>Memanfaatkan obyek pemajuan kebudayaan untuk meningkatkan kesejahteraan masyarakat</li>
                  <li>Memajukan kebudayaan yang melindungi keanekaragaman hayati dan memperkuat ekosistem</li>
                  <li>Reformasi lembaga dan anggaran kebudayaan untuk mendukung agenda pemajuan kebudayaan</li>
                  <li>Meningkatkan peran pemerintah sebagai fasilitator pemajuan kebudayaan</li>
                </ol>
              </p> -->
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  <!-- End Tentang KBKM  -->

  <!-- Start Tentang KBKM 3  -->
  <div id="tentang_kbkm" 
    style="background-image: url('{{ asset('assets/img/background/bg3.jfif') }}');background-repeat: no-repeat;background-size: 100% 101%">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" >
        </div>
      </div>
      <div class="row">
      <!-- single-well start-->
      
        <!-- single-well end-->
        <div class="col-md-12 col-sm-12 col-xs-12" style="background-color:rgba(225,225,225,0.7); #808080; border-radius: 25px; box-shadow: 1px 1px 0.7px grey;">
          <div class="well-middle">
            <div class="single-well">
            <br>
            <br>
              <div class="">
                <h4><p style="text-align= justify; margin-left:5%;"><b>Apa saja yang perlu disiapkan? </b></p></h4>
              </div>
              <br>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Ada berbagai tahap yang mesti Sahabat Muda ikuti. Pertama-tama, 
              kamu mesti bekerja di dalam kelompok. Kalau kamu belum punya 
              kelompok atau komunitas, segera bentuklah bersama teman-temanmu. 
              Untuk Kategori Apliaksi, jumlah anggota tim adalah 5 orang dan 
              untuk Kategori Prakarya 3 orang. Tim harus terdiri dari laki-laki 
              dan perempuan. Daftarkan kelompok kamu di kolom pendaftaran laman 
              ini. Kamu juga mesti menyusun proposal gagasan yang mencakup 
              rancangan anggaran biaya, pembentukan lini masa, penyusunan 
              rencana kerja dan  evaluasi proposal. Jadi  mulai dari sekarang 
              obrolin, tulis dan rancanglah gagasan kalian dengan teman-teman 
              terbaik kalian. Gak usah ragu dan takut.
              </p>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Jika proposal kalian lolos, Sahabat Muda akan dikumpulkan 
              dalam ajang tarung ide di Kemah Budaya Kaum Muda Regional. 
              Kemah Budaya Kaum Muda Regional ini akan diadakan di 11 daerah. 
              Setiap daerah akan dipilih 3 pemenang untuk masing-masing kategori.
               Namun, hanya yang terbaik dari tiga pemenang itulah yang berhak 
               melaju ke Kemah Budaya Kaum Muda Nasional. Di Kemah Budaya Kaum 
               Muda Regional mau pun Nasional, dengan suasana yang hangat, kalian 
               dapat saling bertukar gagasan dengan tutor. Proposal akan dipertajam 
               menjadi sebuah business canvas dan perincian proposal model.
              </p>

              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Tentunya, paska kemah, kalian juga dapat mengaktivasi lebih jauh 
              pengembangan gagasan menjadi aplikasi dan prakarya. Jadi 
              siapkanlah amunisi gagasan terbaik kalian untuk mengikuti KBKM 
              2020!
              </p>

              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Sampai Jumpa Sahabat Muda….
              </p>
              <!-- <p style="text-align: justify;">
                <ol>
                  <li>Pastikan kamu membentuk tim yang terdiri dari 3-5 orang berusia 18 hingga 25 tahun.</li>
                  <li>Mengisi formulir pendaftaran yang akan dibuka di laman <i><a href="#">kbkm.kebudayaan.id</a></i> pada tanggal 28 Maret 2020.</li>
                  <li>Melengkapi semua persyaratan yang ada di laman tersebut.</li>
                  <li>Menciptakan ide prakarya atau aplikasi untuk menjawab permasalahan dari daftar masalah Pemajuan Kebudayan yang ada di Kabupaten/Kota tempat tim kamu mendaftarkan diri.</li>
                </ol>
              </p> -->
              <br>
              <br>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  <!-- End Tentang KBKM 3 -->

  <!-- Start Tentang KBKM 4  -->
  <!-- <div id="tentang_kbkm" class="about-area area-padding" 
    style="background-image: url('{{ asset('assets/img/background/background 4 ( baru )-01.jpg') }}');background-repeat: no-repeat;background-size: 100%" >
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" >
        </div>
      </div>
      <div class="row"> -->
        <!-- single-well start-->
      
        <!-- single-well end-->
        <!-- <div class="col-md-12 col-sm-12 col-xs-12"style="background-color:rgba(225,225,225,0.7); #ffcc99; border-radius: 10px; box-shadow: 1px 1px 0.7px grey;">
          <div class="well-middle">
            <div class="single-well">
              </a> 
              <br>
              <br>
              <div class="">
                <h4><b>Maksud dan Tujuan</b></h4>
              </div>
              <p style="text-align: justify;">
                Kegiatan Kemah Budaya Kaum Muda ini bermaksud untuk menjadi ruang inkubator yang mendorong lahirnya berbagai purwarupa (prototype) dan inisiatif sosial untuk memperkuat upaya pemajuan kebudayaan di berbagai daerah. Upaya pemajuan kebudayaan ini akan berbasis interaksi kreatif antar kaum muda sebagai garda-depan (avant-garde) dengan menggunakan pendekatan terpadu di bidang STEAM (<i>Science, Technology, Engineering, Arts dan Mathematics</i>).
              </p>
              <br>
              <br>
              <div class="">
                <h4><b>Tujuan kegiatan penyusunan Kemah Budaya Kaum Muda adalah:</b></h4>
              </div>
              <p style="text-align: justify;">
                <ol>
                  <li>Meningkatkan pemanfaataan berbagai aspek Revolusi Industri 4.0. dalam pemajuan kebudayaan</li>
                  <li>Mendorong kontribusi berbagai capaian terbaru dalam STEAM (<i>Science, Technology, Engineering, Arts dan Mathematics</i>)</li>
                  <li>Meningkatkan peran kaum muda dalam pemajuan kebudayaan</li>
                  <li>Meningkatkan kerja sama lintas K/L, pemerintah pusat dan daerah, serta pihak swasta dalam pemajuan kebudayaan</li>
                  <li>Mencari solusi atas Daftar Masalah Umum PPKD</li>
                </ol>
              </p>
              <br>
            </div>
          </div>
        </div> -->
        <!-- End col-->
      <!-- </div>
    </div>
  </div> -->
  <!-- End Tentang KBKM 4 -->

    <!-- Start Tahapan KBKM -->
    <div id="tahapan_kbkm" class="about-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <!-- <div class="section-headline text-center">
            <h2>Tahapan Kemah Budaya Kaum Muda</h2>
          </div> -->
        </div>
      </div>
      <div class="row">
        <!-- single-well start-->
      
      <!-- Tahapan KBKM-->
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="well-middle">
          <div class="single-well">
            <!-- <a href="#">
              <h1 class="sec-head">TAHAPAN KBKM</h1>
            </a> -->
            <table class="table table-bordered">
            <thead class="thead-dark">
                <!-- <tr style="background-color: #b3c6ff;">
                  <th scope="col">TAHAPAN KBKM</th>
                  <th scope="col">AGENDA KERJA</th>
                  <th scope="col">INDIKATOR INFORMA KERJA</th>
                </tr> -->
              </thead>
              <tbody>
                <!-- Tahap 1 -->
                <!-- <tr>
                  <td rowspan = "7">
                    <b>Tahap I:</b> <br>
                    Pra-Kemah (April – 20 Juli 2019)
                  </td>
                  <td>Menyusun Proposal</td>
                  <td>Terbentuknya Kerangka Proposal Dasar</td>
                </tr>
                <tr>
                  <td>Menajamkan Proposal</td>
                  <td>
                    Tersusunnya 4 Bab dokumen proposal: Latar Belakang, Rumusan Masalah, Konsep Purwarupa dan Manfaat Purwarupa
                  </td>
                </tr>
                <tr>
                  <td>Penyusunan Rancangan Anggaran Biaya</td>
                  <td>Tersusunnya RAB yang logis dan mencerminkan efektivitas dan efisiensi biaya</td>
                </tr>
                <tr>
                  <td>Pembentukan Lini Masa</td>
                  <td>Tersusunnya Lini Masa kerja kelompok sesuai dengan tenggat waktu KBKM</td>
                </tr>
                <tr>
                  <td>Penyusunan Rencana Kerja</td>
                  <td>Tersusunnya Rencana Kerja beserta aspek turunannya</td>
                </tr>
                <tr>
                  <td>Evaluasi Proposal</td>
                  <td>Terciptanya Proposal yang semakin tajam dan masuk akal</td>
                </tr>
                <tr>
                  <td>Revisi Proposal</td>
                  <td>Tersusunnya Proposal final yang akan dikembangkan dalam KBKM</td>
                </tr> -->
                <!-- Tahap 2 -->
                <!-- <tr>
                  <td rowspan = "2">
                    <b>Tahap II:</b> <br>
                    Kemah (21 – 25 Juli 2019)
                  </td>
                  <td>Penyusunan Business Canvas</td>
                  <td>
                    Tersusunnya kerangka Kanvas Model Bisnis yang memetakan hal-hal yang berkaitan dengan ide proposal
                  </td>
                </tr>
                <tr>
                  <td>Perincian Proposal dan Model</td>
                  <td>
                    Kelompok merinci proposal mereka dan membangun model (misalnya maket, dummy) yang siap dipresentasikan di depan dewan juri
                  </td>
                </tr> -->
                <!-- Tahapan 3 -->
                <!-- <tr>
                  <td rowspan = "3">
                    <b>Tahap III:</b> <br>
                    Pasca-Kemah (26 Juli – 1 Desember 2019)
                  </td>
                  <td>Perencanaan Purwarupa atau Aktivasi</td>
                  <td>
                    Kelompok menyusun roadmap dan pembagian peran dalam mewujudkan purwarupa atau aktivasi
                  </td>
                </tr>
                <tr>
                  <td>Pengembangan Purwarupa atau Aktivasi</td>
                  <td>
                    Kelompok menjalankan pengembangan purwarupa atau aktivasi sesuai jadwal yang telah direncanakan
                  </td>
                </tr>
                <tr>
                  <td>Pameran dan Pelaporan Purwarupa atau Aktivasi</td>
                  <td>
                    Kelompok menyelesaikan purwarupa atau aktivasi dan menyampaikan laporan
                  </td>
                </tr> -->
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- End konten tahapan-->
    </div>
  </div>
</div>
<!-- End Tahapan -->

<!-- Peserta -->

<div class="section-headline text-center" id="peserta">
  <h2>Peserta</h2>
  </div>
    <div class="our-skill-area fix hidden-sm">
      <div class="test-overly"></div>
      <div class="skill-bg area-padding-2">
        <div class="container">
          <!-- section-heading end -->
          <div class="row">
            <div class="skill-text">
              <!-- single-peserta start -->
              <div class="col-xs-12 col-sm-6 col-md-6 text-center">
              <div class="about-move">
                <div class="services-details">
                  <div class="single-services">
                    <a class="services-icon" href="#">
                      <u><b><h1 class="progress-h4">{{ $jmlapk }}</h1></b></u>
                    </a>
                      <h2 class="progress-h4">Aplikasi</h2>
                  </div>
                </div>
                </div>
              </div>
              <!-- single-peserta end -->
              <!-- single-peserta start -->
              <div class="col-xs-12 col-sm-6 col-md-6 text-center">
              <div class="about-move">
                <div class="services-details">
                  <div class="single-services">
                    <a class="services-icon" href="#">
                    <u><b><h1 class="progress-h4">{{ $jmlpra }}</h1></b></u>
                      </a>
                      <h2 class="progress-h4">Prakarya</h2>
                  </div>
                </div>
                </div>
              </div>
              <!-- single-peserta end -->
            </div>
          </div>
        </div>
      </div>
    </div>
<!-- Peserta end -->

    <!-- Start tabel pendaftar KBKM -->
    <div id="peserta" class="about-area area-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <!-- <div class="section-headline text-center">
                <h2>Tahapan Kemah Budaya Kaum Muda</h2>
              </div> -->
            </div>
          </div>
          <div class="row">
          <!-- single-well start-->

          <!-- Peserta KBKM-->
          <div class="col-md-12 col-sm-12 col-xs-12">
                <br>
                  <a href="#">
                    <h1 class="sec-head">Data Kelompok Peserta KBKM 2020</h1>
                  </a>
                  <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" 
                    width="100%" cellspacing="0">
                        <thead align="center">
                          <tr style="background-color: #fffff;">
                            <th>No</th>
                            <th>Provinsi</th>
                            <th>Kabupaten/Kota</th>
                            <th>Kecamatan</th>
                            <th>Nama Kelompok</th>
                            <th>Bentuk Inisiatif</th>
                            <th>Nama Ketua</th>
                          </tr>
                        </thead>
                     <?php
                        $no = 1;
                    ?>
                    <tbody>
                     @foreach ($data as $item)
                      <tr>
                        <td>{{ $no++ }}</td>
                        <td>{!! GetProvinsi($item->id_provinsi) !!}</td>
                        <td>{!! GetKota($item->id_kota) !!}</td>
                        <td>{!! GetKecamatan($item->id_kecamatan) !!}</td>
                        <td>{{ $item->nama_kelompok }}</td>
                        <td>{{ $item->mata_lomba }}</td>
                        <td>{{ $item->ketua }}</td>
                      </tr>
                     @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            </div>
            <!-- End konten TPeserta-->
          </div>
      <!-- End Tabel peserta -->

<!-- Berita -->

  <!-- Start Berita Area -->
  <div id="berita" class="blog-area" style="margin-top:2%; margin-bottom:2%;">
      <div class="container ">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center">
              <h2>Berita</h2>
            </div>
          </div>
        </div>
        <div class="row" style="margin-bottom:5%;">
        <div class="col-md-2"></div>
          <div class="col-md-8">
            <div class="media">
              <div class="media-body">
                <iframe width="100%" height="200%" src="https://www.youtube.com/embed/urt26EKXxUc" frameborder="0" allowfullscreen>
                </iframe>
              </div>
            </div>
          </div>
        <div class="col-md-2"></div>
        </div>
        @foreach($berita as $index => $item)
          <!-- Start Left Blog -->
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="single-blog">
              <div class="single-blog-img">
                <img src="{{ asset('storage/admin/berita/'.$item->title.'/'.$item->foto) }}" alt="{{ $item->title }}"  style="height: 250px;">
              </div>
              <div class="blog-meta">
                <span class="date-type">
										<i class="fa fa-calendar"></i>{{$item->created_at}}
									</span>
              </div>
              <div class="blog-text">
                <h3>
                  {{$item->title}}
								</h3>
                <span id="dots{{ $index+1 }}">...</span>
                <span id="more{{ $index+1 }}">
                  {!!$item->konten!!}
                </span>
              </div>
              <button onclick="myFunction{{ $index+1 }}()" id="myBtn{{ $index+1 }}">baca selengkapnya</button>
            </div>
          </div>
        @endforeach
        <div class="col-md-12"></div>
      </div>
  </div>
  <!-- End Blog -->
  <!-- Start vidio Area -->
  <!-- End vidio -->

    <!-- Start Faq KBKM 1-->
  <div id="faq" class="about-area area-padding" 
    style="background-image: url('{{ asset('assets/img/background/bg1.jfif') }}');background-repeat: no-repeat;background-size: 100% 105%"  >
    <div class="container"> 
      <div class="row">
      </div>
      <div class="row">
        <!-- single-well start-->
      
        <!-- single-well end-->
        <div class="col-md-12 col-sm-12 col-xs-12" style="background-color:rgba(225,225,225,0.7); #808080; border-radius: 35px; box-shadow: 1px 1px 0.7px grey;">
          <div class="well-middle">
            <div class="single-well">
              <a href="#">
              </a>
              <div class="section-headline text-center">
                <br>
                <br>
                <h2><font face="verdana">Faq KBKM</font></h2>
               </div>
               <div class="text">
                <h4><p style="text-align= justify; margin-left:5%;"><b>Apa itu KBKM?</b></p></h4>
              </div>
              <p style="text-align= justify; margin-left:6%; margin-right:6%;">
              Kemah Budaya Kaum Muda (selanjutnya disingkat KBKM) 2020 merupakan platform kerja budaya yang menghimpun kaum muda yang berusia antara 18 sampai dengan 25 tahun untuk menjawab berbagai tantangan pemajuan kebudayaan dengan memanfaatkan kekayaan wawasan di bidang STEAM (<i>Science, Technology, Engineering, Arts dan Mathematics</i>). 
              </p> <br>
              <div class="text" style="margin-left:5%;">
                <h4><p style="text-align= justify;"><b>Apa tujuan diselenggarakan KBKM?</b></p></h4>
              </div>
              <p style="text-align:justify; margin-left:6%; margin-right:6%;">
              Rangkaian kegiatan Kemah Budaya Kaum Muda (KBKM) 2020 bermaksud untuk mewujudkan ruang inkubator yang mendorong lahirnya berbagai aplikasi dan prakarya yang memperkuat upaya pemajuan kebudayaan di berbagai daerah berbasis interaksi kreatif antar kaum muda sebagai garda-depan (avant-garde) pemajuan kebudayaan dengan menggunakan pendekatan terpadu di bidang STEAM (<i>Science, Technology, Engineering, Arts dan Mathematics</i>). Tujuan kegiatan KBKM adalah:
              </p>
              <!-- <div class="text-center">
                <h3><b>Dasar Hukum</b></h3>
              </div> -->
              <div style="text-align: justify; margin-left:6%; margin-right:6%;">
              <ol>
                  <li>Meningkatkan pemanfaatan berbagai aspek STEAM dalam pemajuan kebudayaan</li>
                  <li>Meningkatkan peran angkatan muda dalam pemajuan kebudayaan</li>
                  <li>Meningkatkan kerja sama lintas K/L, pemerintah pusat dan daerah, serta pihak swasta dalam pemajuan kebudayaan</li>
                  <li>Mencari solusi atas Daftar Masalah Umum Pokok Pikiran Kebudayaan Daerah</li>
                </ol>
              </div>
              <br>
              <!-- <p style="text-align: justify;">
                #KemahBudaya pertama kali diselenggarakan tahun 2019 lalu dan akan menjadi agenda tahunannya Ditjen Kebudayaan. Di tahun pertamanya, #KemahBudaya yang berskala Nasional diadakan di Bumi Perkemahan Prambanan, Yogyakarta. Seru banget!
              </p> -->
              <!-- <p style="text-align: center;">
               Gak sabar kan untuk mengikuti #KemahBudaya tahun 2020 ini? Tunggu pengumuman <i>@kemah.budaya</i> selanjutnya untuk tahu bagaimana caranya mendaftarkan diri kamu, Sahabat Muda!
              </p> -->
              <br>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  <!-- End Faq KBKM 1 -->

  <!-- Start Faq KBKM 2  -->
  <div id="faq" class="about-area area-padding" 
    style="background-image: url('{{ asset('assets/img/background/bg2.jfif') }}');background-repeat: no-repeat;background-size: 100% 102%  "  >
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" >
        </div>
      </div>
      <div class="row">
      <!-- single-well start-->
      
        <!-- single-well end-->
        <div class="col-md-12 col-sm-12 col-xs-12" style="background-color:rgba(225,225,225,0.7); #ffcc99;  border-radius: 15px; box-shadow: 1px 1px 0.7px grey;">
          <div class="well-middle">
            <div class="single-well">
              <br>
              <br>
              <div class="">
                <h4><p style="text-align= justify; margin-left:5%;"><b>Siapa yang dapat mengikuti KBKM?</b></p></h4>
              </div>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Warga Negara Indonesia dengan syarat sebagai berikut:
              </p>
              <div style="text-align: justify; margin-left:6%; margin-right:6%;">
              <ol>
                  <li>Berusia 18 s.d. 25 tahun dan membentuk kelompok yang terdiri dari 3 orang (untuk kategori prakarya) dan 5 orang (untuk kategori aplikasi);</li>
                  <li>Satu kelompok harus memiliki perpaduan unsur laki-laki dan perempuan;</li>
                  <li>Satu kelompok diharapkan berdomisili di satu Kota/Kabupaten yang sama untuk mempermudah koordinasi;</li>
                  <li>Menciptakan purwarupa dan aktivasi inisiatif sosial untuk menjawab permasalahan dari Daftar Masalah Umum (DMU) PPKD Kabupaten/Kota terkait dengan 10 Objek Pemajuan Kebudayaan (OPK) yang berperspektif STEAM (<i>Science, Technology, Engineering, Arts, Mathematics</i>);</li>
                  <li>Setiap anggota kelompok harus mengikuti seluruh rangkaian kegiatan KBKM tanpa perwakilan atau anggota pengganti;</li>
                  <li>Nomor ponsel yang dicantumkan harus memiliki fitur Whatsapp</li>
                </ol>
              </div>
              <br>
              <div class="">
                <h4><p style="text-align= justify; margin-left:5%;"><b>Apa manfaat ikut KBKM?</b></p></h4>
              </div>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Peserta KBKM berkesempatan memenangkan total hadiah sebesar Rp. 585.000.000 yang diperuntukkan bagi pengembangan aplikasi atau prakarya yang mereka gagas.
              </p>
              <br>
              <!-- <p style="text-align: justify;">
                Kamu juga akan diperkenalkan dengan calon investor yang nantinya dapat mendampingi kamu untuk mengembangkan aplikasi ataupun karya yang telah kamu buat agar bisa lebih dirasakan manfaatnya oleh masyarakat luas.
              </p>
              <br>
              <p style="text-align: justify;">
                Bukan hanya itu, sepanjang #KemahBudaya berlangsung pun Sahabat Muda akan berkesempatan mendapatkan pendampingan dari pakar-pakar inovasi yang sudah berpengalaman. Juga bisa bertemu dengan Sahabat Muda lainnya dari seluruh Indonesia untuk bisa saling bertukar ilmu dan pikiran. Jadi, jangan sampai ketinggalan.
              </p> -->
              <br>
              <!-- <p style="text-align: justify;">
                <ol>
                  <li>Menyediakan ruang bagi keragaman ekspresi budaya dan mendorong interaksi untuk memperkuat kebudayaan yang inklusif</li>
                  <li>Melindungi dan mengembangkan nilai, ekspresi dan praktik kebudayaan tradisional untuk memperkaya kebudayaan nasional</li>
                  <li>Mengembangkan dan memanfaatkan kekayaan budaya untuk memperkuat kedudukan Indonesia di dunia internasional</li>
                  <li>Memanfaatkan obyek pemajuan kebudayaan untuk meningkatkan kesejahteraan masyarakat</li>
                  <li>Memajukan kebudayaan yang melindungi keanekaragaman hayati dan memperkuat ekosistem</li>
                  <li>Reformasi lembaga dan anggaran kebudayaan untuk mendukung agenda pemajuan kebudayaan</li>
                  <li>Meningkatkan peran pemerintah sebagai fasilitator pemajuan kebudayaan</li>
                </ol>
              </p> -->
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  <!-- End Faq KBKM 2 -->

  <!-- Start Faq KBKM 3  -->
  <div id="faq" 
    style="background-image: url('{{ asset('assets/img/background/bg3.jfif') }}');background-repeat: no-repeat;background-size: 100% 101%">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" >
        </div>
      </div>
      <div class="row">
      <!-- single-well start-->
      
        <!-- single-well end-->
        <div class="col-md-12 col-sm-12 col-xs-12" style="background-color:rgba(225,225,225,0.7); #808080; border-radius: 25px; box-shadow: 1px 1px 0.7px grey;">
          <div class="well-middle">
            <div class="single-well">
            <br>
            <br>
              <div class="">
                <h4><p style="text-align= justify; margin-left:5%;"><b>Bagaimana tahap-tahap pelaksanaan KBKM?</b></p></h4>
              </div>
              <br>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              KBKM 2020 diselenggarakan secara berjenjang dalam dua tahap, yakni KBKM Regional dan KBKM Nasional. Kedua tahap tersebut dijabarkan sebagai berikut:
              </p>
              <div style="text-align: justify; margin-left:6%; margin-right:6%;">
              <ol>
                  <li>KBKM Regional diselenggarakan selama lima hari pada bulan Juli di 11 Balai Pelestarian Nilai Budaya sebagai Unit Pelaksana Teknis (UPT) Direktorat Jenderal Kebudayaan. Rangkaian acara dalam KBKM Regional terdiri atas:
                    <ol>
                      <li>Pelatihan oleh fasilitator</li>
                      <li>Peserta menyiapkan seluruh bahan presentasi</li>
                      <li>Presentasi di hadapan juri</li>
                      <li>Pengumuman peserta yang lolos seleksi KBKM Regional kategori Aplikasi dan Prakarya. Setiap wilayah akan melahirkan pemenang 1, 2, dan 3 untuk masing-masing kategori, hanya pemenang pertama dari setiap kategori yang akan berlaga di KBKM Nasional</li>
                    </ol>
                  </li>
                  <li>Puncak KBKM Nasional diselenggarakan di bulan Oktober dan berakhir pada saat acara Anugerah Kebudayaan dalam Pekan Kebudayaan Nasional 2020. Peserta yang bertanding di KBKM Nasional adalah peserta seleksi dari setiap kategori di KBKM Regional. Pada KBKM Nasional akan dipilih tiga pemenang untuk setiap kategori. Sebelum puncak acara, akan ada tahapan-tahapan kerja pendampingan bagi para pemenang dari KBKM Regional. Rangkaian acara dalam KBKM Nasional terdiri atas:
                    <ol>
                      <li>Pendampingan secara online oleh fasilitator</li>
                      <li>Pelatihan oleh panitia dan fasilitator </li>
                      <li>Peserta menyiapkan seluruh bahan presentasi, membuat model atau <i>dummy</i> dan memamerkannya di KBKM Nasional</li>
                      <li>Pameran dan presentasi di hadapan juri</li>
                      <li>Pengumuman juara pertama, kedua dan ketiga KBKM Nasional kategori Aplikasi dan Prakarya</li>
                      <li>Pertemuan para juara dengan orang tua asuh utk pengembangan karya</li>
                    </ol>
                  </li>
                </ol>
              </div>

              <!-- <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Tentunya, paska kemah, kalian juga dapat mengaktivasi lebih jauh 
              pengembangan gagasan menjadi aplikasi dan prakarya. Jadi 
              siapkanlah amunisi gagasan terbaik kalian untuk mengikuti KBKM 
              2020!
              </p> -->

              <!-- <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Sampai Jumpa Sahabat Muda….
              </p> -->
              <!-- <p style="text-align: justify;">
                <ol>
                  <li>Pastikan kamu membentuk tim yang terdiri dari 3-5 orang berusia 18 hingga 25 tahun.</li>
                  <li>Mengisi formulir pendaftaran yang akan dibuka di laman <i><a href="#">kbkm.kebudayaan.id</a></i> pada tanggal 28 Maret 2020.</li>
                  <li>Melengkapi semua persyaratan yang ada di laman tersebut.</li>
                  <li>Menciptakan ide prakarya atau aplikasi untuk menjawab permasalahan dari daftar masalah Pemajuan Kebudayan yang ada di Kabupaten/Kota tempat tim kamu mendaftarkan diri.</li>
                </ol>
              </p> -->
              <br>
              <br>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
  <!-- End Faq KBKM 3 -->

  <!-- Start Faq KBKM 4  -->
  <div id="faq" class="about-area area-padding" 
    style="background-image: url('{{ asset('assets/img/background/bg4.jpeg') }}');background-repeat: no-repeat;background-size: 100% 101% " >
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" >
        </div>
      </div>
      <div class="row"> 
        <!-- single-well start-->
      
        <!-- single-well end-->
        <div class="col-md-12 col-sm-12 col-xs-12" style="background-color:rgba(225,225,225,0.7); #808080; border-radius: 25px; box-shadow: 1px 1px 0.7px grey;">
          <div class="well-middle">
            <div class="single-well">
            <br>
            <br>
              <div class="">
                <h4><p style="text-align= justify; margin-left:5%;"><b>Apa jadwal pelaksanaan KBKM Regional?</b></p></h4>
              </div>
              <br>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              KBKM Regional dilaksanakan di wilayah kerja 11 UPT Direktorat Jenderal Kebudayaan dengan jadwal sebagai berikut:
              </p>
              <div style="text-align: justify; margin-left:6%; margin-right:6%;">
                <ol>
                  <li>BPNB Aceh (3-7 Juli 2020) untuk wilayah Aceh dan Sumatra Utara</li>
                  <li>BPNB Sumatra Barat (3-7 Juli 2020) untuk wilayah Sumatra Barat, Bengkulu, Jambi, dan Sumsel</li>
                  <li>BPNB Kepulauan Riau (3-7 Juli 2020) untuk wilayah Riau, Kepulauan Riau, dan Bangkla-Belitung</li>
                  <li>BPNB Jawa Barat (10-14 Juli 2020) untuk wilayah Lampung, Banten, DKI Jakarta, dan Jawa Barat</li>
                  <li>BPNB Yogyakarta (10-14 Juli 2020) untuk wilayah Jawa Tengah, DIY, dan Jawa Timur</li>
                  <li>BPNB Bali (10-14 Juli 2020) untuk wilayah Bali, NTB, dan NTT</li>
                  <li>BPNB Kalimantan Barat (17-21 Juli 2020) untuk wilayah Kalimantan Barat, Kaliantan Tengah, Kalimantan Selatan, Kalimantan Timur, dan Kalimantan Utara</li>
                  <li>BPNB Sulawesi Selatan (17-21 Juli 2020) untuk wilayah Sulawesi Selatan, Sulawesi Barat, dan Sulawesi Tenggara</li>
                  <li>BPNB Sulawesi Utara (17-21 Juli 2020) untuk wilayah Sulawesi Utara, Gorontalo, dan Sulawesi Tengah</li>
                  <li>BPNB Maluku (26-30 Juli 2020) untuk wilayah Maluku dan Maluku Utara</li>
                  <li>BPNB Papua (26-30 Juli 2020) untuk wilayah Papua dan Papua Barat</li>
                </ol>
              </div>

              <!-- <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Tentunya, paska kemah, kalian juga dapat mengaktivasi lebih jauh 
              pengembangan gagasan menjadi aplikasi dan prakarya. Jadi 
              siapkanlah amunisi gagasan terbaik kalian untuk mengikuti KBKM 
              2020!
              </p> -->

              <!-- <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Sampai Jumpa Sahabat Muda….
              </p> -->
              <!-- <p style="text-align: justify;">
                <ol>
                  <li>Pastikan kamu membentuk tim yang terdiri dari 3-5 orang berusia 18 hingga 25 tahun.</li>
                  <li>Mengisi formulir pendaftaran yang akan dibuka di laman <i><a href="#">kbkm.kebudayaan.id</a></i> pada tanggal 28 Maret 2020.</li>
                  <li>Melengkapi semua persyaratan yang ada di laman tersebut.</li>
                  <li>Menciptakan ide prakarya atau aplikasi untuk menjawab permasalahan dari daftar masalah Pemajuan Kebudayan yang ada di Kabupaten/Kota tempat tim kamu mendaftarkan diri.</li>
                </ol>
              </p> -->
              <br>
              <br>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
<!-- End Faq KBKM 4 -->

   <!-- Start Faq KBKM 5  -->
   <div id="faq" class="about-area area-padding" 
    style="background-image: url('{{ asset('assets/img/background/bg5.jfif') }}');background-repeat: no-repeat;background-size: 100% 101% " >
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" >
        </div>
      </div>
      <div class="row"> 
        <!-- single-well start-->
      
        <!-- single-well end-->
        <div class="col-md-12 col-sm-12 col-xs-12" style="background-color:rgba(225,225,225,0.7); #808080; border-radius: 25px; box-shadow: 1px 1px 0.7px grey;">
          <div class="well-middle">
            <div class="single-well">
            <br>
            <br>
              <div class="">
                <h4><p style="text-align= justify; margin-left:5%;"><b>Apa yang ditanggung dan tidak ditanggung dalam pembiayaan KBKM Regional?</b></p></h4>
              </div>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Panitia menanggung akomodasi, konsumsi dan uang saku peserta selama kegiatan berlangsung. Setiap kelompok yang lolos seleksi menanggung sendiri biaya transportasi keberangkatan dari lokasi asal peserta ke lokasi KBKM Regional dan kepulangan dari lokasi KBKM Regional ke lokasi asal peserta. 
              </p>
              <div class="">
                <h4><p style="text-align= justify; margin-left:5%;"><b>Bagaimana jika di bulan Juli 2020 masih berlaku status darurat COVID-19?</b></p></h4>
              </div>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Apabila demikian halnya, maka keseluruha rangkaian KBKM Regional mulai pelatihan sampai penjurian akan diselenggarakan secara jarak jauh dengan memanfaatkan aplikasi konferensi online yang memungkinkan untuk itu.
              </p>
              <div class="">
                <h4><p style="text-align= justify; margin-left:5%;"><b>Bagaimana cara ikut KBKM?</b></p></h4>
              </div>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Dengan mendaftar secara daring dalam situs resmi KBKM, yakni <a href=https://kebudayaan.kemdikbud.go.id/ target="_blank">kebudayaan.kemdikbud.id</a>. Pendaftaran akan ditutup pada tanggal 3 Juni 2020.
              </p>
              <div class="">
                <h4><p style="text-align= justify; margin-left:5%;"><b>Di mana dapat mempelajari lebih lanjut tentang Pokok Pikiran Kebudayaan Daeerah (PPKD)?</b></p></h4>
              </div>
              <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              PPKD setiap provinsi dan kabupaten dapat diunduh di tautan ini: <a href=http://kongres.kebudayaan.id/pra-kongres-kebudayaan-2018/ target="_blank">http://kongres.kebudayaan.id/pra-kongres-kebudayaan-2018/</a>
              </p>
              <!-- <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Tentunya, paska kemah, kalian juga dapat mengaktivasi lebih jauh 
              pengembangan gagasan menjadi aplikasi dan prakarya. Jadi 
              siapkanlah amunisi gagasan terbaik kalian untuk mengikuti KBKM 
              2020!
              </p> -->

              <!-- <p style="text-align: justify; margin-left:6%; margin-right:6%;">
              Sampai Jumpa Sahabat Muda….
              </p> -->
              <!-- <p style="text-align: justify;">
                <ol>
                  <li>Pastikan kamu membentuk tim yang terdiri dari 3-5 orang berusia 18 hingga 25 tahun.</li>
                  <li>Mengisi formulir pendaftaran yang akan dibuka di laman <i><a href="#">kbkm.kebudayaan.id</a></i> pada tanggal 28 Maret 2020.</li>
                  <li>Melengkapi semua persyaratan yang ada di laman tersebut.</li>
                  <li>Menciptakan ide prakarya atau aplikasi untuk menjawab permasalahan dari daftar masalah Pemajuan Kebudayan yang ada di Kabupaten/Kota tempat tim kamu mendaftarkan diri.</li>
                </ol>
              </p> -->
              <br>
              <br>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>
<!-- End Faq KBKM 5 -->
 
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
  <script src="{{ asset('assets/demo/datatables-demo.js') }}"></script>
  
     

  <script>
  var page = 0;
  $('#example').dataTable({
      "initComplete": function (oSettings) { //changed line

          var oTable = this;
          var totalRows = oTable.fnGetData().length;


          oTable.fnPageChange(1);
          page = Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength);
      }
  });
  </script>
  <script>
    function myFunction1() {
      var dots1 = document.getElementById("dots1");
      var moreText1 = document.getElementById("more1");
      var btnText1 = document.getElementById("myBtn1");

      if (dots1.style.display === "none") {
        dots1.style.display = "inline";
        btnText1.innerHTML = "baca selengkapnya"; 
        moreText1.style.display = "none";
      } else {
        dots1.style.display = "none";
        btnText1.innerHTML = "tutup"; 
        moreText1.style.display = "inline";
      }
    }

    function myFunction2() {
      var dots2 = document.getElementById("dots2");
      var moreText2 = document.getElementById("more2");
      var btnText2 = document.getElementById("myBtn2");

      if (dots2.style.display === "none") {
        dots2.style.display = "inline";
        btnText2.innerHTML = "baca selengkapnya"; 
        moreText2.style.display = "none";
      } else {
        dots2.style.display = "none";
        btnText2.innerHTML = "tutup"; 
        moreText2.style.display = "inline";
      }
    }

    function myFunction3() {
      var dots3 = document.getElementById("dots3");
      var moreText3 = document.getElementById("more3");
      var btnText3 = document.getElementById("myBtn3");

      if (dots3.style.display === "none") {
        dots3.style.display = "inline";
        btnText3.innerHTML = "baca selengkapnya"; 
        moreText3.style.display = "none";
      } else {
        dots3.style.display = "none";
        btnText3.innerHTML = "tutup"; 
        moreText3.style.display = "inline";
      }
    }
</script>
@endsection