<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kelompok extends Model
{
    protected $table = 'kelompok';
    
    protected $fillable = [
        'nama_kelompok',
        'ketua',
        'mata_lomba',
        'ktp',
        // 'portofolio',
        // 'subjek_portofolio',
        'pertanyaan1',
        'pertanyaan2',
        'pertanyaan3',
        'pertanyaan4',
        'pertanyaan5',
    ];

}
