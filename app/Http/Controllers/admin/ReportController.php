<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use PDF;
use App\Pendaftar;
use App\Kelompok;
use Carbon\Carbon;
use App\Nomenklatur;
use App\Wilayah;

class ReportController extends Controller
{
    public function indexPeserta()
    {
        $data = Pendaftar::all();
        $data_kel = Kelompok::all();
        $nomenklatur = Nomenklatur::all();
        $datas = DB::table('pendaftar')
                    ->select('pendaftar.*')
                    ->join('indonesia_provinces','indonesia_provinces.id','=','pendaftar.id_provinsi')
                    ->join('indonesia_cities','indonesia_cities.id','=','pendaftar.id_kota')
                    ->join('indonesia_districts','indonesia_districts.id','=','pendaftar.id_kecamatan')
                    ->get();
        $provinsi = DB::table('indonesia_provinces')
                    ->select('indonesia_provinces.*')
                    ->get();

        return view('admin.report.peserta.index',compact('data_kel','data','provinsi','nomenklatur'));
    }

    public function indexKelompok()
    {
        $data = Pendaftar::all();
        $data_kel = Kelompok::all();
        $nomenklatur = Nomenklatur::all();
        return view('admin.report.kelompok.index',compact('data_kel','data','nomenklatur'));
    }

    public function pdf($id)
    {
        $pdfview = Kelompok::find($id);
        $pendaftar = Pendaftar::where('id_kelompok','=',$id)->get();
        // return view('admin.report.kelompok.pdf.pdfview' ,compact('pdfview','pendaftar'));
        $pdf = PDF::loadView('admin.report.kelompok.pdf.pdfview',['pdfview' => $pdfview, 'pendaftar' => $pendaftar])->setPaper('a4','potrait');
        $filename = $pdfview->nama_kelompok;
        return $pdf->stream($filename . '.pdf');
    }

    public function searchkelompok(Request $request)
    {
        $data = Pendaftar::all();
        $nomenklatur = Nomenklatur::all();
        $mata_lomba = $_GET['mata_lomba'];
        $mulai = $_GET['mulai'];
        $akhir = $_GET['akhir'];
        if($mata_lomba != "" && $mulai != "" && $akhir != ""){
            $data_kel = DB::table('kelompok')
                ->where('kelompok.mata_lomba', $mata_lomba)
                ->whereBetween(DB::raw('DATE(kelompok.created_at)'),array($mulai,$akhir))
                ->get();
        }elseif($mulai != "" && $akhir != ""){
                $data_kel = DB::table('kelompok')
                ->whereBetween(DB::raw('DATE(kelompok.created_at)'),array($mulai,$akhir))
                ->get();
        }elseif($mata_lomba != ""){
            $data_kel = DB::table('kelompok')
                ->where('kelompok.mata_lomba', $mata_lomba)
                ->get();
        }else{
            $data_kel = Kelompok::all();
        }
        return view('admin.report.kelompok.index',compact('data_kel','data','nomenklatur'));
    }

    public function searchpeserta(Request $request)
    {
        $data = Pendaftar::all();
        $nomenklatur = Nomenklatur::all();
        $provinsi = DB::table('indonesia_provinces')
                        ->select('indonesia_provinces.*')
                        ->get();
        $nomenklaturs = $request->nomenklatur;
        $wilayah = $request->wilayah;
        $dari = $request->dari;
        $sampai = $request->sampai;
        if($wilayah != "" && $dari != "" && $sampai !=""){
            $data = DB::table('pendaftar')
                ->where('pendaftar.id_provinsi', $wilayah)
                ->whereBetween('pendaftar.tanggal_lahir',array($dari,$sampai))
                ->get();
        }elseif($dari != "" && $sampai !="" && $wilayah == ""){
            $data = DB::table('pendaftar')
                ->whereBetween('pendaftar.tanggal_lahir',array($dari,$sampai))
                ->get();
        }elseif($wilayah != ""){
            $data = DB::table('pendaftar')
                ->where('pendaftar.id_provinsi', $wilayah)
                ->get();
        }elseif($nomenklaturs != "" ){
            $data = DB::table('pendaftar')
                ->select('pendaftar.*','wilayah.*','nomenklatur.*')
                ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
                ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
                ->where('nomenklatur.id_nomenklatur','=',$nomenklaturs)
                ->get();
        }else{
            $data = Pendaftar::all();
        }
        return view('admin.report.peserta.index',compact('data','nomenklatur','provinsi'));
    }

    public function findWilayah(Request $request)
    {
        $data = DB::table('wilayah')->select('id_provinsi','name')->where('id_nomenklatur',$request->id)->take(100)->get();
        return response()->json($data);
    }

    public function exportkelompok(Request $request)
    {
        $data = Pendaftar::all();
        $nomenklatur = Nomenklatur::all();
        $mata_lomba = $_GET['mata_lomba'];
        $mulai = $_GET['mulai'];
        $akhir = $_GET['akhir'];
        if($mata_lomba != "" && $mulai != "" && $akhir != ""){
            $data_kel = DB::table('kelompok')
                ->where('kelompok.mata_lomba','=',$mata_lomba)
                ->whereBetween(DB::raw('DATE(kelompok.created_at)'),array($mulai,$akhir))
                ->get();
            $tes = "";
            $no = 1;
            if(count($data_kel) > 0){
                $tes .= '<p align="center"><h3>Data Kelompok</h3>
                <i>Event Budaya</i> - <strong>Kementrian Budaya</strong></p>
                <table class="table" border="1px solid #eee">
                <tr>
                    <th style="background : green; color:white;"> No</th>
                    <th style="background : green; color:white;"> Nama Kelompok</th>
                    <th style="background : green; color:white;"> Ketua Kelompok</th>
                    <th style="background : green; color:white;"> Mata Lomba</th>
                    <th style="background : green; color:white;"> Jawaban 1</th>
                    <th style="background : green; color:white;"> Jawaban 2</th>
                    <th style="background : green; color:white;"> Jawaban 3</th>
                    <th style="background : green; color:white;"> Jawaban 4</th>
                    <th style="background : green; color:white;"> Jawaban 5</th>
                </tr>';

                foreach ($data_kel as $item){
                    $tes .= '
                        <tr>
                            <td>'. $no.'</td>
                            <td>'. $item->nama_kelompok.'</td>
                            <td>'. $item->ketua.'</td>
                            <td>'. $item->mata_lomba.'</td>
                            <td>'. $item->pertanyaan1.'</td>
                            <td>'. $item->pertanyaan2.'</td>
                            <td>'. $item->pertanyaan3.'</td>
                            <td>'. $item->pertanyaan4.'</td>
                            <td>'. $item->pertanyaan5.'</td>
                        </tr>';
                    $no++;
                }
                $tes .= '</table>';
            }
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment; filename=Kelompok.xls');
            echo $tes;
        }elseif($mulai != "" && $akhir != "" ){
            $data_kel = DB::table('kelompok')
            ->whereBetween(DB::raw('DATE(kelompok.created_at)'),array($mulai,$akhir))
            ->get();
            dd($data_kel);
            $tes = "";
            $no = 1;
            if(count($data_kel) > 0){
                $tes .= '<p align="center"><h3>Data Kelompok</h3>
                <i>Event Budaya</i> - <strong>Kementrian Budaya</strong></p>
                <table class="table" border="1px solid #eee">
                <tr>
                    <th style="background : green; color:white;"> No</th>
                    <th style="background : green; color:white;"> Nama Kelompok</th>
                    <th style="background : green; color:white;"> Ketua Kelompok</th>
                    <th style="background : green; color:white;"> Mata Lomba</th>
                    <th style="background : green; color:white;"> Jawaban 1</th>
                    <th style="background : green; color:white;"> Jawaban 2</th>
                    <th style="background : green; color:white;"> Jawaban 3</th>
                    <th style="background : green; color:white;"> Jawaban 4</th>
                    <th style="background : green; color:white;"> Jawaban 5</th>
                </tr>';

                foreach ($data_kel as $item){
                    $tes .= '
                        <tr>
                            <td>'. $no.'</td>
                            <td>'. $item->nama_kelompok.'</td>
                            <td>'. $item->ketua.'</td>
                            <td>'. $item->mata_lomba.'</td>
                            <td>'. $item->pertanyaan1.'</td>
                            <td>'. $item->pertanyaan2.'</td>
                            <td>'. $item->pertanyaan3.'</td>
                            <td>'. $item->pertanyaan4.'</td>
                            <td>'. $item->pertanyaan5.'</td>
                        </tr>';
                    $no++;
                }
                $tes .= '</table>';
            }
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment; filename=Kelompok.xls');
            echo $tes;
        }elseif($mata_lomba != ""){
            $data_kel = DB::table('kelompok')
            ->where('kelompok.mata_lomba','=',$mata_lomba)
            ->get();
            $tes = "";
            $no = 1;
            if(count($data_kel) > 0){
                $tes .= '<p align="center"><h3>Data Kelompok</h3>
                <i>Event Budaya</i> - <strong>Kementrian Budaya</strong></p>
                <table class="table" border="1px solid #eee">
                <tr>
                    <th style="background : green; color:white;"> No</th>
                    <th style="background : green; color:white;"> Nama Kelompok</th>
                    <th style="background : green; color:white;"> Ketua Kelompok</th>
                    <th style="background : green; color:white;"> Mata Lomba</th>
                    <th style="background : green; color:white;"> Jawaban 1</th>
                    <th style="background : green; color:white;"> Jawaban 2</th>
                    <th style="background : green; color:white;"> Jawaban 3</th>
                    <th style="background : green; color:white;"> Jawaban 4</th>
                    <th style="background : green; color:white;"> Jawaban 5</th>
                </tr>';

                foreach ($data_kel as $item){
                    $tes .= '
                        <tr>
                            <td>'. $no.'</td>
                            <td>'. $item->nama_kelompok.'</td>
                            <td>'. $item->ketua.'</td>
                            <td>'. $item->mata_lomba.'</td>
                            <td>'. $item->pertanyaan1.'</td>
                            <td>'. $item->pertanyaan2.'</td>
                            <td>'. $item->pertanyaan3.'</td>
                            <td>'. $item->pertanyaan4.'</td>
                            <td>'. $item->pertanyaan5.'</td>
                        </tr>';
                    $no++;
                }
                $tes .= '</table>';
            }
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment; filename=Kelompok.xls');
            echo $tes;
        }else{
            $data_kel = DB::table('kelompok')->get();
            $tes = "";
            $no = 1;
            if(count($data_kel) > 0){
                $tes .= '<p align="center"><h3>Data Kelompok</h3>
                <i>Event Budaya</i> - <strong>Kementrian Budaya</strong></p>
                <table class="table" border="1px solid #eee">
                <tr>
                    <th style="background : green; color:white;"> No</th>
                    <th style="background : green; color:white;"> Nama Kelompok</th>
                    <th style="background : green; color:white;"> Ketua Kelompok</th>
                    <th style="background : green; color:white;"> Mata Lomba</th>
                    <th style="background : green; color:white;"> Jawaban 1</th>
                    <th style="background : green; color:white;"> Jawaban 2</th>
                    <th style="background : green; color:white;"> Jawaban 3</th>
                    <th style="background : green; color:white;"> Jawaban 4</th>
                    <th style="background : green; color:white;"> Jawaban 5</th>
                </tr>';

                foreach ($data_kel as $item){
                    $tes .= '
                        <tr>
                            <td>'. $no.'</td>
                            <td>'. $item->nama_kelompok.'</td>
                            <td>'. $item->ketua.'</td>
                            <td>'. $item->mata_lomba.'</td>
                            <td>'. $item->pertanyaan1.'</td>
                            <td>'. $item->pertanyaan2.'</td>
                            <td>'. $item->pertanyaan3.'</td>
                            <td>'. $item->pertanyaan4.'</td>
                            <td>'. $item->pertanyaan5.'</td>
                        </tr>';
                    $no++;
                }
                $tes .= '</table>';
            }
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment; filename=Kelompok.xls');
            echo $tes;
        }
    }
                                                                                                                                                                                
    public function exportpendaftar(Request $request)
    {
        $data = Pendaftar::all();
        $nomenklatur = Nomenklatur::all();
        $nomenklaturs = $_GET['nomenklatur'];
        $wilayah = $_GET['wilayah'];
        $dari = $_GET['dari'];
        $sampai = $_GET['sampai'];
        if($wilayah != "" && $dari != "" && $sampai != ""){
            $data_kel = DB::table('pendaftar')
                ->where('pendaftar.id_provinsi','=',$wilayah)
                ->whereBetween('pendaftar.tanggal_lahir',array($dari,$sampai))
                ->get();
            $tes = "";
            $no = 1;
            if(count($data_kel) > 0){
                $tes .= '<p align="center"><h3>Data Pendaftar</h3><br>
                <i>Event Budaya</i> - <strong>Kementrian Budaya</strong></p><br><br>
                <table class="table" border="1px solid #eee">
                <tr>
                    <th style="background : green; color:white;"> No</th>
                    <th style="background : green; color:white;"> Nama Peserta</th>
                    <th style="background : green; color:white;"> Nama Kelompok</th>
                    <th style="background : green; color:white;"> Jenis Kelamin</th>
                    <th style="background : green; color:white;"> nomber </th>
                    <th style="background : green; color:white;"> Email</th>
                    <th style="background : green; color:white;"> Pendidikan</th>
                    <th style="background : green; color:white;"> NIK</th>
                    <th style="background : green; color:white;"> tanggal_lahir</th>
                    <th style="background : green; color:white;"> Alamat</th>
                </tr>';

                foreach ($data_kel as $item){
                    $tes .= '
                        <tr>
                            <td>'. $no.'</td>
                            <td>'. $item->nama.'</td>
                            <td>'. GetNamaKelompok($item->id_kelompok).'</td>
                            <td>'. $item->jk.'</td>
                            <td>'. $item->no_hp.'</td>
                            <td>'. $item->email.'</td>
                            <td>'. $item->pendidikan.'</td>
                            <td>'. $item->nik.'</td>
                            <td>'. $item->tanggal_lahir.'</td>
                            <td>'. GetProvinsi($item->id_provinsi). GetKota($item->id_kota).  GetKecamatan($item->id_kecamatan). $item->alamat.'</td>
                        </tr>';
                    $no++;
                }
                $tes .= '</table>';
            }
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment; filename=Peserta.xls');
            echo $tes;
        }elseif($dari != "" || $sampai !=""){
            $data_kel = DB::table('pendaftar')
            ->whereBetween('pendaftar.tanggal_lahir',array($dari,$sampai))  
            ->get();
            $tes = "";
            $no = 1;
            if(count($data_kel) > 0){
                $tes .= '<p align="center"><h3>Data Peserta</h3><br>
                <i>Event Budaya</i> - <strong>Kementrian Budaya</strong></p><br><br>
                <table class="table" border="1px solid #eee">
                <tr>
                <th style="background : green; color:white;"> No</th>
                <th style="background : green; color:white;"> Nama Peserta</th>
                <th style="background : green; color:white;"> Nama Kelompok</th>
                <th style="background : green; color:white;"> Jenis Kelamin</th>
                <th style="background : green; color:white;"> nomber </th>
                <th style="background : green; color:white;"> Email</th>
                <th style="background : green; color:white;"> Pendidikan</th>
                <th style="background : green; color:white;"> NIK</th>
                <th style="background : green; color:white;"> tanggal_lahir</th>
                <th style="background : green; color:white;"> Alamat</th>
                </tr>';

                foreach ($data_kel as $item){
                    $tes .= '
                    <tr>
                    <td>'. $no.'</td>
                    <td>'. $item->nama.'</td>
                    <td>'. GetNamaKelompok($item->id_kelompok).'</td>
                    <td>'. $item->jk.'</td>
                    <td>'. $item->no_hp.'</td>
                    <td>'. $item->email.'</td>
                    <td>'. $item->pendidikan.'</td>
                    <td>'. $item->nik.'</td>
                    <td>'. $item->tanggal_lahir.'</td>
                    <td>'. GetProvinsi($item->id_provinsi). GetKota($item->id_kota).  GetKecamatan($item->id_kecamatan). $item->alamat.'</td>
                </tr>';
                    $no++;
                }
                $tes .= '</table>';
            }
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment; filename=Peserta.xls');
            echo $tes;
        }elseif($wilayah != ""){
            $data_kel = DB::table('pendaftar')
            ->where('pendaftar.id_provinsi','=',$wilayah)
            ->get();
            $tes = "";
            $no = 1;
            if(count($data_kel) > 0){
                $tes .= '<p align="center"><h3>Data Peserta</h3><br>
                <i>Event Budaya</i> - <strong>Kementrian Budaya</strong></p><br><br>
                <table class="table" border="1px solid #eee">
                <tr>
                <th style="background : green; color:white;"> No</th>
                <th style="background : green; color:white;"> Nama Peserta</th>
                <th style="background : green; color:white;"> Nama Kelompok</th>
                <th style="background : green; color:white;"> Jenis Kelamin</th>
                <th style="background : green; color:white;"> nomber </th>
                <th style="background : green; color:white;"> Email</th>
                <th style="background : green; color:white;"> Pendidikan</th>
                <th style="background : green; color:white;"> NIK</th>
                <th style="background : green; color:white;"> tanggal_lahir</th>
                <th style="background : green; color:white;"> Alamat</th>
                </tr>';

                foreach ($data_kel as $item){
                    $tes .= '
                    <tr>
                    <td>'. $no.'</td>
                    <td>'. $item->nama.'</td>
                    <td>'. GetNamaKelompok($item->id_kelompok).'</td>
                    <td>'. $item->jk.'</td>
                    <td>'. $item->no_hp.'</td>
                    <td>'. $item->email.'</td>
                    <td>'. $item->pendidikan.'</td>
                    <td>'. $item->nik.'</td>
                    <td>'. $item->tanggal_lahir.'</td>
                    <td>'. GetProvinsi($item->id_provinsi). GetKota($item->id_kota).  GetKecamatan($item->id_kecamatan). $item->alamat.'</td>
                </tr>';
                    $no++;
                }
                $tes .= '</table>';
            }
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment; filename=Peserta.xls');
            echo $tes;
        }elseif($nomenklaturs != ""){
            $data_kel = DB::table('pendaftar')
            ->select('pendaftar.*','wilayah.*','nomenklatur.*')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=',$nomenklaturs)
            ->get();
            $tes = "";
            $no = 1;
            if(count($data_kel) > 0){
                $tes .= '<p align="center"><h3>Data Peserta</h3><br>
                <i>Event Budaya</i> - <strong>Kementrian Budaya</strong></p><br><br>
                <table class="table" border="1px solid #eee">
                <tr>
                <th style="background : green; color:white;"> No</th>
                <th style="background : green; color:white;"> Nama Peserta</th>
                <th style="background : green; color:white;"> Nama Kelompok</th>
                <th style="background : green; color:white;"> Jenis Kelamin</th>
                <th style="background : green; color:white;"> nomber </th>
                <th style="background : green; color:white;"> Email</th>
                <th style="background : green; color:white;"> Pendidikan</th>
                <th style="background : green; color:white;"> NIK</th>
                <th style="background : green; color:white;"> tanggal_lahir</th>
                <th style="background : green; color:white;"> Alamat</th>
                </tr>';

                foreach ($data_kel as $item){
                    $tes .= '
                    <tr>
                    <td>'. $no.'</td>
                    <td>'. $item->nama.'</td>
                    <td>'. GetNamaKelompok($item->id_kelompok).'</td>
                    <td>'. $item->jk.'</td>
                    <td>'. $item->no_hp.'</td>
                    <td>'. $item->email.'</td>
                    <td>'. $item->pendidikan.'</td>
                    <td>'. $item->nik.'</td>
                    <td>'. $item->tanggal_lahir.'</td>
                    <td>'. GetProvinsi($item->id_provinsi). GetKota($item->id_kota).  GetKecamatan($item->id_kecamatan). $item->alamat.'</td>
                </tr>';
                    $no++;
                }
                $tes .= '</table>';
            }
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment; filename=Peserta.xls');
            echo $tes;
        }else{
            $data_kel = DB::table('pendaftar')->get();
            $tes = "";
            $no = 1;
            if(count($data_kel) > 0){
                $tes .= '<p style="text-align:center;"><h3>Data Peserta </h3><br>
                <i>Event Budaya</i> - <strong>Kementrian Budaya</strong></p><br><br>
                <table class="table" border="1px solid #eee">
                <tr>
                <th style="background : green; color:white;"> No</th>
                <th style="background : green; color:white;"> Nama Peserta</th>
                <th style="background : green; color:white;"> Nama Kelompok</th>
                <th style="background : green; color:white;"> Jenis Kelamin</th>
                <th style="background : green; color:white;"> nomber </th>
                <th style="background : green; color:white;"> Email</th>
                <th style="background : green; color:white;"> Pendidikan</th>
                <th style="background : green; color:white;"> NIK</th>
                <th style="background : green; color:white;"> tanggal_lahir</th>
                <th style="background : green; color:white;"> Alamat</th>
                </tr>';

                foreach ($data_kel as $item){
                    $tes .= '
                        <tr>
                            <td>'. $no.'</td>
                            <td>'. $item->nama.'</td>
                            <td>'. GetNamaKelompok($item->id_kelompok).'</td>
                            <td>'. $item->jk.'</td>
                            <td>'. $item->no_hp.'</td>
                            <td>'. $item->email.'</td>
                            <td>'. $item->pendidikan.'</td>
                            <td>'. $item->nik.'</td>
                            <td>'. $item->tanggal_lahir.'</td>
                            <td>'. GetProvinsi($item->id_provinsi). GetKota($item->id_kota).  GetKecamatan($item->id_kecamatan). $item->alamat.'</td>
                        </tr>';
                    $no++;
                }
                $tes .= '</table>';
            }
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment; filename=Peserta.xls');
            echo $tes;
            }
    }

    public function ktp($id)
    {
        $ktp = Kelompok::where('id', $id)->select('ktp')->value('ktp');
        $nama_kelompok = Kelompok::where('id', $id)->select('nama_kelompok')->value('nama_kelompok');

        return response()->file('storage/ktp/'.'ktp-'.$nama_kelompok.'/'.$ktp, [
            'Content-Type' => 'application/pdf'
        ]);
    }

    public function portofolio($id)
    {
        $portofolio = Kelompok::where('id', $id)->select('portofolio')->value('portofolio');
        $nama_kelompok = Kelompok::where('id', $id)->select('nama_kelompok')->value('nama_kelompok');

        return response()->file('storage/portofolio/'.'portofolio-'.$nama_kelompok.'/'.$portofolio, [
            'Content-Type' => 'application/pdf'
        ]);
    }
    
    public function detailpdfkelompok($id)
    {
        $kelompok = Kelompok::where('id','=',$id)->get();
        $pendaftar = Pendaftar::where('id_kelompok','=',$id)->get();
        return view('admin.report.kelompok.detail',compact('kelompok','pendaftar'));
    }
}
