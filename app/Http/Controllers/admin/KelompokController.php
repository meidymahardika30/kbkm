<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Pendaftar;
use App\Kelompok;
use Carbon\Carbon;
use App\Nomenklatur;
use App\Wilayah;

class KelompokController extends Controller
{
    public function index()
    {
        $data = Pendaftar::all();
        $data_kel = Kelompok::all();
        // $pra = DB::table('pendaftar')
        //         ->join('kelompok','kelompok.id','=','pendaftar.id_kelompok')
        //         ->get();
        return view('admin.kelompok.index',compact('data_kel'));
    }

    public function ktp($id)
    {
        $ktp = Kelompok::where('id', $id)->select('ktp')->value('ktp');
        $nama_kelompok = Kelompok::where('id', $id)->select('nama_kelompok')->value('nama_kelompok');

        return response()->file('storage/ktp/'.'ktp-'.$nama_kelompok.'/'.$ktp, [
            'Content-Type' => 'application/pdf'
        ]);
    }

    public function portofolio($id)
    {
        $portofolio = Kelompok::where('id', $id)->select('portofolio')->value('portofolio');
        $nama_kelompok = Kelompok::where('id', $id)->select('nama_kelompok')->value('nama_kelompok');

        return response()->file('storage/portofolio/'.'portofolio-'.$nama_kelompok.'/'.$portofolio, [
            'Content-Type' => 'application/pdf'
        ]);
    }

    public function show($id)
    {
        $kelompok = Kelompok::where('id','=',$id)->get();
        $pendaftar = Pendaftar::where('id_kelompok','=',$id)->get();
        return view('admin.kelompok.detail',compact('kelompok','pendaftar'));
    }

    public function edit($id)
    {
        $kelompok = Kelompok::where('id','=',$id)->get();
        $pendaftar = Pendaftar::where('id_kelompok','=',$id)->get();
        return view('admin.kelompok.edit',compact('kelompok','pendaftar'));
    }

    public function update(Request $request, $id)
    {
        $update = Kelompok::where('id', $id)->update([
            'nama_kelompok' => $request->nama_kelompok,
            'pertanyaan1' => $request->pertanyaan1,
            'pertanyaan2' => $request->pertanyaan2,
            'pertanyaan3' => $request->pertanyaan3,
            'pertanyaan4' => $request->pertanyaan4,
            'pertanyaan5' => $request->pertanyaan5,
        ]);

        if($update){
            return redirect('/admin/kelompok')->with('message_store','Berhasil update kelompok');
        }else{
            return back('/admin/kelompok')->with('message_store','Gagal update berita');
        }
    }

    public function destroy($id)
    {
        $destroy = Kelompok::where('id',$id)->delete();
        $pendaftar = Pendaftar::where('id_kelompok',$id)->delete();
        return redirect('/admin/kelompok');
    }
}
