<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Nomenklatur;
use App\Wilayah;
use App\Kelompok;
use App\Pendaftar;
use DB;

class BalaiController extends Controller
{
    public function index()
    {
        $data = Nomenklatur::all();
        $subdata = Wilayah::orderBy('name','ASC')->get();

        $jumlah1 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb1')
        ->get();

        $pra1 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb1')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();

        $apk1 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb1')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        
        $countapk1 = count($apk1);
        $countpra1 = count($pra1);
        $count1 = count($jumlah1);

        $jumlah2 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb2')
        ->get();

        $pra2 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb2')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();

        $apk2 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb2')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        
        $countapk2 = count($apk2);
        $countpra2 = count($pra2);
        $count2 = count($jumlah2);

        $jumlah3 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb3')
        ->get();

        $pra3 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb3')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();

        $apk3 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb3')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        
        $countapk3 = count($apk3);
        $countpra3 = count($pra3);
        $count3 = count($jumlah3);

        $jumlah4 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb4')
        ->get();

        $pra4 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb4')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();

        $apk4 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb4')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        
        $countapk4 = count($apk4);
        $countpra4 = count($pra4);
        $count4 = count($jumlah4);

        $jumlah5 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb5')
        ->get();
        
        $pra5 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb5')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();

        $apk5 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb5')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        
        $countapk5 = count($apk5);
        $countpra5 = count($pra5);
        $count5 = count($jumlah5);


        $jumlah6 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb6')
        ->get();
        
        $pra6 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb6')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();

        $apk6 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb6')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        
        $countapk6 = count($apk6);
        $countpra6 = count($pra6);
        $count6 = count($jumlah6);

        $jumlah7 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb7')
        ->get();
        
        $pra7 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb7')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();

        $apk7 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb7')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        
        $countapk7 = count($apk7);
        $countpra7 = count($pra7);
        $count7 = count($jumlah7);

        $jumlah8 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb8')
        ->get();
        
        $pra8 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb8')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();

        $apk8 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb8')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        
        $countapk8 = count($apk8);
        $countpra8 = count($pra8);
        $count8 = count($jumlah8);

        $jumlah9 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb9')
        ->get();

        $pra9 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb9')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();

        $apk9 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb9')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        
        $countapk9 = count($apk9);
        $countpra9 = count($pra9);
        $count9 = count($jumlah9);

        $jumlah10 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb10')
        ->get();
        
        $pra10 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb10')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();

        $apk10 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb10')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        
        $countapk10 = count($apk10);
        $countpra10 = count($pra10);
        $count10 = count($jumlah10);

        $jumlah11 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb11')
        ->get();
        
        $pra11 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb11')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();


        $apk11 = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('nomenklatur.id_nomenklatur','=','bpnb11')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        
        $countapk11 = count($apk11);
        $countpra11 = count($pra11);
        $count11 = count($jumlah11);

        $jumlahapk = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('kelompok.mata_lomba','=','aplikasi')
        ->get();

        $jumlahpra = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->where('kelompok.mata_lomba','=','prakarya')
        ->get();

        $jumlahseluruh = DB::table('kelompok')
        ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
        ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
        ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
        ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
        ->get();

        $countseluruh = count($jumlahseluruh);
        $countjumlahapk = count($jumlahapk);
        $countjumlahpra = count($jumlahpra);
        

        return view('admin.balai.index',compact('data','subdata',
                                                'count1','count2','count3','count4','count5',
                                                'count6','count7','count8','count9','count10','count11',
                                            'countapk1','countpra1','countapk2','countpra2','countapk3','countpra3',
                                            'countapk4','countpra4','countapk5','countpra5','countapk6','countpra6',
                                            'countapk7','countpra7','countapk8','countpra8','countapk9','countpra9',
                                            'countapk9','countpra9','countapk10','countpra10','countapk11','countpra11',
                                            'countseluruh','countjumlahapk','countjumlahpra'));
    }


    public function exportbpnb()
    {
            $data = Nomenklatur::all();
            $jumlah1 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb1')
            ->get();

            $pra1 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb1')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();

            $apk1 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb1')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
            
            $countapk1 = count($apk1);
            $countpra1 = count($pra1);
            $count1 = count($jumlah1);
            
            $jumlah2 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb2')
            ->get();
    
            $pra2 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb2')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();
    
            $apk2 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb2')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
    
            
            $countapk2 = count($apk2);
            $countpra2 = count($pra2);
            $count2 = count($jumlah2);
    
            $jumlah3 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb3')
            ->get();
    
            $pra3 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb3')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();
    
            $apk3 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb3')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
    
            
            $countapk3 = count($apk3);
            $countpra3 = count($pra3);
            $count3 = count($jumlah3);
    
            $jumlah4 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb4')
            ->get();
    
            $pra4 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb4')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();
    
            $apk4 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb4')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
    
            
            $countapk4 = count($apk4);
            $countpra4 = count($pra4);
            $count4 = count($jumlah4);
    
            $jumlah5 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb5')
            ->get();
            
            $pra5 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb5')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();
    
            $apk5 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb5')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
    
            
            $countapk5 = count($apk5);
            $countpra5 = count($pra5);
            $count5 = count($jumlah5);
    
    
            $jumlah6 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb6')
            ->get();
            
            $pra6 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb6')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();
    
            $apk6 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb6')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
    
            
            $countapk6 = count($apk6);
            $countpra6 = count($pra6);
            $count6 = count($jumlah6);
    
            $jumlah7 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb7')
            ->get();
            
            $pra7 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb7')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();
    
            $apk7 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb7')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
    
            
            $countapk7 = count($apk7);
            $countpra7 = count($pra7);
            $count7 = count($jumlah7);
    
            $jumlah8 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb8')
            ->get();
            
            $pra8 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb8')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();
    
            $apk8 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb8')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
    
            
            $countapk8 = count($apk8);
            $countpra8 = count($pra8);
            $count8 = count($jumlah8);
    
            $jumlah9 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb9')
            ->get();
    
            $pra9 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb9')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();
    
            $apk9 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb9')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
    
            
            $countapk9 = count($apk9);
            $countpra9 = count($pra9);
            $count9 = count($jumlah9);
    
            $jumlah10 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb10')
            ->get();
            
            $pra10 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb10')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();
    
            $apk10 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb10')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
    
            
            $countapk10 = count($apk10);
            $countpra10 = count($pra10);
            $count10 = count($jumlah10);
    
            $jumlah11 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb11')
            ->get();
            
            $pra11 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb11')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();
    
            $apk11 = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('nomenklatur.id_nomenklatur','=','bpnb11')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
    
            
            $countapk11 = count($apk11);
            $countpra11 = count($pra11);
            $count11 = count($jumlah11);
    
            $jumlahapk = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('kelompok.mata_lomba','=','aplikasi')
            ->get();
    
            $jumlahpra = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->where('kelompok.mata_lomba','=','prakarya')
            ->get();
    
            $jumlahseluruh = DB::table('kelompok')
            ->select('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->join('pendaftar','pendaftar.nama','=','kelompok.ketua')
            ->join('wilayah','wilayah.id_provinsi','=','pendaftar.id_provinsi')
            ->join('nomenklatur','nomenklatur.id_nomenklatur','=','wilayah.id_nomenklatur')
            ->groupBy('nomenklatur.id_nomenklatur','kelompok.ketua','nomenklatur.nama_nomenklatur')
            ->get();
    
            $countseluruh = count($jumlahseluruh);
            $countjumlahapk = count($jumlahapk);
            $countjumlahpra = count($jumlahpra);

            $tes = "";
            $no = 1;
            if(count($data) > 0){
                $tes .= '<p align="center"><h3>Data BPNB</h3>
                <i>Event Budaya</i> - <strong>Kementrian Budaya</strong></p>
                <table class="table" border="1px solid #eee">
                <tr>
                    <th rowspan="2" style="background : green; color:white;"> No</th>
                    <th rowspan="2" style="background : green; color:white;"> Wilayah</th>
                    <th colspan="3" style="background : green; color:white;"> Kategori</th>
                </tr>
                <tr>
                    <th style="background : green; color:white;"> Aplikasi</th>
                    <th style="background : green; color:white;"> Prakarya </th>
                    <th style="background : green; color:white;"> Total</th>
                </tr>';

                foreach ($data as $item){
                    $tes .= '
                        <tr>
                            <td>'. $no.'</td>
                            <td style = "padding:20px;">'. $item->nama_nomenklatur.'</td>';
                            if($item['id_nomenklatur'] == 'bpnb1'){
                                $tes.= '<td><b>'.$countapk1.'</b></td>
                                <td><b>'.$countpra1.'</b></td>
                                <td><b>'.$count1.'</b></td>';
                            }elseif($item['id_nomenklatur'] == 'bpnb2'){
                                $tes.= '<td><b>'.$countapk2.'</b></td>
                                <td><b>'.$countpra2.'</b></td>
                                <td><b>'.$count2.'</b></td>';
                            }elseif($item['id_nomenklatur'] == 'bpnb3'){
                                $tes.= '<td><b>'.$countapk3.'</b></td>
                                <td><b>'.$countpra3.'</b></td>
                                <td><b>'.$count3.'</b></td>';
                            }elseif($item['id_nomenklatur'] == 'bpnb4'){
                                $tes.= '<td><b>'.$countapk4.'</b></td>
                                <td><b>'.$countpra4.'</b></td>
                                <td><b>'.$count4.'</b></td>';
                            }elseif($item['id_nomenklatur'] == 'bpnb5'){
                                $tes.= '<td><b>'.$countapk5.'</b></td>
                                <td><b>'.$countpra5.'</b></td>
                                <td><b>'.$count5.'</b></td>';
                            }elseif($item['id_nomenklatur'] == 'bpnb6'){
                                $tes.= '<td><b>'.$countapk6.'</b></td>
                                <td><b>'.$countpra6.'</b></td>
                                <td><b>'.$count6.'</b></td>';
                            }elseif($item['id_nomenklatur'] == 'bpnb7'){
                                $tes.= '<td><b>'.$countapk7.'</b></td>
                                <td><b>'.$countpra7.'</b></td>
                                <td><b>'.$count7.'</b></td>';
                            }elseif($item['id_nomenklatur'] == 'bpnb8'){
                                $tes.= '<td><b>'.$countapk8.'</b></td>
                                <td><b>'.$countpra8.'</b></td>
                                <td><b>'.$count8.'</b></td>';
                            }elseif($item['id_nomenklatur'] == 'bpnb9'){
                                $tes.= '<td><b>'.$countapk9.'</b></td>
                                <td><b>'.$countpra9.'</b></td>
                                <td><b>'.$count9.'</b></td>';
                            }elseif($item['id_nomenklatur'] == 'bpnb10'){
                                $tes.= '<td><b>'.$countapk10.'</b></td>
                                <td><b>'.$countpra10.'</b></td>
                                <td><b>'.$count10.'</b></td>';
                            }else{
                                $tes.= '<td><b>'.$countapk11.'</b></td>
                                <td><b>'.$countpra11.'</b></td>
                                <td><b>'.$count11.'</b></td>';
                            }
                            
                        '</tr>';
                    $no++;
                }
                $tes .= '<tr>
                        <td></td>
                        <td>Total</td>
                        <td>'. $countjumlahapk.'</td>
                        <td>'. $countjumlahpra.'</td>
                        <td>'. $countseluruh.'</td>
                        </tr>';
                $tes .= '</table>';
            }
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment; filename=BPNB.xls');
            echo $tes;
        }
}
