<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Pendaftar;
use App\Kelompok;
use Carbon\Carbon;
use App\Nomenklatur;
use App\Wilayah;

class PesertaController extends Controller
{
    public function index()
    {
        $data = Pendaftar::all();
        $data_kel = Kelompok::all();
        $nomenklatur = Nomenklatur::all();
        $datas = DB::table('pendaftar')
                          ->select('pendaftar.*')
                          ->join('indonesia_provinces','indonesia_provinces.id','=','pendaftar.id_provinsi')
                          ->join('indonesia_cities','indonesia_cities.id','=','pendaftar.id_kota')
                          ->join('indonesia_districts','indonesia_districts.id','=','pendaftar.id_kecamatan')
                          ->get();

        $provinsi = DB::table('indonesia_provinces')
                    ->select('indonesia_provinces.*','indonesia_cities.*','indonesia_districts.*')
                    ->join('indonesia_cities','indonesia_cities.province_id','=','indonesia_provinces.id')
                    ->join('indonesia_districts','indonesia_districts.city_id','=','indonesia_cities.id')
                    ->get();
        // $kota = DB::table('indonesia_cities')
        //             ->select('indonesia_cities.*')
        //             ->get();
        // $kecamatan = DB::table('indonesia_districts')
        //             ->select('indonesia_districts.*')
        //             ->get();
        return view('admin.peserta.index',compact('data_kel','data','provinsi','nomenklatur'));
    }

    public function edit($id)
    {
        $provinsi = \Indonesia::allProvinces();
        $kota = \Indonesia::allCities();
        $kecamatan = \Indonesia::allDistricts();
        $year = date('Y');
        $m = date('m');
        $d = date('d');
        $yMax = $year - 18;
        $yMin = $year - 25;
        $max = $yMax.'-'.$m.'-'.$d;
        $min = $yMin.'-'.$m.'-'.$d;
        $pendaftar = Pendaftar::where('id_kelompok','=',$id)->get();
        return view('admin.kelompok.editpeserta',compact('pendaftar','min','max','provinsi','kecamatan','kota'));
    }

    public function update(Kelompok $kelompok, Pendaftar $pendaftar, Request $request,$id)
    {
        $email = Pendaftar::select('email')->where('id_kelompok', $id)->value('email');

        if($request->mata_lomba == "aplikasi"){
            $rules = [
                'nama.*' => 'required',
                'jk.*' => 'required',
                'no_hp.*' => 'required',
                'email.*' => 'required',
                'pendidikan.*' => 'required',
                'nik.*' => 'required | size:16',
                'tanggal_lahir.*' => 'required',
                'provinsi.*' => 'required',
                'kota.*' => 'required',
                'kecamatan.*' => 'required',
                'alamat.*' => 'required',
            ];
        }else{
            $rules = [
                'nama.0' => 'required', 
                'nama.1' => 'required', 
                'nama.2' => 'required',
                'jk.0' => 'required', 
                'jk.1' => 'required', 
                'jk.2' => 'required',
                'no_hp.0' => 'required', 
                'no_hp.1' => 'required', 
                'no_hp.2' => 'required',
                'email.0' => 'required', 
                'email.1' => 'required', 
                'email.2' => 'required',
                'pendidikan.0' => 'required', 
                'pendidikan.1' => 'required', 
                'pendidikan.2' => 'required',
                'nik.0' => 'required | size:16', 
                'nik.1' => 'required | size:16', 
                'nik.2' => 'required | size:16',
                'tanggal_lahir.0' => 'required', 
                'tanggal_lahir.1' => 'required', 
                'tanggal_lahir.2' => 'required',
                'provinsi.0' => 'required', 
                'provinsi.1' => 'required', 
                'provinsi.2' => 'required',
                'kota.0' => 'required', 
                'kota.1' => 'required', 
                'kota.2' => 'required',
                'kecamatan.0' => 'required', 
                'kecamatan.1' => 'required', 
                'kecamatan.2' => 'required',
                'alamat.0' => 'required', 
                'alamat.1' => 'required', 
                'alamat.2' => 'required',
            ];
        
        }
        
        $messages = [
            'nama.*.required' => 'Nama wajib diisi',
            'jk.*.required' => 'Jenis kelamin wajib diisi',
            'no_hp.*.required' => 'No.HP wajib diisi',
            'email.*.required' => 'Email wajib diisi',
            'email.*.unique' => 'Email yang anda masukan sudah digunakan.',
            'pendidikan.*.required' => 'Pendidikan wajib diisi',
            'nik.*.required' => 'NIK wajib diisi',
            'nik.*.size' => 'NIK harus :size karakter.',
            'nik.*.unique' => 'NIK yang anda masukan sudah digunakan.',
            'tanggal_lahir.*.required' => 'Tanggal lahir wajib diisi',
            'provinsi.*.required' => 'Provinsi wajib diisi',
            'kota.*.required' => 'Kota wajib diisi',
            'kecamatan.*.required' => 'Kecamatan wajib diisi',
            'alamat.*.required' => 'Alamat wajib diisi',
            'required' => ':attribute wajib diisi.',
            'min' => ':attribute minimal :min karakter.',
            'max' => [
                'file' => ':attribute tidak boleh lebih besar dari :max kilobytes.',
                'string' => ':attribute maximal :max karakter.',
            ],
            'unique' => ':attribute yang anda masukan sudah digunakan.',
            'email' => 'email harus berupa alamat email yang valid.',
            'size' => [
                'string' => ':attribute harus :size karakter.',
            ],
        ];

        // if($request->portofolio->getClientMimeType() != "application/pdf"){
        //     return back()->with('danger','Portofolio harus berupa file type: pdf.');
        // }
      

        if(!in_array("p", $request->jk, FALSE)){
            return back()->with('danger','Salah satu peserta harus ada perempuan!');
        }


        $loop = count($request->nama);
        
        $no = 1;
        for ($i=0; $i<$loop; $i++){
            if($request->nama[$i] != null){
                $update_pendaftar = Pendaftar::where('id_kelompok', $id)->where('nomor', $no++)->update([
                    'id_kelompok' => $id,
                    'nama' => $request->nama[$i],
                    'jk' => $request->jk[$i],
                    'no_hp' => $request->no_hp[$i],
                    'email' => $request->email[$i],
                    'pendidikan' => $request->pendidikan[$i],
                    'nik' => $request->nik[$i],
                    'tanggal_lahir' => $request->tanggal_lahir[$i],
                    'id_provinsi' => $request->provinsi[$i],
                    'id_kota' => $request->kota[$i],
                    'id_kecamatan' => $request->kecamatan[$i],
                    'alamat' => $request->alamat[$i],
                ]);
            }
        }

        $nama_ketua = Kelompok::where('id', $id)->update(['ketua' => $request->nama[0]]);

        if($update_pendaftar && $nama_ketua){
            return redirect('admin/kelompok/edit/'. $id)->with('success', 'Berhasil update data peserta');
        }else{
            return back()->with('danger', 'Update data pendaftaran gagal!');
        }
    }
}
