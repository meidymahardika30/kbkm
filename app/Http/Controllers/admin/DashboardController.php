<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Pendaftar;
use App\Kelompok;
use Carbon\Carbon;
use App\Nomenklatur;
use App\Wilayah;
use App\Berita;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $age18 = DB::table('pendaftar')
                    ->whereRaw('TIMESTAMPDIFF(YEAR,DATE(pendaftar.tanggal_lahir), current_date) = 18')
                    ->count('pendaftar.tanggal_lahir');
       
        $age19 = DB::table('pendaftar')
                    ->whereRaw('TIMESTAMPDIFF(YEAR,DATE(pendaftar.tanggal_lahir), current_date) = 19')
                    ->count('pendaftar.tanggal_lahir');

        $age20 = DB::table('pendaftar')
                    ->whereRaw('TIMESTAMPDIFF(YEAR,DATE(pendaftar.tanggal_lahir), current_date) = 20')
                    ->count('pendaftar.tanggal_lahir');

        $age21 = DB::table('pendaftar')
                    ->whereRaw('TIMESTAMPDIFF(YEAR,DATE(pendaftar.tanggal_lahir), current_date) = 21')
                    ->count('pendaftar.tanggal_lahir');

        $age22 = DB::table('pendaftar')
                    ->whereRaw('TIMESTAMPDIFF(YEAR,DATE(pendaftar.tanggal_lahir), current_date) = 22')
                    ->count('pendaftar.tanggal_lahir');

        $age23 = DB::table('pendaftar')
                    ->whereRaw('TIMESTAMPDIFF(YEAR,DATE(pendaftar.tanggal_lahir), current_date) = 23')
                    ->count('pendaftar.tanggal_lahir');

        $age24 = DB::table('pendaftar')
                    ->whereRaw('TIMESTAMPDIFF(YEAR,DATE(pendaftar.tanggal_lahir), current_date) = 24')
                    ->count('pendaftar.tanggal_lahir');

        $age25 = DB::table('pendaftar')
                    ->whereRaw('TIMESTAMPDIFF(YEAR,DATE(pendaftar.tanggal_lahir), current_date) = 25')
                    ->count('pendaftar.tanggal_lahir');
        // Start Data Seluruh Pendaftar
        $dataall = DB::table('pendaftar')
        ->count('id');
        $databoy = DB::table('pendaftar')
        ->where('pendaftar.jk','=','l')
        ->count('id');
        $datagirl = DB::table('pendaftar')
        ->where('pendaftar.jk','=','p')
        ->count('id');

        // End Data Seluruh Pendaftar

        // APlikasi
        $apk = DB::table('pendaftar')
                ->join('kelompok','kelompok.id','=','pendaftar.id_kelompok')
                ->where('kelompok.mata_lomba','=','aplikasi')
                ->count('nama');
        $apkboy = DB::table('pendaftar')
                ->join('kelompok','kelompok.id','=','pendaftar.id_kelompok')
                ->where('kelompok.mata_lomba','=','aplikasi')
                ->where('pendaftar.jk','=','l')
                ->count('nama');
        $apkgirl = DB::table('pendaftar')
                ->join('kelompok','kelompok.id','=','pendaftar.id_kelompok')
                ->where('kelompok.mata_lomba','=','aplikasi')
                ->where('pendaftar.jk','=','p')
                ->count('nama');

        // Prakarya
        $pra = DB::table('pendaftar')
                ->join('kelompok','kelompok.id','=','pendaftar.id_kelompok')
                ->where('kelompok.mata_lomba','=','prakarya')
                ->count('nama');
        $praboy = DB::table('pendaftar')
                ->join('kelompok','kelompok.id','=','pendaftar.id_kelompok')
                ->where('kelompok.mata_lomba','=','prakarya')
                ->where('pendaftar.jk','=','l')
                ->count('nama');
        $pragirl = DB::table('pendaftar')
                ->join('kelompok','kelompok.id','=','pendaftar.id_kelompok')
                ->where('kelompok.mata_lomba','=','prakarya')
                ->where('pendaftar.jk','=','p')
                ->count('nama');
        
        $jmlapk = DB::table('kelompok')
                ->where('kelompok.mata_lomba','=','aplikasi')
                ->count('id');

        $jmlpra = DB::table('kelompok')
                ->where('kelompok.mata_lomba','=','prakarya')
                ->count('id');

        $berita = Berita::all();

        return view('admin.dashboard.index',
            compact('berita','jmlapk','jmlpra','dataall','databoy',
        'datagirl','apk','apkboy','apkgirl','pra','praboy','pragirl','age18',
        'age19','age20','age21','age22','age23','age24','age25')
        );
        
    }

    public function indexberita()
    {
        
        return view('admin.realberita.beitaform');
    }

    public function findWilayah(Request $request)
    {
        $data = DB::table('wilayah')->select('id_provinsi','name')->where('id_nomenklatur',$request->id)->take(100)->get();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
