<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Arsip;
use DB;
use Storage;

class ArsipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Arsip::all();
        return view('admin.arsip.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.arsip.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $namaFoto = $request->file('foto')->getClientOriginalName();
        $request->foto->storeAs('public/admin/arsip/'.$request->title, $namaFoto);

        $store = Arsip::create([
            'title' => $request->title,
            'konten' => $request->konten,
            'foto' => $namaFoto,
            'tgl' => $request->tgl,
        ]);

        if($store){
            return redirect('/admin/arsip')->with('message_store','Berhasil menambahkan berita');
        }else{
            return back('/admin/arsip')->with('message_store','Gagal menambahkan berita');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('arsip')
        ->where('arsip.id','=',$id)
        ->get();

        return view('admin.arsip.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $title = Arsip::where('id', $id)->select('title')->value('title');
        Storage::deleteDirectory('public/admin/arsip/'.$title);

        $namaFoto = $request->file('foto')->getClientOriginalName();
        $request->foto->storeAs('public/admin/arsip/'.$request->title, $namaFoto);

        $berita = Arsip::select('title')->where('id',$id)->first();
        $update = Arsip::where('id', $id)->update([
            'title' => $request->title,
            'konten' => $request->konten,
            'foto' => $namaFoto,
            'tgl' => $request->tgl,
        ]);

        if($update){
            return redirect('/admin/arsip')->with('message_store','Berhasil mengupdate berita');
        }else{
            return back('/admin/arsip')->with('message_store','Gagal update berita');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Arsip::where('id',$id)->delete();
        return redirect('/admin/arsip')->with('message_store','Berhasil menghapus berita');
    }
}
