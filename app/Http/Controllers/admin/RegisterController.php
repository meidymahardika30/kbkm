<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator()
    // {
    //     return Validator::make([
    //         'name' => 'required|string|max:255',
    //         'email' => 'required|string|email|max:255|unique:users',
    //         'password' => 'required|string|min:8|confirmed',
    //     ]);
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function create(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
        ];
        $messages = [
            'name.required' => 'Nama wajib diisi',
            'email.required' => 'Email wajib diisi',
            'email.unique' => 'Email yang anda masukan sudah digunakan.',
            'min' => ':attribute minimal :min karakter.',
        ];

        $request->validate($rules, $messages);

        $store = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        
        if($store){
            return redirect('/admin/user')->with('message_store','Berhasil menambahkan user');
        }else{
            return back('/admin/user')->with('message_store','Gagal menambahkan user');
        }
    }

    public function index()
    {
        $data = User::all();
        return view('admin.user.index', compact('data'));
    }

    public function view()
    {
        return view('admin.user.create');
    }

    public function edit($id)
    {
        $data = User::where('id',$id)->get();
        $d = User::select('password')->where('id',$id)->first();
        $d1 = $d->password;
        $pass = Crypt::decryptString($d1);
        return view('admin.user.edit',compact('data','pass'));
    }

    public function destroy($id)
    {
        $destroy = User::where('id',$id)->delete();
        return redirect('/admin/user');
    }

    public function update(Request $request,$id)
    {
        $data = User::select('email')->where('id',$id)->first();
        $d = $data->email;
        if($d != $request->email ){
            $rules = [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:8',
            ];
            $messages = [
                'name.required' => 'Nama wajib diisi',
                'email.required' => 'Email wajib diisi',
                'email.unique' => 'Email Yang anda masukan '.$request->email.' sudah terpakai ini email awalmu ' . $d,
                'min' => ':attribute minimal :min karakter.',
            ];
    
            $request->validate($rules, $messages);
            $store = User::where('id', $id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Crypt::encryptString($request->password),
            ]);

            if($store){
                return redirect('/admin/user')->with('message_store','Berhasil Mengupdate user');
            }else{
                return back('/admin/user')->with('message_store','Gagal Mengupdate user');
            }
        }else{
            $rules = [
                'name' => 'required|string|max:255',
                'password' => 'required|string|min:8',
            ];
            $messages = [
                'name.required' => 'Nama wajib diisi',
                'email.required' => 'Email wajib diisi',
                'min' => ':attribute minimal :min karakter.',
            ];
    
            $request->validate($rules, $messages);
            $store = User::where('id', $id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Crypt::encryptString($request->password),
            ]);

            if($store){
                return redirect('/admin/user')->with('message_store','Berhasil Mengupdate user');
            }else{
                return back('/admin/user')->with('message_store','Gagal Mengupdate user');
            }
        }
        
    }
}
