<?php

namespace App\Http\Controllers;
use App\Provinsi;
use App\Kelompok;
use App\Pendaftar;
use Storage;
use DB;

use Illuminate\Http\Request;

class CekController extends Controller
{
    public function index(Request $request)
    {
        $sessionId =$request->session()->get('id');
        $pendaftar = DB::table('pendaftar')
                ->where('id_kelompok', '=', $request->session()->get('id'))
                ->get();
        $kelompok = DB::table('kelompok')
                ->where('id', $request->session()->get('id'))
                ->get();
        
        return view('pendaftaran.cek.index', compact('sessionId', 'pendaftar', 'kelompok'));
    }

    public function store()
    {
        return redirect('/pendaftaran/index')->with('success', 'data berhasil tersimpan');
    }

    public function edit($id)
    {
        $provinsi = \Indonesia::allProvinces();
        $kota = \Indonesia::allCities();
        $kecamatan = \Indonesia::allDistricts();
        $year = date('Y');
        $m = date('m');
        $d = date('d');
        $yMax = $year - 18;
        $yMin = $year - 25;
        $max = $yMax.'-'.$m.'-'.$d;
        $min = $yMin.'-'.$m.'-'.$d;
        
        $pendaftar = DB::table('pendaftar')
                ->where('id_kelompok', '=', $id)
                ->get();
        $kelompok = DB::table('kelompok')
                ->where('id', $id)
                ->get();
        
        return view('/pendaftaran/cek/edit', compact('max', 'min', 'pendaftar', 'kelompok', 'provinsi', 'kota', 'kecamatan'));
    }

    public function update(Kelompok $kelompok, Pendaftar $pendaftar, Request $request,$id)
    {
        $nama_kelompok = Kelompok::select('nama_kelompok')->where('id', $id)->value('nama_kelompok');
        $email = Pendaftar::select('email')->where('id_kelompok', $id)->value('email');

        if($request->mata_lomba == "aplikasi"){
            $rules = [
                'nama_kelompok' => 'required | min:3 | unique:kelompok,nama_kelompok,'.$id,
                'mata_lomba' => 'required',
                'nama.*' => 'required',
                'jk.*' => 'required',
                'no_hp.*' => 'required',
                'email.*' => 'required',
                'pendidikan.*' => 'required',
                'nik.*' => 'required | size:16',
                'tanggal_lahir.*' => 'required',
                'provinsi.*' => 'required',
                'kota.*' => 'required',
                'kecamatan.*' => 'required',
                'alamat.*' => 'required',
                'ktp' => 'required | max:5000',
                // 'portofolio' => 'max:10000',
                'pertanyaan1' => 'required',
                'pertanyaan2' => 'required',
                'pertanyaan3' => 'required',
                'pertanyaan4' => 'required',
                'pertanyaan5' => 'required',
            ];
        }elseif($request->mata_lomba == "prakarya"){
            $rules = [
                'nama_kelompok' => $nama_kelompok == $request->nama_kelompok ? 'required | min:3' : 'required | min:3 | unique:kelompok,nama_kelompok',
                'mata_lomba' => 'required',
                'nama.0' => 'required', 
                'nama.1' => 'required', 
                'nama.2' => 'required',
                'jk.0' => 'required', 
                'jk.1' => 'required', 
                'jk.2' => 'required',
                'no_hp.0' => 'required', 
                'no_hp.1' => 'required', 
                'no_hp.2' => 'required',
                'email.0' => 'required', 
                'email.1' => 'required', 
                'email.2' => 'required',
                'pendidikan.0' => 'required', 
                'pendidikan.1' => 'required', 
                'pendidikan.2' => 'required',
                'nik.0' => 'required | size:16', 
                'nik.1' => 'required | size:16', 
                'nik.2' => 'required | size:16',
                'tanggal_lahir.0' => 'required', 
                'tanggal_lahir.1' => 'required', 
                'tanggal_lahir.2' => 'required',
                'provinsi.0' => 'required', 
                'provinsi.1' => 'required', 
                'provinsi.2' => 'required',
                'kota.0' => 'required', 
                'kota.1' => 'required', 
                'kota.2' => 'required',
                'kecamatan.0' => 'required', 
                'kecamatan.1' => 'required', 
                'kecamatan.2' => 'required',
                'alamat.0' => 'required', 
                'alamat.1' => 'required', 
                'alamat.2' => 'required',
                'ktp' => 'required | max:5000',
                // 'portofolio' => 'max:10000',
                'pertanyaan1' => 'required',
                'pertanyaan2' => 'required',
                'pertanyaan3' => 'required',
                'pertanyaan4' => 'required',
                'pertanyaan5' => 'required',
            ];
        }else{
            $rules = [
                'nama_kelompok' => $nama_kelompok == $request->nama_kelompok ? 'required | min:3' : 'required | min:3 | unique:kelompok,nama_kelompok',
                'mata_lomba' => 'required',
                'ktp' => 'required | max:5000',
                'pertanyaan1' => 'required',
                'pertanyaan2' => 'required',
                'pertanyaan3' => 'required',
                'pertanyaan4' => 'required',
                'pertanyaan5' => 'required',
            ];
        }
        
        $messages = [
            'nama.*.required' => 'Nama wajib diisi',
            'jk.*.required' => 'Jenis kelamin wajib diisi',
            'no_hp.*.required' => 'No.HP wajib diisi',
            'email.*.required' => 'Email wajib diisi',
            'email.*.unique' => 'Email yang anda masukan sudah digunakan.',
            'pendidikan.*.required' => 'Pendidikan wajib diisi',
            'nik.*.required' => 'NIK wajib diisi',
            'nik.*.size' => 'NIK harus :size karakter.',
            'nik.*.unique' => 'NIK yang anda masukan sudah digunakan.',
            'tanggal_lahir.*.required' => 'Tanggal lahir wajib diisi',
            'provinsi.*.required' => 'Provinsi wajib diisi',
            'kota.*.required' => 'Kota wajib diisi',
            'kecamatan.*.required' => 'Kecamatan wajib diisi',
            'alamat.*.required' => 'Alamat wajib diisi',
            'required' => ':attribute wajib diisi.',
            'min' => ':attribute minimal :min karakter.',
            'max' => [
                'file' => ':attribute tidak boleh lebih besar dari :max kilobytes.',
                'string' => ':attribute maximal :max karakter.',
            ],
            'unique' => ':attribute yang anda masukan sudah digunakan.',
            'email' => 'email harus berupa alamat email yang valid.',
            'size' => [
                'string' => ':attribute harus :size karakter.',
            ],
        ];
        
        $request->validate($rules, $messages);
        
        if($request->ktp->getClientMimeType() != "application/pdf"){
            return back()->with('danger','KTP harus berupa file type: pdf.');
        }

        // if($request->portofolio->getClientMimeType() != "application/pdf"){
        //     return back()->with('danger','Portofolio harus berupa file type: pdf.');
        // }

        if(!in_array("p", $request->jk, FALSE)){
            return back()->with('danger','Salah satu peserta harus ada perempuan!');
        }

        Storage::deleteDirectory('public/ktp/ktp-'.$nama_kelompok);
        // Storage::deleteDirectory('public/potofolio/portofolio-'.$nama_kelompok);

        $namaKTP = $request->file('ktp')->getClientOriginalName();
        // $namaPortofolio = $request->file('portofolio')->getClientOriginalName();
        
        $request->ktp->storeAs('public/ktp/ktp-'.$request->nama_kelompok, $namaKTP);
        // $request->portofolio->storeAs('public/potofolio/portofolio-'.$request->nama_kelompok, $namaPortofolio);
        
        $ketua_kelompok = $request->nama[0];
        
        $update_kelompok = Kelompok::where('id', $id)->update([
            'nama_kelompok' => $request->nama_kelompok,
            'ketua' => $ketua_kelompok,
            'mata_lomba' => $request->mata_lomba,
            'ktp' => $namaKTP,
            // 'portofolio' => $namaPortofolio,
            // 'subjek_portofolio' => $request->subjek_portofolio,
            'pertanyaan1' => $request->pertanyaan1,
            'pertanyaan2' => $request->pertanyaan2,
            'pertanyaan3' => $request->pertanyaan3,
            'pertanyaan4' => $request->pertanyaan4,
            'pertanyaan5' => $request->pertanyaan5,
        ]);

        // Pendaftar::where('id_kelompok', $id)->delete();

        $loop = count($request->nama);
        
        $no = 1;
        for ($i=0; $i<$loop; $i++){
            if($request->nama[$i] != null){
                $update_pendaftar = Pendaftar::where('id_kelompok', $id)->where('nomor', $no++)->update([
                    'id_kelompok' => $id,
                    'nama' => $request->nama[$i],
                    'jk' => $request->jk[$i],
                    'no_hp' => $request->no_hp[$i],
                    'email' => $request->email[$i],
                    'pendidikan' => $request->pendidikan[$i],
                    'nik' => $request->nik[$i],
                    'tanggal_lahir' => $request->tanggal_lahir[$i],
                    'id_provinsi' => $request->provinsi[$i],
                    'id_kota' => $request->kota[$i],
                    'id_kecamatan' => $request->kecamatan[$i],
                    'alamat' => $request->alamat[$i],
                ]);
            }
        }

        if($update_kelompok && $update_pendaftar){
            return redirect('pendaftaran/index')->with('success', 'Berhasil update data pendaftaran');
        }else{
            return back()->with('danger', 'Update data pendaftaran gagal!');
        }
    }
}
