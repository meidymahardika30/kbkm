<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function index()
    {
        return response()->file('assets/pdf/ktp.pdf', [
            'Content-Type' => 'application/pdf'
        ]);
    }
}
