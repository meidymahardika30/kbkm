<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Berita;
use App\Pendaftar;
use App\Kelompok;

class LandingPageController extends Controller
{
    public function index()
    {
        $jmlapk = DB::table('kelompok')
                ->where('kelompok.mata_lomba','=','aplikasi')
                ->count('id');

        $jmlpra = DB::table('kelompok')
                ->where('kelompok.mata_lomba','=','prakarya')
                ->count('id');

        $berita = Berita::latest('created_at')->paginate(3);


                $data = Kelompok::leftJoin('pendaftar', function($join){
                        $join->on('kelompok.id', '=', 'pendaftar.id_kelompok');
                        $join->on('kelompok.ketua','=','pendaftar.nama');
                })->get();
                
        
        return view('index',compact('jmlapk','jmlpra','berita','data'));

        
    }
}
