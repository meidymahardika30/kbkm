<?php

namespace App\Http\Controllers;
use App\Provinsi;
use App\Kelompok;
use App\Pendaftar;
use DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PendaftaranController extends Controller
{
    public function index()
    {
        $provinsi = \Indonesia::allProvinces();
        $kota = \Indonesia::allCities();
        $kecamatan = \Indonesia::allDistricts();
        $year = date('Y');
        $m = date('m');
        $d = date('d');
        $yMax = $year - 18;
        $yMin = $year - 25;
        $max = $yMax.'-'.$m.'-'.$d;
        $min = $yMin.'-'.$m.'-'.$d;
        
        return view('pendaftaran.index', compact('provinsi', 'kota', 'kecamatan', 'max', 'min'));
    }

    public function findkota(Request $request)
    {
        $data = DB::table('indonesia_cities')->select('id','name')->where('province_id',$request->id)->take(100)->get();
        return response()->json($data);
    }

    public function findkec(Request $request)
    {
        $data = DB::table('indonesia_districts')->select('id','name')->where('city_id',$request->id)->take(100)->get();
        return response()->json($data);
    }
    
    public function store(Request $request)
    {
        if($request->mata_lomba == "aplikasi"){
            $rules = [
                'nama_kelompok' => 'required | min:3 | unique:kelompok,nama_kelompok',
                'mata_lomba' => 'required',
                'nama.*' => 'required',
                'jk.*' => 'required',
                'no_hp.*' => 'required',
                'email.*' => 'required | unique:pendaftar,email',
                'pendidikan.*' => 'required',
                'nik.*' => 'required | size:16 | unique:pendaftar,nik | distinct',
                'nik.*' => 'required|distinct',
                'tanggal_lahir.*' => 'required',
                'provinsi.*' => 'required',
                'kota.*' => 'required',
                'kecamatan.*' => 'required',
                'alamat.*' => 'required',
                'ktp' => 'required | max:5000',
                // 'portofolio' => 'max:10000',
                'pertanyaan1' => 'required',
                'pertanyaan2' => 'required',
                'pertanyaan3' => 'required',
                'pertanyaan4' => 'required',
                'pertanyaan5' => 'required',
            ];
        }elseif($request->mata_lomba == "prakarya"){
            $rules = [
                'nama_kelompok' => 'required | min:3 | unique:kelompok,nama_kelompok',
                'mata_lomba' => 'required',
                'nama.0' => 'required', 
                'nama.1' => 'required', 
                'nama.2' => 'required',
                'jk.0' => 'required', 
                'jk.1' => 'required', 
                'jk.2' => 'required',
                'no_hp.0' => 'required', 
                'no_hp.1' => 'required', 
                'no_hp.2' => 'required',
                'email.0' => 'required | unique:pendaftar,email', 
                'email.1' => 'required | unique:pendaftar,email', 
                'email.2' => 'required | unique:pendaftar,email',
                'pendidikan.0' => 'required', 
                'pendidikan.1' => 'required', 
                'pendidikan.2' => 'required',
                'nik.0' => 'required|size:16|unique:pendaftar,nik|distinct ', 
                'nik.1' => 'required|size:16|unique:pendaftar,nik|distinct', 
                'nik.2' => 'required|size:16|unique:pendaftar,nik|distinct',
                'tanggal_lahir.0' => 'required', 
                'tanggal_lahir.1' => 'required', 
                'tanggal_lahir.2' => 'required',
                'provinsi.0' => 'required', 
                'provinsi.1' => 'required', 
                'provinsi.2' => 'required',
                'kota.0' => 'required', 
                'kota.1' => 'required', 
                'kota.2' => 'required',
                'kecamatan.0' => 'required', 
                'kecamatan.1' => 'required', 
                'kecamatan.2' => 'required',
                'alamat.0' => 'required', 
                'alamat.1' => 'required', 
                'alamat.2' => 'required',
                'ktp' => 'required | max:5000',
                // 'portofolio' => 'max:10000',
                'pertanyaan1' => 'required',
                'pertanyaan2' => 'required',
                'pertanyaan3' => 'required',
                'pertanyaan4' => 'required',
                'pertanyaan5' => 'required',
            ];
        }else{
            $rules = [
                'nama_kelompok' => 'required | min:3 | unique:kelompok,nama_kelompok',
                'mata_lomba' => 'required',
                'ktp' => 'required | max:5000',
                'pertanyaan1' => 'required',
                'pertanyaan2' => 'required',
                'pertanyaan3' => 'required',
                'pertanyaan4' => 'required',
                'pertanyaan5' => 'required',
            ];
        }
        
        $messages = [
            'nama.*.required' => 'Nama wajib diisi',
            'jk.*.required' => 'Jenis kelamin wajib diisi',
            'no_hp.*.required' => 'No.HP wajib diisi',
            'email.*.required' => 'Email wajib diisi',
            'email.*.unique' => 'Email yang anda masukan sudah digunakan.',
            'pendidikan.*.required' => 'Pendidikan wajib diisi',
            'nik.*.required' => 'NIK wajib diisi',
            'nik.*.size' => 'NIK harus :size karakter.',
            'nik.*.unique' => 'NIK sudah digunakan.',
            'tanggal_lahir.*.required' => 'Tanggal lahir wajib diisi',
            'provinsi.*.required' => 'Provinsi wajib diisi',
            'kota.*.required' => 'Kota wajib diisi',
            'kecamatan.*.required' => 'Kecamatan wajib diisi',
            'alamat.*.required' => 'Alamat wajib diisi',
            'required' => ':attribute wajib diisi.',
            'min' => ':attribute minimal :min karakter.',
            'max' => [
                'file' => ':attribute tidak boleh lebih besar dari :max kilobytes.',
                'string' => ':attribute maximal :max karakter.',
            ],
            'unique' => ':attribute yang anda masukan sudah digunakan.',
            'distinct' => ':NIK yang anda masukan sama.',
            'email' => 'email harus berupa alamat email yang valid.',
            'size' => [
                'string' => ':attribute harus :size karakter.',
            ],
        ];
        
        $request->validate($rules, $messages);
        
        if($request->ktp->getClientMimeType() != "application/pdf"){
            return back()->with('danger','Gagal menyimpan data, KTP harus berupa file type: pdf. Isi data kembali');
        }

        // if($request->portofolio->getClientMimeType() != "application/pdf"){
        //     return back()->with('danger','Portofolio harus berupa file type: pdf.');
        // }

        if(!in_array("p", $request->jk, FALSE)){
            return back()->with('danger','Gagal menyimpan data, Salah satu peserta harus ada perempuan. Isi data kembali');
        }
        
        $namaKTP = $request->file('ktp')->getClientOriginalName();
        // $namaPortofolio = $request->file('portofolio')->getClientOriginalName();

        $request->ktp->storeAs('public/ktp/ktp-'.$request->nama_kelompok, $namaKTP);
        // $request->portofolio->storeAs('public/portofolio/portofolio-'.$request->nama_kelompok, $namaPortofolio);
        
        $ketua_kelompok = $request->nama[0];
        
        $store_kelompok = Kelompok::create([
            'nama_kelompok' => $request->nama_kelompok,
            'ketua' => $ketua_kelompok,
            'mata_lomba' => $request->mata_lomba,
            'ktp' => $namaKTP,
            // 'portofolio' => $namaPortofolio,
            // 'subjek_portofolio' => $request->subjek_portofolio,
            'pertanyaan1' => $request->pertanyaan1,
            'pertanyaan2' => $request->pertanyaan2,
            'pertanyaan3' => $request->pertanyaan3,
            'pertanyaan4' => $request->pertanyaan4,
            'pertanyaan5' => $request->pertanyaan5,
        ]);

        $loop = count($request->nama);
        $id = Kelompok::get('id')->max('id');
        $hasil = $id == null ? 0 : $id;
        $id_kelompok = $hasil++;

        $no = 1;
        for ($i=0; $i<$loop; $i++){
            if($request->nama[$i] != null){
                $store_pendaftar = Pendaftar::create([
                    'id_kelompok' => $id_kelompok,
                    'nama' => $request->nama[$i],
                    'jk' => $request->jk[$i],
                    'no_hp' => $request->no_hp[$i],
                    'email' => $request->email[$i],
                    'pendidikan' => $request->pendidikan[$i],
                    'nik' => $request->nik[$i],
                    'tanggal_lahir' => $request->tanggal_lahir[$i],
                    'id_provinsi' => $request->provinsi[$i],
                    'id_kota' => $request->kota[$i],
                    'id_kecamatan' => $request->kecamatan[$i],
                    'alamat' => $request->alamat[$i],
                    'nomor' => $no++,
                ]);
            }
        }

        $request->session()->put('id' ,$id_kelompok);

        if($store_kelompok && $store_pendaftar){
            return redirect('pendaftaran/cek/index')->with('id_kelompok', $request->session()->get('id'));
        }else{
            return back()->with('danger', 'Pendaftaran Anda Gagal!');
        }
    }
}
