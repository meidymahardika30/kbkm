<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nomenklatur extends Model
{
    protected $table = 'nomenklatur';
    protected $fillable = ['id_nomenklatur','nama_nomenklatur'];
}
