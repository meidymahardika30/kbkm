<?php
    
    function GetProvinsi($id){
		$provinsi = DB::table('indonesia_provinces')->where('id', $id)->first();
		
		if(is_object($provinsi)){
			return $provinsi->name;
		}
    }
    
    function GetKota($id){
		$kota = DB::table('indonesia_cities')->where('id', $id)->first();
		
		if(is_object($kota)){
			return $kota->name;
		}
    }
    
    function GetKecamatan($id){
		$kecamatan = DB::table('indonesia_districts')->where('id', $id)->first();
		
		if(is_object($kecamatan)){
			return $kecamatan->name;
		}
	}

	function GetNamaKelompok($id){
		$kelompok = DB::table('kelompok')->where('id', $id)->first();
		
		if(is_object($kelompok)){
			return $kelompok->nama_kelompok;
		}
	}

	function GetMataLomba($id){
		$kelompok = DB::table('kelompok')->where('id', $id)->first();
		
		if(is_object($kelompok)){
			return $kelompok->mata_lomba;
		}
	}