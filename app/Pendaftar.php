<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pendaftar extends Model
{
    protected $table = 'pendaftar';
    
    protected $fillable = [
        'id_kelompok',
        'nama',
        'jk',
        'no_hp',
        'email',
        'pendidikan',
        'nik',
        'tanggal_lahir',
        'id_provinsi',
        'id_kota',
        'id_kecamatan',
        'alamat',
        'nomor',
    ];

}
